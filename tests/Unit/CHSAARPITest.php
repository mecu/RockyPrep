<?php

namespace Tests\Unit;

use App\Game;
use App\Team;
use App\Season;
use App\State;
use App\Year;
use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;

class CHSAARPITest extends TestCase
{
    use RefreshDatabase;

    private $year;

    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed');
        $this->year = new Year();
    }

    /**
     * Generate a single team & season but no games and calculate RPIs
     *
     * @dataProvider levels
     * @test
     *
     * @param int $levelid
     */
    public function singleColoradoTeamNoGames(int $levelid)
    {
        /** @var Season $season */
        [$team, $season] = $this->generateTeamAndSeason($levelid);

        $this->assertEquals(0.0, $season->cwp());
        $this->assertEquals(0.0, $season->cowp());
        $this->assertEquals(0.0, $season->coowp());
    }

    /**
     * Generate a teams & season with games but no scores and calculate RPIs
     *
     * @dataProvider levels
     * @test
     *
     * @param int $levelid
     */
    public function teamWithGamesRPI(int $levelid)
    {
        /** @var Season $seasonA */
        [$teamA, $seasonA] = $this->generateTeamAndSeason($levelid);
        [$teamB, $seasonB] = $this->generateTeamAndSeason($levelid);
        [$teamC, $seasonC] = $this->generateTeamAndSeason($levelid);

        $this->game($teamA, $teamB);
        $this->game($teamA, $teamC);
        $this->game($teamB, $teamC);

        $this->assertEquals(0.0, $seasonA->wp(), 'WP A');
        $this->assertEquals(0.0, $seasonA->owp(), 'OWP A');
        $this->assertEquals(0.0, $seasonA->oowp(), 'OOWP A');
        $this->assertEquals(0.0, $seasonA->cwp(), 'CWP A');
        $this->assertEquals(0.0, $seasonA->cowp(), 'COWP A');
        $this->assertEquals(0.0, $seasonA->coowp(), 'COOWP A');

        $this->assertEquals(0.0, $seasonB->wp());
        $this->assertEquals(0.0, $seasonB->owp());
        $this->assertEquals(0.0, $seasonB->oowp());
        $this->assertEquals(0.0, $seasonB->cwp());
        $this->assertEquals(0.0, $seasonB->cowp());
        $this->assertEquals(0.0, $seasonB->coowp());

        $this->assertEquals(0.0, $seasonC->wp());
        $this->assertEquals(0.0, $seasonC->owp());
        $this->assertEquals(0.0, $seasonC->oowp());
        $this->assertEquals(0.0, $seasonC->cwp());
        $this->assertEquals(0.0, $seasonC->cowp());
        $this->assertEquals(0.0, $seasonC->coowp());
    }

    /**
     * Team A beats Team B, calculate RPIs
     *
     * @dataProvider levels
     * @test
     *
     * @param int $levelid
     */
    public function twoColoradoTeamsSameLevelRPI(int $levelid)
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason($levelid);
        [$teamB, $seasonB] = $this->generateTeamAndSeason($levelid);

        $this->gameWithWinner($teamA, $teamB);

        $this->assertEquals(1.0, $seasonA->cwp());
        $this->assertEquals(0.0, $seasonA->cowp());
        $this->assertEquals(1.0, $seasonA->coowp());

        $this->assertEquals(0.0, $seasonB->cwp());
        $this->assertEquals(0.0, $seasonB->cowp());
        $this->assertEquals(0.0, $seasonB->coowp());
    }

    /**
     * Team A beats Team B & C
     * B beats C
     *
     * Calculate RPIs
     *
     * @dataProvider levels
     * @test
     *
     * @param int $levelid
     */
    public function threeColoradoTeamsSameLevelRPI(int $levelid)
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason($levelid);
        [$teamB, $seasonB] = $this->generateTeamAndSeason($levelid);
        [$teamC, $seasonC] = $this->generateTeamAndSeason($levelid);

        $this->gameWithWinner($teamA, $teamB);
        $this->gameWithWinner($teamA, $teamC);
        $this->gameWithWinner($teamB, $teamC);

        $this->assertEquals(1.0, $seasonA->cwp());
        $this->assertEquals(0.5, $seasonA->cowp());
        $this->assertEquals(0.625, $seasonA->coowp());

        $this->assertEquals(0.5, $seasonB->cwp());
        $this->assertEquals(0.5, $seasonB->cowp());
        $this->assertEquals(0.5, $seasonB->coowp());

        $this->assertEquals(0.0, $seasonC->cwp());
        $this->assertEquals(0.5, $seasonC->cowp());
        $this->assertEquals(0.375, $seasonC->coowp());
    }

    /**
     * A beats BCD
     * B beats _CD
     * C beats __D
     * D beats ___
     *
     * Calculate RPIs
     *
     * @dataProvider levels
     * @test
     *
     * @param int $levelid
     */
    public function fourColoradoTeamsSameLevelRPI(int $levelid)
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason($levelid);
        [$teamB, $seasonB] = $this->generateTeamAndSeason($levelid);
        [$teamC, $seasonC] = $this->generateTeamAndSeason($levelid);
        [$teamD, $seasonD] = $this->generateTeamAndSeason($levelid);

        $this->gameWithWinner($teamA, $teamB);
        $this->gameWithWinner($teamA, $teamC);
        $this->gameWithWinner($teamA, $teamD);
        $this->gameWithWinner($teamB, $teamC);
        $this->gameWithWinner($teamB, $teamD);
        $this->gameWithWinner($teamC, $teamD);

        $this->assertEquals(1.0, $seasonA->cwp());
        $this->assertEquals(0.5, $seasonA->cowp());
        $this->assertEquals(0.5556, $seasonA->coowp());

        $this->assertEquals(0.6667, $seasonB->cwp());
        $this->assertEquals(0.5, $seasonB->cowp());
        $this->assertEquals(0.5185, $seasonB->coowp());

        $this->assertEquals(0.3333, $seasonC->cwp());
        $this->assertEquals(0.5, $seasonC->cowp());
        $this->assertEquals(0.4815, $seasonC->coowp());

        $this->assertEquals(0.0, $seasonD->cwp());
        $this->assertEquals(0.5, $seasonD->cowp());
        $this->assertEquals(0.4444, $seasonD->coowp());
    }

    /**
     * Team A beats Team B, A is 55, B, calculate RPIs
     *
     * @test
     */
    public function teamPlays1LowerTeam()
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason(55);
        [$teamB, $seasonB] = $this->generateTeamAndSeason(44);

        $this->gameWithWinner($teamA, $teamB);

        $this->assertEquals(1.0, $seasonA->cwp());
        $this->assertEquals(0.0, $seasonA->cowp());
        $this->assertEquals(1.0, $seasonA->coowp());

        $this->assertEquals(0.0, $seasonB->cwp());
        $this->assertEquals(0.0, $seasonB->cowp());
        $this->assertEquals(0.0, $seasonB->coowp());
    }

    /**
     * Team A beats Team B and Team C, A is 55, B & C is 44, calculate RPIs
     *
     * @dataProvider levelsForInterLevelGames
     * @test
     *
     * @param int   $higherLevel
     * @param int   $lowerLevel
     * @param float $wp
     * @param float $cwp
     */
    public function teamPlays2LowerTeam(int $higherLevel, int $lowerLevel, float $wp, float $cwp)
    {
        /** @var Season $seasonA */
        [$teamA, $seasonA] = $this->generateTeamAndSeason($higherLevel);
        [$teamB, $seasonB] = $this->generateTeamAndSeason($lowerLevel);
        [$teamC, $seasonC] = $this->generateTeamAndSeason($lowerLevel);

        $this->gameWithWinner($teamA, $teamB);
        $this->gameWithWinner($teamA, $teamC);

        $this->assertEquals($wp, $seasonA->wp(), "$higherLevel HigherLevel WP");
        $this->assertEquals(0, $seasonA->owp(), "$higherLevel HigherLevel OWP");
        $this->assertEquals($wp, $seasonA->oowp(), "$higherLevel HigherLevel OOWP");
        $this->assertEquals($cwp, $seasonA->cwp(), "$higherLevel HigherLevel CWP");
        $this->assertEquals(0, $seasonA->cowp(), "$higherLevel HigherLevel COWP");
        $this->assertEquals($cwp, $seasonA->coowp(), "$higherLevel HigherLevel COOWP");

        $this->assertEquals(0.0, $seasonB->cwp());
        $this->assertEquals(1.0, $seasonB->cowp());
        $this->assertEquals(0.0, $seasonB->coowp());
    }

    private function generateTeamAndSeason(int $level = 55): array
    {
        $state = State::where('name', 'Colorado')->first();

        $team = factory(Team::class)->create([
            'stateid' => $state->stateid,
        ]);

        $season = factory(Season::class)->create(
            [
                'year' => $this->year->year,
                'teamid' => $team->teamid,
                'levelid' => $level,
                'levelid_state' => $level,
            ]
        );

        return [$team, $season];
    }

    private function gameWithWinner(Team $A, Team $B): Game
    {
        $game = $this->game($A, $B);

        // TeamA beats TeamB
        $game->winner    = $A->teamid;
        $game->homescore = rand(7, 50);
        $game->awayscore = rand(0, $game->homescore - 1);
        $game->save();

        return $game;
    }

    private function game(Team $A, Team $B): Game
    {
        $game           = new Game();
        $game->hometeam = $A->teamid;
        $game->awayteam = $B->teamid;
        $game->date     = date('Y-m-d');

        return $game;
    }

    /**
     * Data provider of levels
     *
     * @return array
     */
    public function levels(): array
    {
        return [
            [55],
            [44],
            [33],
            [22],
            [11],
            [7],
            [6],
        ];
    }

    /**
     * Data provider of levelsForInterLevelGames
     *
     * @return array
     */
    public function levelsForInterLevelGames(): array
    {
        return [
            [55, 44, 1.0,  0.9347],
            [44, 33, 0.9,  0.9349],
            [33, 22, 0.75, 0.9348],
            [22, 11, 0.6,  0.9349],
            [11,  7, 0.45, 0.9346],
            [7,   6, 0.3,  0.9348],
        ];
    }
}
