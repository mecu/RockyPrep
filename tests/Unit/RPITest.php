<?php

namespace Tests\Unit;

use App\Game;
use App\Team;
use App\Season;
use App\State;
use App\Year;
use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;

class RPITest extends TestCase
{
    use RefreshDatabase;

    /** @var Year */
    private $year;

    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed');
        $this->year = new Year();
    }

    /**
     * Generate a single team & season and calculate RPIs
     *
     * @test
     */
    public function singleColoradoTeamNoGames()
    {
        /** @var Season $season */
        [$team, $season] = $this->generateTeamAndSeason();

        $this->assertEquals(0.0, $season->wp());
        $this->assertEquals(0.0, $season->owp());
        $this->assertEquals(0.0, $season->oowp());
    }

    /**
     * Team A beats Team B, calculate RPIs
     *
     * @test
     */
    public function twoColoradoTeamsRPI()
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason();
        [$teamB, $seasonB] = $this->generateTeamAndSeason();

        $this->game($teamA, $teamB);

        $this->assertEquals(1.0, $seasonA->wp());
        $this->assertEquals(0.0, $seasonA->owp());
        $this->assertEquals(1.0, $seasonA->oowp());

        $this->assertEquals(0.0, $seasonB->wp());
        $this->assertEquals(0.0, $seasonB->owp());
        $this->assertEquals(0.0, $seasonB->oowp());
    }

    /**
     * Team A beats Team B & C
     * B beats C
     *
     * Calculate RPIs
     *
     * @test
     */
    public function threeColoradoTeamsRPI()
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason();
        [$teamB, $seasonB] = $this->generateTeamAndSeason();
        [$teamC, $seasonC] = $this->generateTeamAndSeason();

        $this->game($teamA, $teamB);
        $this->game($teamA, $teamC);
        $this->game($teamB, $teamC);

        $this->assertEquals(1.0, $seasonA->wp());
        $this->assertEquals(0.5, $seasonA->owp());
        $this->assertEquals(0.625, $seasonA->oowp());

        $this->assertEquals(0.5, $seasonB->wp());
        $this->assertEquals(0.5, $seasonB->owp());
        $this->assertEquals(0.5, $seasonB->oowp());

        $this->assertEquals(0.0, $seasonC->wp());
        $this->assertEquals(0.5, $seasonC->owp());
        $this->assertEquals(0.375, $seasonC->oowp());
    }

    /**
     * A beats BCD
     * B beats _CD
     * C beats __D
     * D beats ___
     *
     * Calculate RPIs
     *
     * @test
     */
    public function fourColoradoTeamsRPI()
    {
        [$teamA, $seasonA] = $this->generateTeamAndSeason();
        [$teamB, $seasonB] = $this->generateTeamAndSeason();
        [$teamC, $seasonC] = $this->generateTeamAndSeason();
        [$teamD, $seasonD] = $this->generateTeamAndSeason();

        $this->game($teamA, $teamB);
        $this->game($teamA, $teamC);
        $this->game($teamA, $teamD);
        $this->game($teamB, $teamC);
        $this->game($teamB, $teamD);
        $this->game($teamC, $teamD);

        $this->assertEquals(1.0, $seasonA->wp());
        $this->assertEquals(0.5, $seasonA->owp());
        $this->assertEquals(0.5556, $seasonA->oowp());

        $this->assertEquals(0.6667, $seasonB->wp());
        $this->assertEquals(0.5, $seasonB->owp());
        $this->assertEquals(0.5185, $seasonB->oowp());

        $this->assertEquals(0.3333, $seasonC->wp());
        $this->assertEquals(0.5, $seasonC->owp());
        $this->assertEquals(0.4815, $seasonC->oowp());

        $this->assertEquals(0.0, $seasonD->wp());
        $this->assertEquals(0.5, $seasonD->owp());
        $this->assertEquals(0.375, $seasonD->oowp());
    }

    private function generateTeamAndSeason(): array
    {
        $state = State::where('name', 'Colorado')->first();

        $team = factory(Team::class)->create([
            'stateid' => $state->stateid,
        ]);

        $season = factory(Season::class)->create(
            [
                'year'   => $this->year->year,
                'teamid' => $team->teamid,
            ]
        );

        return [$team, $season];
    }

    private function game(Team $A, Team $B)
    {
        $game           = new Game();
        $game->hometeam = $A->teamid;
        $game->awayteam = $B->teamid;
        $game->date     = date('Y-m-d');

        // TeamA beats TeamB
        $game->winner    = $A->teamid;
        $game->homescore = rand(7, 50);
        $game->awayscore = rand(0, $game->homescore - 1);
        $game->save();
    }
}
