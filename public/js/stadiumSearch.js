$(function() {
    var cache = {};
    $("#stadiumSearch").autocomplete({
      minLength: 3,
      autoFocus: true,
      delay: 280,
      source: function( request, response ) {
        var term = request.term;
        var statenum = $(".btn-success").attr('id');
        if ($("#stadiumallstates").is(':checked')) statenum = null;
        if ( term+'|'+statenum in cache ) {
          response( cache[ term+'|'+statenum ] );
          return;
        }
        $.getJSON( "/api/v1/searchstadium", {term: request.term, stateid: statenum}, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      },
      select: function( event, ui ) {
        $( "#stadiumSearch" ).val( ui.item.name );
        $( "#stadiumid" ).val( ui.item.id );
        
        return false;
      }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li><a>" + item.label + "</a></li>" )
        .appendTo( ul );
    };
});