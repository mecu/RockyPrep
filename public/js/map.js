var map;

//markerState controls the state of markers are on or off, default is on (true)
var markerState = [];

function initialize() {
    "use strict";
    var myOptions = {
        zoom: 7,
        center: new google.maps.LatLng(stateCenterLat, stateCenterLng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var i;
    
    map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    
    for (i = 0; i < teams.length; i++) {
        var team = teams[i];
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(team[1], team[2]),
            map: map,
            title: team[0],
            zIndex: team[3],
            icon: new google.maps.MarkerImage("/img/marker/marker_" + imageColor[team[3]] + ".png")
        });
        markerState[team[3]] = true;
        window.markers[team[3]].push(marker);
    }
}

google.maps.event.addDomListener(window, 'load', initialize);
    
function Markers(level) {
    var i;
    if (markerState[level] === true) {
        markerState[level] = false;
        // hide the marker
        for (i = 0; i < markers[level].length; i++) {
            markers[level][i].setMap(null);
        }
    }
    else {
        markerState[level] = true;
        // show the marker again
        for (i = 0; i < markers[level].length; i++) {
            markers[level][i].setMap(map);
        }
    }
}