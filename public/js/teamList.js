$().ready(function () {
    $("#teamList").listnav({
        includeNums: false,
        flagDisabled: true,
        noMatchText: "No teams",
        showCounts: true
    });
});
