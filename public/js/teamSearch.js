$(function () {
    var cache = {};
    $("#teamSearch").autocomplete({
        minLength: 3,
        autoFocus: true,
        delay: 280,
        source: function (request, response) {
            var term = request.term;
            var statenum = $(".btn-success").attr('id');
            if ($("#allstates").is(':checked')) statenum = null;
            if (term + '|' + statenum in cache) {
                response(cache[term + '|' + statenum]);
                return;
            }
            $.getJSON("/api/v1/search", {term: request.term, stateid: statenum}, function (data) {
                cache[term] = data;
                response(data);
            });
        },
        select: function (event, ui) {
            window.location.href = '/' + ui.item.state + '/team/' + ui.item.urlname + '/' + ui.item.year;
        }
        })
        .data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<a>" + item.label + "<br>" + item.loc + "</a>")
            .appendTo(ul);
    };

    $('.stateselect').click(function () {
        $('.stateselect').removeClass('btn-success').addClass('btn-primary');
        $(this).removeClass('btn-primary').addClass('btn-success');
    });
});
