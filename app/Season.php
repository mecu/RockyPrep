<?php

namespace App;

use Eloquent;
use URL;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Support\Collection;

/**
 * Class Season
 */
class Season extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'season';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['teamid', 'levelid', 'year', 'confid', 'coachid', 'levelid_state'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'year' => 'int',
        'levelid' => 'int',
        'levelid_state' => 'int',
        'srs' => 'float',
        'srs_avgpd' => 'float',
        'srs_oppavg' => 'float',
        'srs_numgames' => 'int',
        'rpi' => 'float',
        'rpi_wp' => 'float',
        'rpi_owp' => 'float',
        'rpi_oowp' => 'float',
    ];

    private $lowerbonus = true;

    public function getCutoff()
    {
        return Cutoff::where(['year' => $this->year, 'levelid' => $this->levelid])->first();
    }

    /**
     * @return bool
     */
    public function isPlayDown(): bool
    {
        return $this->level_alter < 0;
    }

    /**
     * @return bool
     */
    public function isPlayUp(): bool
    {
        return $this->level_alter > 0;
    }

    public function getRPI(): array
    {
        return [
            'RPI' => $this->rpi,
            'wp' => $this->rpi_wp,
            'owp' => $this->rpi_owp,
            'oowp' => $this->rpi_oowp,
        ];
    }

    public function getCRPI(): array
    {
        return [
            'CRPI' => $this->crpi,
            'cwp' => $this->crpi_wp,
            'cowp' => $this->crpi_owp,
            'coowp' => $this->crpi_oowp,
        ];
    }

    public function getSRS(): array
    {
        return [
            'SRS' => $this->srs,
            'avgpd' => $this->srs_avgpd,
            'oppavg' => $this->srs_oppavg,
            'numgames' => $this->srs_numgames,
        ];
    }

    /**
     * @param bool|false $realValue
     *
     * @return \App\Level|int|null
     */
    public function getLevelid($realValue = false)
    {
        if (!$realValue && empty($this->levelid)) {
            return 55;
        }
        if ($realValue && empty($this->levelid)) {
            return null;
        }

        return $this->levelid;
    }

    /**
     * @param \App\Level $level
     */
    public function setLevel(Level $level)
    {
        $this->levelid = $level;
    }

    /**
     * @param Year $year
     */
    public function setYear(Year $year)
    {
        $this->year = $year->year;
        $this->thisyear = new Year($this->year);
    }

    public function url(): string
    {
        return URL::route('team', [
            'state' => $this->team->state->urlname,
            'team' => $this->team->urlname,
            'year' => $this->year,
        ]);
    }

    public function thisYear(): Year
    {
        return new Year($this->year);
    }

    public function getSchedule(bool $includeDeleted = false): Collection
    {
        $away = Game::where('awayteam', $this->teamid)
            ->whereBetween('date', [$this->thisYear()->start, $this->thisYear()->end]);

        if ($includeDeleted) {
            $away->withTrashed();
        }

        $home = Game::where('hometeam', $this->teamid)
            ->whereBetween('date', [$this->thisYear()->start, $this->thisYear()->end])
            ->union($away)
            ->orderBy('date');

        if ($includeDeleted) {
            $home->withTrashed();
        }

        return $home->get();
    }

    public function schedule()
    {
        return $this->homeSchedule()->getResults()->merge($this->awaySchedule()->getResults());
    }

    public function homeSchedule(): HasMany
    {
        return $this->hasMany('App\Game', 'hometeam', 'teamid');
    }

    public function awaySchedule(): HasMany
    {
        return $this->hasMany('App\Game', 'awayteam', 'teamid');
    }

    /**
     * @param Year $year
     * @param Team $teamExcluded
     *
     * @return int
     */
    public function getWins(Year $year = null, Team $teamExcluded = null): int
    {
        $wins = 0;
        /** @var Game $game */
        foreach ($this->getSchedule() as $game) {
            if ($year !== null && $game->date->format('Y-m-d') >= $year->getEnd()) {
                continue;
            }
            if ($teamExcluded !== null && $teamExcluded->teamid === $game->opponent($this->team)->teamid) {
                continue;
            }
            if ($game->winner === $this->team->teamid) {
                ++$wins;
            }
        }

        return $wins;
    }

    /**
     * @param Year $date
     * @param Team $teamExcluded
     *
     * @return int
     */
    public function getLoss(Year $date = null, Team $teamExcluded = null): int
    {
        $loss = 0;
        foreach ($this->getSchedule() as $game) {
            if ($date !== null && $game->date->format('Y-m-d') >= $date->getEnd()) {
                continue;
            }
            if ($teamExcluded !== null && $teamExcluded->teamid === $game->opponent($this->team)->teamid) {
                continue;
            }
            if ($game->winner !== null && $game->winner !== $this->team->teamid && $game->winner !== 0) {
                ++$loss;
            }
        }

        return $loss;
    }

    public function getConferenceWins(): int
    {
        $conferenceWins = 0;
        foreach ($this->getSchedule() as $game) {
            if ($game->winner === $this->team->teamid && $game->confid) {
                ++$conferenceWins;
            }
        }

        return $conferenceWins;
    }

    public function getConferenceLoss(): int
    {
        $conferenceLosses = 0;
        foreach ($this->getSchedule() as $game) {
            if ($game->winner !== null && $game->winner !== $this->team->teamid && $game->winner !== 0 && $game->confid) {
                ++$conferenceLosses;
            }
        }

        return $conferenceLosses;
    }

    public function getTotalGames(Year $year): int
    {
        return $this->getSchedule()->filter(function (Game $game) use ($year) {
            return $game->date <= $year->getEnd() && $game->winner !== null;
        })->count();
    }

    /**
     * Gives a list of opponent (season objects) for this season
     *
     * @param bool $includeDeleted
     *
     * @return Collection opponents
     */
    public function getOpponents(bool $includeDeleted = false): Collection
    {
        // Get the schedule, and then for each game, get the opponent
        $schedule = $this->getSchedule($includeDeleted);

        $opponents = new Collection();
        foreach ($schedule as $game) {
            $opponents[] = $game->opponent($this->team);
        }

        return $opponents;
    }

    /**
     * Generates a team array for this team as their "reach"
     * "reach" is all their opponents and their opponent's opponents
     * Only teams not in the state of this team included
     * Rating Percentage Index
     * http://www.collegerpi.com/rpifaq.html
     *
     * @return array
     */
    public function getReach(): array
    {
        $games = $this->getSchedule();
        $opponents = [];

        /** @var Game $game */
        foreach ($games as $game) {
            $opponent = $game->opponent($this->team);

            if ($opponent->stateid !== $this->team->stateid) {
                $opponents[] = $opponent;
            }
        }

        $rpi = [];

        foreach ($opponents as $opponent) {
            /* @var Season $oppSeason */
            $oppSeason = Season::ofTeam($opponent)->ofYear($this->thisYear())->first();

            foreach($oppSeason->getSchedule() as $game) {
                $oppOpp = $game->opponent($opponent);
                if ($oppOpp->stateid !== $this->team->stateid) {
                    $opponents[] = $oppOpp;
                }
                $rpi[] = $game->opponent($opponent);
            }
        }

        return $opponents + $rpi;
    }

    /**
     * @param int $levelId
     * @param bool|false $bonus
     *
     * @return float
     */
    public static function getReductionFactor(?int $levelId = null, bool $bonus = false): float
    {
        if ($bonus) {
            switch ($levelId) {
                case 44:
                    $levelId = 55;
                    break;
                case 33:
                    $levelId = 44;
                    break;
                case 22:
                    $levelId = 33;
                    break;
                case 11:
                    $levelId = 22;
                    break;
                case 7:
                    $levelId = 11;
                    break;
                case 6:
                    $levelId = 7;
                    break;
                case null:
                    return 1.0;
            }
        }

        switch ($levelId) {
            case 66:
                return 1.00;
            case 55:
                return 1.00;
            case 44:
                return 0.90;
            case 33:
                return 0.75;
            case 22:
                return 0.60;
            case 11:
                return 0.45;
            case 9:
                return 0.40;
            case 8:
                return 0.35;
            case 7:
                return 0.30;
            case 6:
                return 0.15;
            case null:
            default:
                return 1.00;
        }
    }

    /**
     * Calculate WP (winning percentage)
     *
     * @param bool $reduce
     * @param bool $debug
     *
     * @return float
     */
    public function wp(bool $reduce = true, bool $debug = false): float
    {
        if ($debug) {
            echo('WP for ' . $this->team->name . ':');
        }
        $totalGames = $this->getTotalGames($this->thisYear());
        if ($totalGames === 0 || $this->levelid_state === null) {
            return 0.000;
        }

        $wp = round($this->getWins() / $totalGames, 4);

        $reduction = $reduce ? static::getReductionFactor($this->levelid_state) : 1.0;

        if ($debug) {
            echo('   W-L: ' . $this->getWins() . '-' . ($totalGames - $this->getWins()) . ' WP:' . $wp . ' Reduction:' . $reduction);
        }

        return round($wp * $reduction, 4);
    }

    /**
     * Calculate WP (winning percentage) with exclusion
     *
     * @param Team $teamExcluding
     * @param bool $debug
     *
     * @return float
     */
    public function wpExclude(Team $teamExcluding, bool $debug = false): float
    {
        $schedule = $this->getSchedule();
        $wins = 0;
        $totalGames = 0;
        if ($debug) {
            echo($this->team->name . ' WP_exclude calc:');
        }

        foreach ($schedule as $game) {
            $opponent = $game->opponent($this->team);

            if ($debug) {
                echo('    ' . $opponent->name . ' game:');
            }
            if ($opponent->teamid === $teamExcluding->teamid || !$game->hasWinner()) {
                continue;
            }

            ++$totalGames;

            if ($game->status($opponent) === 'LOSE') {
                if ($debug) {
                    echo('    ' . $opponent->name . ' wins!');
                }
                ++$wins;
            } elseif ($game->status($opponent) === 'TIE') {
                if ($debug) {
                    echo('    ' . $opponent->name . ' tie????!');
                }
                $wins += 0.5;
            }
        }

        if ($totalGames === 0) {
            return 0.000;
        }
        if ($debug) {
            echo('    ' . $teamExcluding->name . ' WP_exclude record ' . $wins . '-' . ($totalGames - $wins));
        }

        return round($wins / $totalGames, 4);
    }

    /**
     * Calculate OWP (opponents winning percentage)
     *
     * @param $debug |false
     *
     * @return float
     */
    public function owp($debug = false): float
    {
        /** @var Game[] $schedule */
        $schedule = $this->getSchedule();

        $owp = 0;
        $totalGames = 0;
        $bonus = true;

        foreach ($schedule as $gameID => $game) {
            if (!$game->hasWinner()) {
                continue;
            }

            $opp = $game->opponent($this->team);

            if ($debug) {
                echo('Calculating OWP for ' . $opp->name . ':');
            }

            /** @var Season $oppSeason */
            $oppSeason = Season::firstOrCreate([
                'teamid' => $opp->teamid,
                'year' => $this->thisYear()->year,
            ]);

            ++$totalGames;

            $wpExclude = $oppSeason->wpExclude($this->team, $debug);
            if ($bonus && $oppSeason->getLevelid(true) < $this->getLevelid(true)) {
                $reduction = static::getReductionFactor($oppSeason->levelid_state, true);
                $bonus = false;
            } else {
                $reduction = static::getReductionFactor($oppSeason->levelid_state, false);
            }

            $owp += $wpExclude * $reduction;
            if ($debug) {
                echo($wpExclude * $reduction);
            }
        }
        if ($totalGames === 0) {
            return 0.000;
        }

        if ($debug) {
            echo($owp);
            echo($totalGames);
            echo(round($owp / $totalGames, 4));
        }

        return round($owp / $totalGames, 4);
    }

    /**
     * Calculate OOWP (opponents opponents winning percentage)
     *
     * @param $debug |false
     *
     * @return float
     */
    public function oowp($debug = false): float
    {
        $schedule = $this->getSchedule();

        $oowp = 0;
        $totalGames = 0;

        if ($debug) {
            echo('Calculating OOWP for ' . $this->team->name . ':');
        }

        foreach ($schedule as $game) {
            if (!$game->hasWinner()) {
                continue;
            }

            $opponent = $game->opponent($this->team);

            /** @var Season $oppSeason */
            $oppSeason = Season::ofTeam($opponent)->ofYear($this->thisYear())->first();

            $opponentSchedule = $oppSeason->getSchedule();

            $opponentBonus = true;
            if ($debug) {
                echo('    First Opponent: ' . $opponent->name . ':');
            }

            foreach ($opponentSchedule as $opponentGame) {
                if (!$opponentGame->hasWinner()) {
                    continue;
                }

                $opponentOpponent = $opponentGame->opponent($opponent);

                /** @var Season $oppOppSeason */
                $oppOppSeason = Season::ofTeam($opponentOpponent)->ofYear($this->thisYear())->first();

                ++$totalGames;

                if ($debug) {
                    echo('        Second Opponent: ' . $opponentOpponent->name . ':');
                }

                $oppOppSeasonWP = $oppOppSeason->wp(false, false);
                if ($opponentBonus && $oppOppSeason->getLevelid(true) < $oppSeason->getLevelid(true)) {
                    $reduction = static::getReductionFactor($oppOppSeason->levelid_state, true);
                    $opponentBonus = false;
                    if ($debug) {
                        echo('            Level: ' . $oppOppSeason->levelid_state . ' BONUS');
                    }
                } else {
                    $reduction = static::getReductionFactor($oppOppSeason->levelid_state, false);
                    if ($debug) {
                        echo('            Level: ' . $oppOppSeason->levelid_state);
                    }
                }

                $oowp += $oppOppSeasonWP * $reduction;
            }
        }
        if ($totalGames === 0) {
            return 0.000;
        }

        return round($oowp / $totalGames, 4);
    }

    /**
     * @param int $levelId
     * @param bool|false $bonus
     *
     * @return float
     */
    public static function cRPIGameValue(int $levelId = null, bool $bonus = false): float
    {
        if ($levelId === null) {
            return 1.0;
        }

        if ($bonus) {
            switch ($levelId) {
                case 44:
                    $levelId = 55;
                    break;
                case 33:
                    $levelId = 44;
                    break;
                case 22:
                    $levelId = 33;
                    break;
                case 11:
                    $levelId = 22;
                    break;
                case 7:
                    $levelId = 11;
                    break;
                case 6:
                    $levelId = 7;
                    break;
            }
        }
        /*
         * 6-man	1.000
         * 8-man	1.150
         * 1A	    1.323
         * 2A	    1.521
         * 3A	    1.749
         * 4A	    2.011
         * 5A   	2.313
         */
        switch ($levelId) {
            case 55:
                return 2.313;
            case 44:
                return 2.011;
            case 33:
                return 1.749;
            case 22:
                return 1.521;
            case 11:
                return 1.323;
            case 7:
                return 1.150;
            default:
                return 1.000;
        }
    }

    /**
     * Calculate CHSAA WP (winning percentage)
     *
     * @param bool $debug
     *
     * @return float
     */
    public function cwp(bool $debug = false): float
    {
        $this->setLowerBonus(true);

        $totalGames = $this->getTotalGames($this->thisYear());
        if ($totalGames === 0 || $this->levelid === null) {
            return 0.000;
        }

        $schedule = $this->getSchedule();
        $winPoints = 0.0;
        /** @var Game $game */
        foreach ($schedule as $game) {
            // Only get points if you win
            if ($game->isWinner($this->team)) {
                // Get game value points based on the opponent level
                /** @var Season $oppSeason */
                $oppSeason = Season::ofTeam($game->opponent($this->team))->ofYear($this->thisYear())->first();

                if ($oppSeason->levelid < $this->levelid && $this->hasLowerBonus()) {
                    $winPoints += self::cRPIGameValue($oppSeason->levelid, $this->hasLowerBonus());
                    $this->setLowerBonus(false);
                } else {
                    $winPoints += self::cRPIGameValue($oppSeason->levelid);
                }
            }
        }

        // Total possible points is the number of games * your value
        $totalGameValuePoints = $totalGames * self::cRPIGameValue($this->levelid);

        $wp = round($winPoints / $totalGameValuePoints, 4);
        if ($debug) {
            $wins = $this->getWins();
            $loss = $totalGames - $wins;
            echo("<tr><td>W-L:</td><td>$wins-$loss</td><td>%:</td><td>$wp</td>");
        }

        return $wp;
    }

    /**
     * Calculate CHSAA WP (winning percentage) with exclusion
     *
     * @param Team $teamExcluding
     * @param bool $debug
     *
     * @return float
     */
    public function cwpExclude(Team $teamExcluding, bool $debug = false): float
    {
        $this->setLowerBonus(true);
        $schedule = $this->getSchedule();
        $wins = 0;
        $winPoints = 0.0;
        $totalGames = 0;
        if ($debug) {
            echo($this->team->name . ' CWP_exclude calc:');
        }

        foreach ($schedule as $game) {
            $opponent = $game->opponent($this->team);

            if ($debug) {
                echo('    ' . $opponent->name . ' game:');
            }
            if ($opponent->teamid === $teamExcluding->teamid || !$game->hasWinner()) {
                continue;
            }

            ++$totalGames;

            // Only get points if you win
            if ($game->isWinner($this->team)) {
                ++$wins;
                // Get game value points based on the opponent level
                /** @var Season $oppSeason */
                $oppSeason = Season::ofTeam($game->opponent($this->team))->ofYear($this->thisYear())->first();

                if ($oppSeason->levelid < $this->levelid && $this->hasLowerBonus()) {
                    $winPoints += self::cRPIGameValue($oppSeason->levelid, $this->hasLowerBonus());
                    $this->setLowerBonus(false);
                } else {
                    $winPoints += self::cRPIGameValue($oppSeason->levelid);
                }
            }
        }

        if ($totalGames === 0) {
            if ($debug) {
                echo('    ' . $teamExcluding->name . ' WP_exclude record 0-0 (no games)');
            }
            return 0.000;
        }
        if ($debug) {
            echo('    ' . $teamExcluding->name . ' WP_exclude record ' . $wins . '-' . ($totalGames - $wins));
        }

        // Total possible points is the number of games * your value
        $totalGameValuePoints = $totalGames * self::cRPIGameValue($this->levelid);

        return round($winPoints / $totalGameValuePoints, 4);
    }

    /**
     * Calculate OWP (opponents winning percentage)
     *
     * @param $debug |false
     *
     * @return float
     */
    public function cowp(bool $debug = false): float
    {
        /** @var Game[] $schedule */
        $schedule = $this->getSchedule();

        $owp = 0;
        $totalGames = 0;

        foreach ($schedule as $game) {
            if (!$game->hasWinner()) {
                continue;
            }

            $opp = $game->opponent($this->team);

            if ($debug) {
                echo('Calculating OWP for ' . $opp->name . ':');
            }

            /** @var Season $oppSeason */
            $oppSeason = Season::ofTeam($game->opponent($this->team))->ofYear($this->thisYear())->first();

            ++$totalGames;

            $owp += $oppSeason->cwpExclude($this->team, $debug);
        }
        if ($totalGames === 0) {
            return 0.000;
        }

        if ($debug) {
            echo $owp, $totalGames, round($owp / $totalGames, 4);
        }

        return round($owp / $totalGames, 4);
    }

    /**
     * Calculate OOWP (opponents opponents winning percentage)
     *
     * @param bool $debug |false
     *
     * @return float
     */
    public function coowp(bool $debug = false): float
    {
        $schedule = $this->getSchedule();

        $oowp = 0;
        $totalGames = 0;

        if ($debug) {
            echo('Calculating OOWP for ' . $this->team->name . ':');
        }

        foreach ($schedule as $game) {
            if (!$game->hasWinner()) {
                continue;
            }

            $opponent = $game->opponent($this->team);

            /** @var Season $oppSeason */
            $oppSeason = Season::ofTeam($opponent)->ofYear($this->thisYear())->first();

            $opponentSchedule = $oppSeason->getSchedule();

            if ($debug) {
                echo('    First Opponent: ' . $opponent->name . ':');
            }

            foreach ($opponentSchedule as $opponentGame) {
                if (!$opponentGame->hasWinner()) {
                    continue;
                }

                ++$totalGames;

                // Before 2018, a Colorado team's whose opponent was not Colorado gave a 0.500
                // Starting 2018, New Mexico, Arizona, Utah, Wyoming, Nebraska get real values, otherwise 0.500
                // Non-Colorado Teams get 0.5000 by rule
                if ($this->year < 2018 && $opponent->stateid != 6
                    || !in_array($opponent->stateid, [3, 6, 27, 31, 44, 51])) {
                    $oowp += 0.500;

                    continue;
                }

                $opponentOpponent = $opponentGame->opponent($opponent);

                if ($debug) {
                    echo('        Second Opponent: ' . $opponentOpponent->name . ':');
                }

                /** @var Season $oppOppSeason */
                $oppOppSeason = Season::ofTeam($opponentOpponent)->ofYear($this->thisYear())->first();

                $oowp += $oppOppSeason->cwp();
            }

        }
        if ($totalGames === 0) {
            return 0.000;
        }

        return round($oowp / $totalGames, 4);
    }

    /**
     * Calculate the margin of victory (MOV) for this season
     */
    public function calcMOV()
    {
        $this->srs_avgpd = 0.0;

        $games = $this->getSchedule();

        if ($games->count() > 0) {
            $pointsFor = 0;
            $pointsAgainst = 0;

            foreach ($games as $game) {
                //see if this team is the home or away
                if ($game->hometeam === $this->teamid) {
                    $pointsFor += $game->homescore;
                    $pointsAgainst += $game->awayscore;
                } else {
                    $pointsFor += $game->awayscore;
                    $pointsAgainst += $game->homescore;
                }
            }
            $this->srs_avgpd = round(($pointsFor - $pointsAgainst) / $games->count(), 1);
        }
    }

    public function hasLowerBonus(): bool
    {
        return $this->lowerbonus;
    }

    public function setLowerBonus(bool $lowerBonus)
    {
        $this->lowerbonus = $lowerBonus;
    }

    public function team(): HasOne
    {
        return $this->hasOne('App\Team', 'teamid', 'teamid');
    }

    public function level(): HasOne
    {
        return $this->hasOne('App\Level', 'levelid', 'levelid');
    }

    public function levelState(): HasOne
    {
        return $this->hasOne('App\Level', 'levelid', 'levelid_state');
    }

    public function year(): HasOne
    {
        return $this->hasOne('App\Year', 'year', 'year');
    }

    public function conference(): HasOne
    {
        return $this->hasOne('App\Conference', 'confid', 'confid');
    }

    /**
     * Scope a query to only include seasons of year
     *
     * @param Builder $query
     * @param Year    $year
     *
     * @return Builder
     */
    public function scopeOfYear(Builder $query, Year $year): Builder
    {
        return $query->where('year', $year->year);
    }

    /**
     * Scope a query to only include seasons of team
     *
     * @param Builder $query
     * @param Team    $team
     *
     * @return Builder
     */
    public function scopeOfTeam(Builder $query, Team $team): Builder
    {
        return $query->where('teamid', $team->teamid);
    }
}
