<?php namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;

class Conference extends Eloquent
{
    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'confid';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'conference';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function teamList(Year $year): Collection
    {
        return Season::where('confid', $this->confid)
            ->where('year', $year->year)
            ->with('team')
            ->get();
    }
}
