<?php namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Level
 */
class Level extends Eloquent
{
    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'levelid';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'level';

    /*
     * Sets that only the levelid cannot be mass assigned
     */
    protected $guarded = ['levelid'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function state(): BelongsTo
    {
        return $this->belongsTo('App\State', 'stateid', 'stateid');
    }
}
