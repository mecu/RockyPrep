<?php namespace App;

use Eloquent;
use Iatstuti\Database\Support\NullableFields;

/**
 * Class Team
 */
class Team extends Eloquent
{
    use NullableFields;
    
    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'teamid';

    /*
     * Sets that only the teamid cannot be mass assigned
     */
    protected $guarded = ['teamid'];

    protected $nullable = [
        'mascot',
        'city',
        'color1',
        'color2',
        'address',
        'zip',
        'lat',
        'lng',
        'twitter',
        'maxpreps_name',
        'facebook_id',
        'facebook',
        'website',
        'hudl',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'team';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getHelmet()
    {
        return is_file("img/helmet/{$this->urlname}.gif") ? "img/helmet/{$this->urlname}.gif" : false;
    }

    public function hasColors()
    {
        return isset($this->color1) && isset($this->color2);
    }

    public function getCity($micro = false)
    {
        return $micro ? "<span itemprop='locality'>{$this->city}</span>" : $this->city;
    }

    public function isJV()
    {
        return $this->jv;
    }

    public function fullAddress()
    {
        return "{$this->name} High School, {$this->address}, {$this->city}, {$this->state->abbr}, {$this->zip}";
    }

    public function googleMap()
    {
        return str_replace(' ', '+', '//maps.googleapis.com/maps/api/staticmap?center=' . $this->fullAddress() . '&amp;zoom=15&amp;size=180x180&amp;maptype=hybrid&amp;sensor=false&amp;markers=' . $this->fullAddress());
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'stateid', 'stateid');
    }

    public function seasons()
    {
        return $this->hasMany('App\Season', 'teamid', 'teamid');
    }

    public function getHudl($link = true)
    {
        if ($link && $this->hudl !== null) {
            return '<a href="https://www.hudl.com/team/' . $this->hudl . '/highlights" target="_blank" rel="noopener">
            <img src="/img/hudl.png" height="50" width="50" alt="Hudl Highlights" title="' . $this->name . ' Hudl Video Highlights (may not be for current year)" style="padding:0;display:inline">
            </a>';
        }

        return $this->hudl;
    }

    public function getFacebook($link = true)
    {
        if ($link && $this->facebook !== null) {
            return '<a href="https://www.facebook.com/' . $this->facebook . '" target="_blank" rel="noopener">
            <img src="/img/facebook.png" alt="Official Facebook Page" height="50" width="50" title="' . $this->name . ' Official Facebook Page" style="padding:0;display:inline">
            </a>';
        }

        return $this->facebook;
    }

    public function getTwitter($img = true)
    {
        if ($img && $this->twitter !== null) {
            return '<a href="https://twitter.com/' . $this->twitter . '" target="_blank" rel="noopener">
            <img src="/img/twitter.png" height="50" width="50" alt="Official Twitter" title="' . $this->name . ' Official Twitter" style="padding:0;display:inline">
            </a>';
        }

        return $this->twitter;
    }

    public function getWebsite($link = true)
    {
        if ($link && $this->website !== null) {
            return '<a href="http://' . $this->website . '">Official Website</a>';
        }

        return $this->website;
    }
}
