<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CalcRPIforTeam implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * Create a new job instance.
     *
     * @param int $teamid
     * @param string $year
     */
    public function __construct(int $teamid, string $year)
    {
        $this->teamid = $teamid;
        $this->year = $year;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        \Artisan::call('Calc:RPI', [
            'season' => $this->year,
            'team' => $this->teamid,
        ]);
    }
}
