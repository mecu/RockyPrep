<?php namespace App\Jobs;

use App\Game;
use App\Helpers\Helpers;
use App\Season;
use App\Social;
use App\State;
use App\Team;
use App\Tweet;
use App\Year;
use Carbon;
use Exception;
use Twitter;
use URL;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class TweetGame extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /* @var Game $game */
    public $game;

    /* @var State $state */
    public $state;

    /**
     * Create a new job instance.
     *
     * @param Game $game
     * @param State $state
     */
    public function __construct(Game $game, State $state)
    {
        $this->game = $game;
        $this->state = $state;
    }

    /**
     * Handle the command
     */
    public function handle()
    {
        $now = new \Carbon\Carbon();
        $threeDaysAgo = $now->subDays(3);
        // If the game wasn't 3 days ago or less, we skip it (relevancy and all that jazz)
        if ($this->game->date->lt($threeDaysAgo)) {
            return 'Old game';
        }

        // Check for terrible scores
        if ($this->game->homescore > 99 || $this->game->awayscore > 99) {
            return 'Bad scores';
        }


        // Get the AccessToken and AccessTokenSecret for this sport/state combo
        $social = Social::where(['stateid' => $this->state->stateid])->firstOrFail();
        if ($social->twitter_active == 0) {
            // This state is currently blocked on Twitter, sigh
            return false;
        }

        try {
            URL::forceRootUrl('https://rockyprep.com');
            \Twitter::reconfig([
                'token' => $social->accesstoken,
                'secret' => $social->accesstokensecret,
            ]);

            $message = $this->generateTweet();
            $response = Twitter::postTweet(['status' => $message]);

            $tweet = new Tweet();
            $tweet->gameid = $this->game->gameid;
            $tweet->tweet = $response->id;
            $tweet->save();

            $this->delete();
        } catch (\RuntimeException $e) {
            if ($e->getMessage() === '[326] To protect our users from spam and other malicious activity, this account is temporarily locked. Please log in to https://twitter.com to unlock your account.') {
                $social->twitter_active = 0;
                $social->save();
            }
        } catch (\Exception $e) {
            $logs = Twitter::logs();
            foreach ($logs as $log) {
                if ($log === 'ERROR_CODE : 187') {
                    return true;
                }
            }
            throw $e;
        }

        return true;
    }

    /**
     * @return string
     */
    private function generateTweet()
    {
        /* @var Team $winner */
        $winner = $this->game->getWinnerObject();
        $winnerTweet = substr($winner->name, 0, 30);

        /* @var Season $winnerSeason */
        $winnerSeason = Season::ofTeam($winner)->ofYear(new Year())->with('team', 'level')->first();
        $winnerTweet .= ' ' . URL::route('team', ['state' => $winner->state->urlname, 'team' => $winner->urlname, 'year' => Helpers::currentYear()]);
        $winLevel = '';
        if ($winnerSeason !== null && $winnerSeason->level !== null) {
            $winLevel = $winnerSeason->level->name;
        }

        if ($winner->getTwitter(false) !== null) {
            $winnerTweet .= ' @' . $winner->getTwitter(false) . ' ';
        }

        /* @var Team $loser */
        $loser = $this->game->getLoserObject();
        $loserTweet = substr($loser->name, 0, 30);

        /* @var Season $loserSeason */
        $loserSeason = Season::ofTeam($loser)->ofYear(new Year())->with('team', 'level')->first();
        $loserTweet .= ' ' . URL::route('team', ['state' => $loser->state->urlname, 'team' => $loser->urlname, 'year' => Helpers::currentYear()]);
        $loseLevel = '';
        if ($loserSeason !== null && $loserSeason->level !== null) {
            $loseLevel = $loserSeason->level->name;
        }

        if ($loser->getTwitter(false) !== null) {
            $loserTweet .= ' @' . $loser->getTwitter(false) . ' ';
        }

        $levelMessage = " #$winLevel/#$loseLevel";
        if ($winLevel === $loseLevel) {
            $levelMessage = " #$winLevel";
        }
        if ($winLevel) {
            $levelMessage = " #$loseLevel";
        }
        if ($loseLevel) {
            $levelMessage = '';
        }

        $date = ' on ' . $this->game->date->format('D M j');

        $box = $this->game->hasBoxscore() ? ' #boxscore' : '';

        $score = $winner->teamid === $this->game->home->teamid ?
            $this->game->homescore . '-' . $this->game->awayscore :
            $this->game->awayscore . '-' . $this->game->homescore;

        $verb = ' beat ';

        $sport = ' #football';

        // Craft actual message
        $message = $winnerTweet . $verb . $loserTweet . ', ' . $score . $date . $box . $levelMessage . $sport;

        if ($this->game->playoff) {
            $message .= ' #playoff';
        }
        if ($this->game->isOT()) {
            $message .= ' #ot';
        }

        if ((int)$this->state->stateid === 5) {
            $message .= ' #capreps';
        } elseif ((int)$this->state->stateid === 6) {
            $message .= ' #copreps';
        }

        return $message;
    }

    /*
    * The job failed to process.
    *
    * @param  Exception  $exception
    * @return void
    */
    public function failed(Exception $exception)
    {
        \Log::error('TweetGame job failed.', [
            'exception' => $exception,
            'gameid' => $this->game->gameid,
            'stateid' => $this->state->stateid,
        ]);
    }
}
