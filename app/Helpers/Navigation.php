<?php declare(strict_types=1);
namespace App\Helpers;

use App\Level;
use App\State;
use App\Team;
use App\Season;
use App\Year;
use Illuminate\Database\Eloquent\Collection;

class Navigation
{
    /**
     * @param State $state
     * @return array
     */
    public static function menuYearsShowAll(State $state)
    {
        $teams = Team::select('teamid')->where(['stateid' => $state->stateid])->get();
        $seasons = Season::select('year')->distinct()->whereIn('teamid', $teams)->where('year', '<=', Helpers::currentYear())->get();

        return $seasons->sortBy('year')->pluck('year')->toArray();
    }

    /**
     * @param Team $team
     * @return array
     */
    public static function menuYearsTeam(Team $team)
    {
        $seasons = Season::select('year')->distinct()->ofTeam($team)->where('year', '<=', Helpers::currentYear())->get();

        return $seasons->sortBy('year')->pluck('year')->toArray();
    }

    /**
     * Levels (1A, 2A, ..., 5A) for the provided state and year
     *
     * @param State $state
     * @param Year $year
     * @return Collection
     */
    public static function menuLevels(State $state, Year $year)
    {
        $teams = Team::select('teamid')->where(['stateid' => $state->stateid])->get();
        $seasons = Season::select('levelid_state')->distinct()
            ->whereIn('teamid', $teams)
            ->where('year', $year->year)
            ->where('levelid_state', '!=', 0)
            ->get();

        $levels = Level::whereIn('levelid', $seasons)->get();

        return $levels->sortBy('order');
    }

    /**
     * @param State $state
     *
     * @return array
     */
    public static function menuYearsSchedule(State $state)
    {
        $teams = Team::select('teamid')->where(['stateid' => $state->stateid])->get();
        $seasons = Season::select('year')->distinct()->whereIn('teamid', $teams)->where('year', '<=', Helpers::currentYear())->get();

        return $seasons->sortBy('year')->pluck('year')->toArray();
    }
}
