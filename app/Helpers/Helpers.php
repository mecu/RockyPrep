<?php

namespace App\Helpers;

class Helpers
{
    public static function currentYear(): int
    {
        if ((int)date('n') < 6) {
            return (int)(date('Y') - 1);
        }

        return (int)date('Y');
    }
}
