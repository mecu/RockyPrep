<?php namespace App\Helpers;

use App\Game;
use App\Score;
use Illuminate\Http\JsonResponse;
use Response;

trait BoxScore
{
    /**
     * @param array $scores
     * @param Game $game
     * @return JsonResponse|mixed
     */
    public static function processScores(array $scores, Game $game)
    {
        list($awayScores, $homeScores) = self::convertScores($scores, $game);
        list($awayScores, $homeScores) = $game::validateOTScores($awayScores, $homeScores);
        if (!$game::validateScores($awayScores, $homeScores)) {
            return Response::json([
                'error'      => 'The scores failed validation. Most likely, the box scores did not equal the final scores provided.',
                'awayscore'  => $game->awayscore,
                'homescore'  => $game->homescore,
                'scores'     => $scores,
                'awayscores' => $awayScores,
                'homescores' => $homeScores,
            ], 400);
        }

        self::recordNewScores($scores, $game);

        return true;
    }

    /**
     * Save the Score data
     *
     * @param array $scores
     * @param Game $game
     */
    public static function recordNewScores(array $scores, Game $game)
    {
        foreach ($scores as $quarter => $numbers) {
            $score = new Score();
            $score->gameid = $game->gameid;
            $score->quarter = $quarter;
            $score->away = $numbers['away'];
            $score->home = $numbers['home'];
            $score->save();
        }
    }

    /**
     * Convert scores submitted from a form to away and home score arrays
     * @param array $scores
     * @param Game $game
     *
     * @return array
     */
    public static function convertScores(array $scores, Game $game): array
    {
        $returnScoresAway = [];
        $returnScoresHome = [];

        foreach ($scores as $quarter => $contestant) {
            $returnScoresAway[$quarter] = $contestant['away'];
            $returnScoresHome[$quarter] = $contestant['home'];
        }

        // Add on the final scores
        $returnScoresAway[] = $game->awayscore;
        $returnScoresHome[] = $game->homescore;

        return [$returnScoresAway, $returnScoresHome];
    }

    /**
     * http://stackoverflow.com/a/7696597/84162
     * @param array $haystack
     * @return mixed
     */
    public static function arrayRemoveEmpty(array $haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = self::arrayRemoveEmpty($haystack[$key]);
            }

            if (empty($haystack[$key]) && (int)$haystack[$key] !== 0) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }
}
