<?php
namespace App\Helpers;

use Carbon\Carbon;

class Pretty
{
    /**
     * Change date from 20xx-xx-xx to "Mon, Aug 7" if $short=TRUE, else longform: "Monday, August 7"
     * @param Carbon $date
     * @param bool $short
     * @return string
     */
    public static function prettydate(Carbon $date, $short = true)
    {
        if ($short) {
            return $date->format('D, M j');
        }

        return $date->format('l, F j, Y');
    }

    // *********************************************************************************************************************************
    // function to change time from 19:00:00 to "7:00 pm"
    // *********************************************************************************************************************************
    public static function prettytime($time, $date = null)
    {
        $hour = (int)substr($time, 0, 2);
        $minute = (int)substr($time, 3, 2);
        if ($hour !== 0) {
            if (date('I', $date)) {
                return date('g:i a', mktime($hour - 1, $minute, 0, 0, 0, 0));
            }
            return date('g:i a', mktime($hour, $minute, 0, 0, 0, 0));
        }

        return 'TBA';
    }
}
