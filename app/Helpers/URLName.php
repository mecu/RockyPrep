<?php

namespace App\Helpers;

use App\Team;

class URLName
{
    const BADCHARS = [' ', '\'', '.', '"', '\\', '/', '%', '!', '@', '&'];

    /**
     * Generate a URL for the team provided
     * name and state must be set prior to passing
     *
     * @param Team $team
     * @return bool
     */
    public static function generate(Team $team): bool
    {
        // Generate the url name
        $URLName = str_replace(self::BADCHARS, '_', strtolower($team->name . '_' . $team->state->abbr));
        $urlCheck = Team::where('urlname', $URLName)->first();

        // Test the url name
        if ($urlCheck !== null) {
            // Fix the url name with the city
            $URLName = str_replace(self::BADCHARS, '_', strtolower($team->name . '_' . $team->city . '_' . $team->state->abbr));
            $urlCheck = Team::where('urlname', $URLName)->first();
            /** @noinspection NotOptimalIfConditionsInspection */
            if ($urlCheck !== null) {
                return false;
            }
        }

        $team->urlname = $URLName;

        return true;
    }

    public static function provideString(string $string): string
    {
        return trim(str_replace(self::BADCHARS, '_', strtolower($string)));
    }
}
