<?php

namespace App\Helpers;

class Scraper
{
    /**
     * Grabs the requested $url
     * Will wait 10 seconds between failures
     * Will only try 6 times before giving up
     *
     * returns string $page of results
     * returns boolean false if failed
     *
     * @param $url
     * @return string
     */
    public static function fetch(string $url): string
    {
        $context = stream_context_create([
            'http' => [
                'user_agent' => 'RockyPrep schedule bot/v0.8',
                'ignore_errors' => true,
                'timeout' => 10,
            ],
        ]);

        $tries = 5;
        $page = false;

        while ($tries > 0 && !is_string($page)) {
            try {
                $page = file_get_contents($url, false, $context);
            } catch (\ErrorException $e) {
                // Something happened, let's pause and try again
                sleep(30);
            }

            if ($page === false) {
                sleep(10);
            }

            --$tries;
        }

        if ($tries < 1) {
            echo 'I failed to get the MaxPreps page.';
            throw new \RuntimeException('The page was not found.');
        }

        return $page;
    }
}
