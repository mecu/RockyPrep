<?php
namespace App\Helpers;

class Adblock
{
    public static function top()
    {
        return '<ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-5467966764772875"
                 data-ad-slot="7738199014"
                 data-ad-format="auto"></ins>';
    }

    public static function bottom()
    {
        return '<ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-5467966764772875"
                 data-ad-slot="4645131813"
                 data-ad-format="auto"></ins>';
    }

    public static function side()
    {
        return '<ins class="adsbygoogle"
                 style="display:inline-block;width:120px;height:600px"
                 data-ad-client="ca-pub-5467966764772875"
                 data-ad-slot="6157597419"></ins>';
    }
}
