<?php namespace App;

use Eloquent;

class Coach extends Eloquent
{
    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'coachid';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coach';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function seasons()
    {
        return $this->hasMany('App\Season', 'coachid', 'coachid');
    }
}
