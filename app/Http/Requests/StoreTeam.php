<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|string',
            'urlname'     => 'required|alpha_dash',
            'mascot'      => 'alpha',
            'city'        => 'string',
            'stateid'     => 'numeric|required',
            'color1'      => 'alpha_num',
            'color2'      => 'alpha_num',
            'zip'         => 'digits_between:5,9',
            'maxpreps_id' => 'required|alpha_dash',
        ];
    }
}
