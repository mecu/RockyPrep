<?php

namespace App\Http\Controllers;

use App\Game;
use App\Helpers\Navigation;
use App\Team;
use App\Season;
use App\State;
use App\Year;
use App\Wildcard;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;
use Redirect;
use Excel;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Illuminate\Support\Facades\Auth;

/**
 * Class TeamController
 *
 * @package App\Http\Controllers
 */
class TeamController extends Controller
{
    /**
     * @param State $state
     * @param Year $year
     *
     * @return View
     */
    public function showAll(State $state, Year $year)
    {
        $years = Navigation::menuYearsShowAll($state);

        return view('pages.showAllTeams', compact(['state', 'year', 'years']));
    }

    /**
     * @return View
     * @throws \Throwable
     */
    public function teamFinder()
    {
        if (!Cache::has('teamfinder')) {
            $render = view('pages.teamFinder', ['states' => State::all()])->render();

            Cache::put('teamfinder', $render, 1440);
        }

        return Cache::get('teamfinder');
    }

    /**
     * @param State $state
     * @param Team $team
     * @param Year $year
     *
     * @return View
     * @throws \ErrorException
     */
    public function team(State $state, Team $team, Year $year)
    {
        // See if the state in the URL matches what the state of the team should be, if not, redirect to proper URL
        if ($team->state->urlname <> $state->urlname) {
            return Redirect::route('team',
                ['state' => $team->state->urlname, 'team' => $team->urlname, 'year' => $year->year]);
        }

        $data['trust'] = Auth::check();

        $data['years'] = Navigation::menuYearsTeam($team);
        $data['team'] = $team;
        $data['state'] = $state;
        $data['year'] = $year;
        $data['yearDisplay'] = $year->year;
        $data['title'] = "{$year->year} {$team->name} {$team->mascot} High School Football Team Schedule :: ";

        $season = Season::ofTeam($team)->ofYear($year)->first();

        if ($season === null) {
            // They aren't in the season database, but should they be? If they have games, they should be!
            $season = new Season();
            $season->teamid = $team->teamid;
            $season->year = $year->year;
            $season->save();
            $data['season'] = $season;

            /** @var Collection $games */
            $games = Game::whereBetween('date', [$year->start, $year->end])
                ->where('awayteam', $team->teamid)
                ->orWhere('hometeam', $team->teamid)
                ->whereBetween('date', [$year->start, $year->end])
                ->get();

            if ($games->isEmpty()) {
                $data['nogames'] = true;

                return view('pages.team', $data);
            }
        }

        $data['season'] = $season;

        date_default_timezone_set($state->timezone);

        /* @var Game[] $teamSchedule */
        $teamSchedule = $season->getSchedule($data['trust']);

        if (count($teamSchedule) === 0) {
            $data['nogames'] = true;

            return view('pages.team', $data);
        }

        $data['teaminfo'] = $season->teaminfo !== null;

        if ($state->stateid === 6 &&
            $season->levelid > 9 &&
            $year->year > 2008 && $year->year < 2016
        ) {
            //Figure out the cutoff date for calculations
            $year->setEnd($season->getCutoff()->date->format('Y-m-d'));

            $data['coloradoFootball'] = true;

                    $points = 0;
                    $tieBreakTotal = 0;
                    $season->setLowerBonus(true);

            foreach ($teamSchedule as $key => $game) {
                $TBP_1 = 0;
                $TBP_2 = 0;
                $TBP_T = 0;
                $bonus = false;
                $tieBreakTotal = 0;
                $justify = '';

                $opponentSeason = $game->opponentSeason($season->team, $year);

                        if ($game->hasWinner()) {
                            if (!$game->playoff) {
                                if ($opponentSeason->levelid < $season->levelid && $season->hasLowerBonus()) {
                                    $season->setLowerBonus(false);

                            $bonus = true;

                            if ($opponentSeason->levelid) {
                                $justify = $opponentSeason->level->name;
                                if ($opponentSeason->team->isJV()) {
                                    $justify .= ' JV';
                                }
                                $justify .= ' team<br>counts ' . $opponentSeason->level->name;
                                if ($opponentSeason->team->isJV()) {
                                    $justify .= ' JV';
                                }
                                $justify .= ' with ';
                            } else {
                                $justify = 'unknown with ';
                            }
                        } else {
                            if ($opponentSeason->levelid) {
                                $justify = $opponentSeason->level->name;
                                if ($opponentSeason->team->isJV()) {
                                    $justify .= ' JV';
                                }
                                $justify .= ' with ';
                            } else {
                                $justify = 'unknown with ';
                            }
                        }

                        if ($opponentSeason->getWins($year) > 1) {
                            $justify .= $opponentSeason->getWins($year) . ' wins';
                        } elseif ($opponentSeason->getWins($year) === 0) {
                            $justify .= 'no wins';
                        } else {
                            $justify .= '1 win';
                        }

                        $gpoints = Wildcard::givepoints($season, $opponentSeason, $game, $year, $bonus);
                        $points += $gpoints;
                        $justify = "$gpoints points<br>$justify";
                    }

                } elseif ($game->hasPredict()) {
                    //Show a prediction, if exists
                    $data['schedule'][$key]['predict'] = $game->srs_predict;

                    if ($team->teamid === $game->srs_teamid) {
                        $data['schedule'][$key]['predict'] *= -1;
                    }
                }

                if (!$game->playoff && $game->status($team) === 'WIN') {
                    if ($opponentSeason->level === null) {
                        dd($opponentSeason->team);
                    }
                    $TBP_1 = Wildcard::givetiebreakpoints($opponentSeason->level);
                    if ($bonus) {
                        $TBP_1 += 0.5;
                    }
                    $TBP_2 = Wildcard::secondleveltiebreakpoints($opponentSeason, $year);
                    $TBP_T = $TBP_1 + $TBP_2;
                    $tieBreakTotal += $TBP_T;
                }

                if ($game->playoff) {
                    $justify = 'Playoff';
                    $TBP_1 = $TBP_2 = $TBP_T = '&nbsp;';
                }

                if ($game->deleted_at !== null) {
                    $points = $TBP_1 = $TBP_2 = $TBP_T = '&nbsp;';
                }

                $data['schedule'][$key]['justify'] = $justify;

                $data['schedule'][$key]['tb1'] = $TBP_1;
                $data['schedule'][$key]['tb2'] = $TBP_2;
                $data['schedule'][$key]['tbt'] = $TBP_T;

                $data['schedule'][$key]['game'] = $game;
            }

            if ($season->getTotalGames($year) > 0) {
                $data['scheduleSummary']['sumPoints'] = round($points / $season->getTotalGames($year), 3);
                $data['scheduleSummary']['sumTBP'] = round($tieBreakTotal / $season->getTotalGames($year), 3);
            }
        } else {
            $data['coloradoFootball'] = false;

            foreach ($teamSchedule as $key => $game) {
                $data['schedule'][$key]['predict'] = null;
                if ($game->hasPredict()) {
                    //Show a prediction, if exists
                    $data['schedule'][$key]['predict'] = $game->srs_predict;

                    if ($team->teamid === $game->srs_teamid) {
                        $data['schedule'][$key]['predict'] *= -1;
                    }
                }

                $data['schedule'][$key]['game'] = $game;
            }
        }

        /*
        //team season stats, if any
        $awayresult = $my_db->query("SELECT SUM(COALESCE(a1,0)) AS a1t, SUM(COALESCE(a2,0)) AS a2t, SUM(COALESCE(a3,0)) AS a3t, SUM(COALESCE(a4,0)) AS a4t, SUM(COALESCE(a5,0)) AS a5t, SUM(COALESCE(a6,0)) AS a6t, SUM(COALESCE(a7,0)) AS a7t, SUM(COALESCE(a8,0)) AS a8t, SUM(awayscore) at FROM `game` WHERE `awayteam` = $team->teamid AND `date` BETWEEN '$year->start' AND '$year->end' AND `sportid`=$sportid AND `winner` IS NOT NULL;");
        $homeresult = $my_db->query("SELECT SUM(COALESCE(h1,0)) AS h1t, SUM(COALESCE(h2,0)) AS h2t, SUM(COALESCE(h3,0)) AS h3t, SUM(COALESCE(h4,0)) AS h4t, SUM(COALESCE(h5,0)) AS h5t, SUM(COALESCE(h6,0)) AS h6t, SUM(COALESCE(h7,0)) AS h7t, SUM(COALESCE(h8,0)) AS h8t, SUM(homescore) AS ht FROM `game` WHERE `hometeam` = $team->teamid AND `date` BETWEEN '$year->start' AND '$year->end' AND `sportid`=$sportid AND `winner` IS NOT NULL;");
        $awayrow = $awayresult->fetchRow(MDB2_FETCHMODE_ASSOC);
        $homerow = $homeresult->fetchRow(MDB2_FETCHMODE_ASSOC);

        if ( $awayrow['a1t']>0 OR $homerow['h1t']>0 OR
            $awayrow['a2t']>0 OR $homerow['h2t']>0 OR
            $awayrow['a3t']>0 OR $homerow['h3t']>0 OR
            $awayrow['a4t']>0 OR $homerow['h4t']>0
            ) {
            echo '<h2>Season Statistics</h2>';
            echo '<h3>' . $team->name() . ' Season Points by Quarter:</h3>';

            $attributes = array( 'class' => 'text-center' );
            $table = new HTML_Table(array('class'=>'table table-hover table-condensed'));

            $ot = false;

            $contents = array('', '1', '2', '3', '4');
            if ( $awayrow['a5t']>0 OR $homerow['h5t']>0) {$ot = 1; $contents[] = 'OT';}
            if ( $awayrow['a6t']>0 OR $homerow['h6t']>0) {$ot = 2; $contents[] = 'OT2';}
            if ( $awayrow['a7t']>0 OR $homerow['h7t']>0) {$ot = 3; $contents[] = 'OT3';}
            if ( $awayrow['a8t']>0 OR $homerow['h8t']>0) {$ot = 4; $contents[] = 'OT4';}
            $contents[] = 'T';

            $table->addRow( $contents, $attributes, 'th' );

            $contents = array( 'Away', $awayrow['a1t'], $awayrow['a2t'], $awayrow['a3t'], $awayrow['a4t'] );
            if ( $ot >= 1 ) $contents[] = $awayrow['a5t'];
            if ( $ot >= 2 ) $contents[] = $awayrow['a6t'];
            if ( $ot >= 3 ) $contents[] = $awayrow['a7t'];
            if ( $ot >= 4 ) $contents[] = $awayrow['a8t'];
            $contents[] = $awayrow['at'];

            $table->addRow( $contents, $attributes, 'td' );

            $contents = array( 'Home', $homerow['h1t'], $homerow['h2t'], $homerow['h3t'], $homerow['h4t'] );
            if ( $ot >= 1) $contents[] = $homerow['h5t'];
            if ( $ot >= 2) $contents[] = $homerow['h6t'];
            if ( $ot >= 3) $contents[] = $homerow['h7t'];
            if ( $ot >= 4) $contents[] = $homerow['h8t'];
            $contents[] = $homerow['ht'];

            $table->addRow( $contents, $attributes, 'td' );

            $contents = array( 'Total', $awayrow['a1t']+$homerow['h1t'], $awayrow['a2t']+$homerow['h2t'], $awayrow['a3t']+$homerow['h3t'], $awayrow['a4t']+$homerow['h4t']);
            if ( $ot >= 1) $contents[] = $awayrow['a5t']+$homerow['h5t'];
            if ( $ot >= 2) $contents[] = $awayrow['a6t']+$homerow['h6t'];
            if ( $ot >= 3) $contents[] = $awayrow['a7t']+$homerow['h7t'];
            if ( $ot >= 4) $contents[] = $awayrow['a8t']+$homerow['h8t'];
            $contents[] = $awayrow['at']+$homerow['ht'];

            $table->addRow( $contents, $attributes, 'td' );

            $table->updateRowAttributes( 0, array( 'class' => 'row-b' ), TRUE );
            $table->updateRowAttributes( 3, array( 'class' => 'row-b' ), TRUE );

            echo '<div class="table-responsive">';
            $table->display();
            echo '</div>';

            echo '<h3>Opponent Season Points by Quarter:</h3>';
            $awayresult = $my_db->query("SELECT SUM(a1) AS a1t, SUM(a2) AS a2t, SUM(a3) AS a3t, SUM(a4) AS a4t, SUM(a5) AS a5t, SUM(a6) AS a6t, SUM(a7) AS a7t, SUM(a8) AS a8t, SUM(awayscore) AS at FROM `game` WHERE `sportid`=$sportid AND `hometeam` = $team->teamid AND `date` BETWEEN '$year->start' AND '$year->end';");
            $homeresult = $my_db->query("SELECT SUM(h1) AS h1t, SUM(h2) AS h2t, SUM(h3) AS h3t, SUM(h4) AS h4t, SUM(h5) AS h5t, SUM(h6) AS h6t, SUM(h7) AS h7t, SUM(h8) AS h8t, SUM(homescore) AS ht FROM `game` WHERE `sportid`=$sportid AND `awayteam` = $team->teamid AND `date` BETWEEN '$year->start' AND '$year->end';");

            $awayrow = $awayresult->fetchRow(MDB2_FETCHMODE_ASSOC);
            $homerow = $homeresult->fetchRow(MDB2_FETCHMODE_ASSOC);

            $table = new HTML_Table( array( 'class'=>'table table-hover table-condensed' ) );

            $contents = array('', '<span class="text-center">1</span>', '<span class="text-center">2</span>', '<span class="text-center">3</span>', '<span class="text-center">4</span>');
            if (is_numeric( $awayrow['a5t']) OR is_numeric( $homerow['h5t'])) $contents[] = '<span class="text-center">OT</span>';
            if (is_numeric( $awayrow['a6t']) OR is_numeric( $homerow['h6t'])) $contents[] = '<span class="text-center">OT2</span>';
            if (is_numeric( $awayrow['a7t']) OR is_numeric( $homerow['h7t'])) $contents[] = '<span class="text-center">OT3</span>';
            if (is_numeric( $awayrow['a8t']) OR is_numeric( $homerow['h8t'])) $contents[] = '<span class="text-center">OT4</span>';
            $contents[] = '<span class="text-center">T</span>';

            $table->addRow( $contents, $attributes, 'th' );

            $contents = array('Away Opponent', $awayrow['a1t'], $awayrow['a2t'], $awayrow['a3t'], $awayrow['a4t']);
            if (is_numeric( $awayrow['a5t']) OR is_numeric( $homerow['h5t'])) {if (is_numeric( $awayrow['a5t'])) $contents[] = $awayrow['a5t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a6t']) OR is_numeric( $homerow['h6t'])) {if (is_numeric( $awayrow['a6t'])) $contents[] = $awayrow['a6t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a7t']) OR is_numeric( $homerow['h7t'])) {if (is_numeric( $awayrow['a7t'])) $contents[] = $awayrow['a7t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a8t']) OR is_numeric( $homerow['h8t'])) {if (is_numeric( $awayrow['a8t'])) $contents[] = $awayrow['a8t']; else $contents[] = 0;}
            $contents[] = $awayrow['at'];

            $table->addRow( $contents, $attributes, 'td' );

            $contents = array('Home Opponent', $homerow['h1t'], $homerow['h2t'], $homerow['h3t'], $homerow['h4t']);
            if (is_numeric( $awayrow['a5t']) OR is_numeric( $homerow['h5t'])) {if (is_numeric( $homerow['h5t'])) $contents[] = $homerow['h5t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a6t']) OR is_numeric( $homerow['h6t'])) {if (is_numeric( $homerow['h6t'])) $contents[] = $homerow['h6t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a7t']) OR is_numeric( $homerow['h7t'])) {if (is_numeric( $homerow['h7t'])) $contents[] = $homerow['h7t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a8t']) OR is_numeric( $homerow['h8t'])) {if (is_numeric( $homerow['h8t'])) $contents[] = $homerow['h8t']; else $contents[] = 0;}
            $contents[] = $homerow['ht'];

            $table->addRow( $contents, $attributes, 'td' );

            $contents = array('Opponent Total', $awayrow['a1t']+$homerow['h1t'], $awayrow['a2t']+$homerow['h2t'], $awayrow['a3t']+$homerow['h3t'], $awayrow['a4t']+$homerow['h4t']);
            if (is_numeric( $awayrow['a5t']) OR is_numeric( $homerow['h5t'])) {if (is_numeric( $awayrow['a5t']+$homerow['h5t'])) $contents[] = $awayrow['a5t']+$homerow['h5t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a6t']) OR is_numeric( $homerow['h6t'])) {if (is_numeric( $awayrow['a6t']+$homerow['h6t'])) $contents[] = $awayrow['a6t']+$homerow['h6t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a7t']) OR is_numeric( $homerow['h7t'])) {if (is_numeric( $awayrow['a7t']+$homerow['h7t'])) $contents[] = $awayrow['a7t']+$homerow['h7t']; else $contents[] = 0;}
            if (is_numeric( $awayrow['a8t']) OR is_numeric( $homerow['h8t'])) {if (is_numeric( $awayrow['a8t']+$homerow['h8t'])) $contents[] = $awayrow['a8t']+$homerow['h8t']; else $contents[] = 0;}
            $contents[] = $awayrow['at']+$homerow['ht'];

            $table->addRow( $contents, $attributes, 'td' );
            $table->updateRowAttributes(0, array('class'=>'row-b'), TRUE);
            $table->updateRowAttributes(3, array('class'=>'row-b'), TRUE);

            echo '<div class="table-responsive">';
            $table->display();
            echo '</div>';
        }
        */

        return view('pages.team', $data);
    }

    /**
     * For the Excel generation, converts a row/column number to an Alpha.
     *
     * @param int $n
     *
     * @return string
     */
    protected function num2alpha($n)
    {
        for ($r = ''; $n >= 0; $n = (int)($n / 26) - 1) {
            $r = chr($n % 26 + 0x41) . $r;
        }

        return $r;
    }

    /**
     * Creates an Excel file showing the RPI calculations
     *
     * @param State $state
     * @param Team $team
     * @param Year $year
     */
    public function rpi(State $state, Team $team, Year $year)
    {
        Excel::create("{$team->name} Football RPI for {$year->year}",
            function (LaravelExcelWriter $workbook) use ($state, $team, $year) {
                $badChars = ['*', ':', '/', '\\', '?', '[', ']'];

                $workbook->getProperties()->setCreator('RockyPrep.com')->setLastModifiedBy('RockyPrep.com')->setCompany('RockyPrep.com')
                    ->setTitle("{$year->year}_{$team->urlname}_RPI")
                    ->setDescription('RPI calculation for ' . $team->name . ' for the year ' . $year->year . ' calculated on ' . date('Y-m-d'))
                    ->setSubject($state->name . ' High School Football RPI calculation.')
                    ->setKeywords($team->name . ' Football ' . $year->year . ' ' . $state->name . ' RockyPrep RockyPrep.com RPRPI RPI');

                $workbook->sheet(substr(str_replace($badChars, '_', $team->name) . ' Football', 0, 31),
                    function (LaravelExcelWorksheet $worksheet) use ($team, $year) {

                        $style_header = [
                            'fill' => [
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => ['rgb' => 'c3c3c3'],
                            ],
                            'font' => ['bold' => true],
                        ];

                        $style_win = [
                            'fill' => [
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => ['rgb' => 'C6EFCE'],
                            ],
                            'font' => ['color' => ['rgb' => '006100']],
                        ];

                        $style_lose = [
                            'fill' => [
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => ['rgb' => 'FFC7CE'],
                            ],
                            'font' => ['color' => ['rgb' => '9C0006']],
                        ];

                        $style_borders_outline_thick = [
                            'borders' => [
                                'outline' => [
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                                ],
                            ],
                        ];
                        $style_borders_inside_thin = [
                            'borders' => [
                                'inside' => [
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                ],
                            ],
                        ];
                        $style_borders_inside_dotted = [
                            'borders' => [
                                'inside' => [
                                    'style' => \PHPExcel_Style_Border::BORDER_HAIR,
                                ],
                            ],
                        ];

                        $worksheet->setCellValue('A3', 'WP');
                        $worksheet->mergeCells('A3:B3');
                        $worksheet->mergeCells('A4:A6');
                        $worksheet->setCellValue('B4', 'Wins');
                        $worksheet->setCellValue('B5', 'Losses');
                        $worksheet->setCellValue('B6', 'WF');
                        $worksheet->setCellValue('A7', 'OWP');
                        $worksheet->mergeCells('A7:B7');
                        $worksheet->mergeCells('A8:A10');
                        $worksheet->setCellValue('B8', 'Wins');
                        $worksheet->setCellValue('B9', 'Losses');
                        $worksheet->setCellValue('B10', 'WF');
                        $worksheet->setCellValue('A11', 'Individual OWP');
                        $worksheet->mergeCells('A11:B11');
                        $worksheet->setCellValue('A12', 'OOWP');
                        $worksheet->mergeCells('A12:B12');
                        $worksheet->mergeCells('A13:A15');
                        $worksheet->setCellValue('B13', 'Wins');
                        $worksheet->setCellValue('B14', 'Losses');
                        $worksheet->setCellValue('B15', 'WF');
                        $worksheet->setCellValue('A16', 'Individual OOWP');
                        $worksheet->mergeCells('A16:B16');
                        $worksheet->cell('A3', function (CellWriter $cell) {
                            $cell->setFontWeight('bold');
                        });

                        $worksheet->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->cell('A7', function (CellWriter $cell) {
                            $cell->setFontWeight('bold');
                        });
                        $worksheet->getStyle('A7')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('A11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->cell('A12', function (CellWriter $cell) {
                            $cell->setFontWeight('bold');
                        });
                        $worksheet->getStyle('A12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $worksheet->getStyle('A16')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->setCellValue('C18', 'WP');
                        $worksheet->setCellValue('C19', 'OWP');
                        $worksheet->setCellValue('C20', 'OOWP');
                        $worksheet->setCellValue('C21', 'WF');
                        $worksheet->getStyle('C18:C21')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $worksheet->mergeCells('D18:I18');
                        $worksheet->mergeCells('D19:I19');
                        $worksheet->mergeCells('D20:I20');
                        $worksheet->mergeCells('D21:I21');
                        $worksheet->setCellValue('D18', 'Winning Percentage');
                        $worksheet->setCellValue('D19', 'Opponent\'s Winning Percentage');
                        $worksheet->setCellValue('D20', 'Opponent\'s Opponent\'s Winning Percentage');
                        $worksheet->setCellValue('D21', 'Weighting Factor');

                        // Get opponent array
                        /** @var Season $season */
                        $season = Season::ofTeam($team)->ofYear($year)->first();

                        $team_blocks = [];
                        $column = 2;

                        $oBonus = true;

                        $seasonSchedule = $season->getSchedule();
                        if ($season->getTotalGames($year) === 0) {
                            return ['Error' => 'That team does not have any games played yet for the requested season.'];
                        }

                        /** @var Game $game */
                        foreach ($seasonSchedule as $key => $game) {
                            /** @var Season $opponent_season */
                            $opponent_season = Season::ofTeam($game->opponent($team))->ofYear($year)->first();

                            $column_start = $column;
                            $column_end = $column + 1;

                            $ooBonus = true;
                            /** @var Game $oGame */
                            foreach ($opponent_season->getSchedule() as $oKey => $oGame) {
                                /** @var Season $opponent_opponent_season */
                                $opponent_opponent_season = Season::ofTeam($oGame->opponent($opponent_season->team))->ofYear($year)->first();

                                $worksheet->setCellValueByColumnAndRow($column, 12,
                                    $opponent_opponent_season->team->name);
                                $worksheet->setCellValueByColumnAndRow($column, 13,
                                    $opponent_opponent_season->getWins($year));
                                $worksheet->setCellValueByColumnAndRow($column, 14,
                                    $opponent_opponent_season->getLoss($year));

                                if ($ooBonus && $opponent_opponent_season->levelid_state < $opponent_season->levelid_state) {
                                    $ooBonus = false;
                                    $worksheet->setCellValueByColumnAndRow($column, 15,
                                        $opponent_opponent_season::getReductionFactor($opponent_opponent_season->levelid_state,
                                            true));
                                } else {
                                    $worksheet->setCellValueByColumnAndRow($column, 15,
                                        $opponent_opponent_season::getReductionFactor($opponent_opponent_season->levelid_state,
                                            false));
                                }

                                $worksheet->setCellValueByColumnAndRow($column, 16,
                                    '=round(' . self::num2alpha($column) . '15*' . self::num2alpha($column) . '13/(' . self::num2alpha($column) . '13+' . self::num2alpha($column) . '14), 4)');

                                if ($oGame->winner == $opponent_season->teamid) {
                                    $worksheet->getStyle(self::num2alpha($column) . '12:' . self::num2alpha($column) . '16')->applyFromArray($style_win);
                                } elseif ($oGame->winner == $opponent_opponent_season->teamid) {
                                    $worksheet->getStyle(self::num2alpha($column) . '12:' . self::num2alpha($column) . '16')->applyFromArray($style_lose);
                                }

                                ++$column;
                            }

                            $column_end = $column - 1;

                            $worksheet->mergeCells(self::num2alpha($column_start) . '7:' . self::num2alpha($column_end) . '7');
                            $worksheet->mergeCells(self::num2alpha($column_start) . '8:' . self::num2alpha($column_end) . '8');
                            $worksheet->mergeCells(self::num2alpha($column_start) . '9:' . self::num2alpha($column_end) . '9');
                            $worksheet->mergeCells(self::num2alpha($column_start) . '10:' . self::num2alpha($column_end) . '10');
                            $worksheet->mergeCells(self::num2alpha($column_start) . '11:' . self::num2alpha($column_end) . '11');
                            $worksheet->getStyle(self::num2alpha($column_start) . '7')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $worksheet->getStyle(self::num2alpha($column_start) . '8')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $worksheet->getStyle(self::num2alpha($column_start) . '9')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $worksheet->getStyle(self::num2alpha($column_start) . '10')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $worksheet->getStyle(self::num2alpha($column_start) . '11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $worksheet->setCellValueByColumnAndRow($column_start, 7, $opponent_season->team->name);

                            $wins = $opponent_season->getWins(null, $team);
                            $loss = $opponent_season->getLoss(null, $team);
                            if ($game->winner == $team->teamid) {
                                $worksheet->getStyle(self::num2alpha($column_start) . '7:' . self::num2alpha($column_start) . '11')->applyFromArray($style_win);
                            } elseif ($game->winner === $opponent_season->teamid) {
                                $worksheet->getStyle(self::num2alpha($column_start) . '7:' . self::num2alpha($column_start) . '11')->applyFromArray($style_lose);
                            }

                            $worksheet->setCellValueByColumnAndRow($column_start, 8, $wins)
                                ->setCellValueByColumnAndRow($column_start, 9, $loss);

                            if ($oBonus && $opponent_season->levelid_state < $season->levelid_state) {
                                $oBonus = false;
                                $worksheet->setCellValueByColumnAndRow($column_start, 10,
                                    $opponent_season::getReductionFactor($opponent_season->levelid_state, true));
                            } else {
                                $worksheet->setCellValueByColumnAndRow($column_start, 10,
                                    $opponent_season::getReductionFactor($opponent_season->levelid_state, false));
                            }

                            $worksheet->setCellValueByColumnAndRow($column_start, 11,
                                '=round(' . self::num2alpha($column_start) . '10*' . self::num2alpha($column_start) . '8/(' . self::num2alpha($column_start) . '8+' . self::num2alpha($column_start) . '9), 4)');

                            $team_blocks[] = ['start' => $column_start, 'end' => $column_end];
                        }

                        $worksheet->mergeCells('A1:' . self::num2alpha($column_end) . '1');
                        $worksheet->mergeCells('A2:' . self::num2alpha($column_end) . '2');
                        $worksheet->mergeCells('C3:' . self::num2alpha($column_end) . '3');
                        $worksheet->mergeCells('C4:' . self::num2alpha($column_end) . '4');
                        $worksheet->mergeCells('C5:' . self::num2alpha($column_end) . '5');
                        $worksheet->mergeCells('C6:' . self::num2alpha($column_end) . '6');

                        $worksheet->setCellValue('A1', $team->name);
                        $worksheet->setCellValue('A2', '=round(A4/4+A8/2+A13/4, 4)');
                        $worksheet->setCellValue('C3', $team->name);
                        $worksheet->setCellValue('A4', '=round(C6*C4/(C4+C5), 4)');
                        $worksheet->setCellValue('C4', $season->getWins($year));
                        $worksheet->setCellValue('C5', $season->getLoss($year));
                        $worksheet->setCellValue('C6', $season::getReductionFactor($season->levelid_state));

                        $worksheet->setColumnFormat([
                                'C6' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                                'C10:' . self::num2alpha($column_end) . '10' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                                'C15:' . self::num2alpha($column_end) . '15' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                                'A4' => '0.0000',
                                'A8' => '0.0000',
                                'A13' => '0.0000',
                                'C16:' . self::num2alpha($column_end) . '16' => '.0000',
                                'C11:' . self::num2alpha($column_end) . '11' => '0.0000',
                            ]
                        );
                        $worksheet->setCellValueByColumnAndRow(0, 8,
                            '=round(average(C11:' . self::num2alpha($column_end) . '11), 4)');
                        $worksheet->setCellValueByColumnAndRow(0, 13,
                            '=round(average(C16:' . self::num2alpha($column_end) . '16), 4)');

                        $worksheet->cells('A1:A2', function (CellWriter $cells) {
                            $cells->setFontWeight('bold');
                        });
                        $worksheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('C3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('C4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('C5')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('C6')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $worksheet->getStyle('C12:' . self::num2alpha($column_end) . '12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setTextRotation(90);
                        $worksheet->getStyle('A4')->applyFromArray($style_header)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $worksheet->getStyle('A8')->applyFromArray($style_header)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $worksheet->getStyle('A13')->applyFromArray($style_header)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $worksheet->getStyle('B4:B6')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $worksheet->getStyle('B8:B11')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $worksheet->getStyle('B13:B15')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $worksheet->getStyle('C12:' . self::num2alpha($column_end) . '16')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                        $worksheet->getStyle('A1:' . self::num2alpha($column_end) . '16')->applyFromArray($style_borders_inside_thin);
                        $worksheet->getStyle('A3:' . self::num2alpha($column_end) . '6')->applyFromArray($style_borders_outline_thick);
                        $worksheet->getStyle('C4:' . self::num2alpha($column_end) . '6')->applyFromArray($style_borders_inside_dotted);
                        $worksheet->getStyle('A7:' . self::num2alpha($column_end) . '11')->applyFromArray($style_borders_outline_thick);
                        $worksheet->getStyle('B4:B6')->applyFromArray($style_borders_inside_dotted);
                        $worksheet->getStyle('B8:B10')->applyFromArray($style_borders_inside_dotted);
                        $worksheet->getStyle('B13:B15')->applyFromArray($style_borders_inside_dotted);

                        // Inside each team block
                        foreach ($team_blocks as $key => $range) {
                            $worksheet->getStyle(self::num2alpha($range['start']) . '7:' . self::num2alpha($range['end']) . '11')->applyFromArray($style_borders_inside_dotted);
                            $worksheet->getStyle(self::num2alpha($range['start']) . '12:' . self::num2alpha($range['end']) . '16')->applyFromArray($style_borders_inside_dotted);
                        }

                        $worksheet->getStyle('A1:' . self::num2alpha($column_end) . '16')->applyFromArray($style_borders_outline_thick);
                    });

            })->download('xlsx');
    }

    /**
     * @deprecated use $state->timezone
     * @param string $abbr
     * @return string
     */
    protected static function timezone($abbr): string
    {
        switch ($abbr) {
            case 'AK':
                return 'America/Anchorage';
            case 'ID':
                return 'America/Boise';
            case 'AL':
            case 'AR':
            case 'IL':
            case 'IA':
            case 'KS':
            case 'LA':
            case 'MN':
            case 'MS':
            case 'MO':
            case 'NE':
            case 'OK':
            case 'SD':
            case 'TN':
            case 'TX':
            case 'WI':
                return 'America/Chicago';
            case 'CO':
                return 'America/Denver';
            case 'MT':
            case 'NM':
            case 'UT':
            case 'WY':
                return 'America/Denver';
            case 'MI':
                return 'America/Detroit';
            case 'IN':
                return 'America/Indiana/Indianapolis';
            case 'KY':
                return 'America/Kentucky/Louisville';
            case 'CA':
            case 'NV':
            case 'OR':
            case 'WA':
                return 'America/Los_Angeles';
            case 'CT':
            case 'DE':
            case 'FL':
            case 'GA':
            case 'ME':
            case 'MD':
            case 'MA':
            case 'NH':
            case 'NJ':
            case 'NY':
            case 'NC':
            case 'OH':
            case 'PA':
            case 'RI':
            case 'SC':
            case 'VT':
            case 'VA':
            case 'DC':
            case 'WV':
                return 'America/New_York';
            case 'ND':
                return 'America/North_Dakota/Center';
            case 'AZ':
                return 'America/Phoenix';
            case 'HI':
                return 'Pacific/Honolulu';
            default:
                return 'America/Denver';
        }
    }
}
