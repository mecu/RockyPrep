<?php

namespace App\Http\Controllers;

use App\Game;
use App\Team;
use App\State;
use App\Year;
use App\Helpers\Navigation;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ScheduleController extends Controller
{
    public function show(State $state, Year $year, string $month, string $day)
    {
        $todaysDate = "{$year->year}-$month-$day";
        $timezone = new \DateTimeZone($state->timezone);
        $today = Carbon::createFromFormat('Y-m-d', $todaysDate, $timezone);

        $data['years'] = Navigation::menuYearsSchedule($state);

        // List of teams in this state
        $teamsInState = Team::select('teamid')->where(['stateid' => $state->stateid])->get();

        // Get the full list of dates for the teams for this season
        $validDatesHome = Game::select('date')
            ->distinct()
            ->whereIn('hometeam', $teamsInState)
            ->whereBetween('date', [$year->start, $year->end])
            ->get()
            ->pluck('date');

        $validDatesAway = Game::select('date')
            ->distinct()
            ->whereIn('awayteam', $teamsInState)
            ->whereBetween('date', [$year->start, $year->end])
            ->get()
            ->pluck('date');

        $data['nogames'] = false;
        $data['nodates'] = false;

        $data['month'] = $month;
        $data['day'] = $day;
        $data['date'] = $today->format('l, F j, Y');
        $data['title'] = "{$state->name} Football Schedule for {$data['date']} :: ";
        $data['state'] = $state;
        $data['year'] = $year;

        if ($validDatesHome->count() === 0 && $validDatesAway->count() === 0) {
            $data['nodates'] = true;

            return view('pages.schedule', $data);
        }

        // Combine the 2 Carbon result sets into 1 string array
        /** @var Collection $validDates */
        $validDates = $validDatesHome->merge($validDatesAway)
            ->map(function (Carbon $date) {return $date->format('Y-m-d');})
            ->unique()
            ->sort();

        // Figure out max and max dates
        $maxDay = new \DateTime($validDates->last());
        $maxDay->add(new \DateInterval('P1D'));

        $data['maxDay'] = $maxDay->format('Y-m-d');
        $data['minDay'] = $validDates->first();
        $data['today'] = $today->format('Y-m-d');
        $data['validDates'] = $validDates;

        // See if there are games for the requested date
        $gameListAway = Game::where(['date' => $today->format('Y-m-d')])
            ->whereIn('awayteam', $teamsInState)
            ->with('home', 'away', 'stadium')
            ->get();
        $gameListHome = Game::where(['date' => $today->format('Y-m-d')])
            ->whereIn('hometeam', $teamsInState)
            ->with('home', 'away', 'stadium')
            ->get();

        /** @var Collection $gameList */
        $gameList = $gameListAway->merge($gameListHome);

        if ($gameList->count() === 0) {
            $data['nogames'] = true;
        }

        $data['games'] = $gameList;

        return view('pages.schedule', $data);
    }
}
