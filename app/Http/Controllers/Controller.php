<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Sort a multi-dimensional array by a sub-key
     * @link http://php.net/manual/en/function.array-multisort.php#91645
     * @internal Array Multisorting
     * @return array
     */
    public static function arrayMultiSort($array, $cols)
    {
        $colarr = [];
        foreach ($cols as $col => $order) {
            $colarr[$col] = [];
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $params = [];
        foreach ($cols as $col => $order) {
            $params[] =& $colarr[$col];
            $params = array_merge($params, (array)$order);
        }
        call_user_func_array('array_multisort', $params);
        $ret = [];
        $keys = [];
        $first = true;
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                if ($first) {
                    $keys[$k] = substr($k, 1);
                }
                $k = $keys[$k];
                if (!isset($ret[$k])) {
                    $ret[$k] = $array[$k];
                }
                $ret[$k][$col] = $array[$k][$col];
            }
            $first = false;
        }

        return $ret;
    }
}
