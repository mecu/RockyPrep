<?php
namespace App\Http\Controllers;

use App\Season;
use App\Year;
use App\State;
use App\Coach;
use URL;

class CoachController extends Controller
{
    public function record(State $state, Coach $coach)
    {
        $data['title'] = 'Coach ' . $coach->name . ' Football Record :: ';
        $data['state'] = $state;
        $data['coach'] = $coach;
        $data['year'] = new Year();

        $teamList = $coach->teamList();

        $data['total']['wins'] = 0;
        $data['total']['loss'] = 0;
        $data['total']['cwins'] = 0;
        $data['total']['closs'] = 0;

        foreach ($teamList as $key => $team) {
            /* @var \App\Season $team */
            $year = new Year($team->getYear());

            $data['team'][$year->year]['team'] = $team;
            $data['team'][$year->year]['url'] = URL::action('TeamController@team', array($state->urlname, $team->team->urlname, $year->year));
            $data['team'][$year->year]['name'] = $team->team->name;
            $data['team'][$year->year]['wins'] = $team->getWins();
            $data['team'][$year->year]['loss'] = $team->getLoss();
            $data['team'][$year->year]['cwins'] = $team->getConferenceWins();
            $data['team'][$year->year]['closs'] = $team->getConferenceLoss();

            $data['total']['wins'] += $data['team'][$year->year]['wins'];
            $data['total']['loss'] += $data['team'][$year->year]['loss'];
            $data['total']['cwins'] += $data['team'][$year->year]['cwins'];
            $data['total']['closs'] += $data['team'][$year->year]['closs'];
        }

        if ($data['total']['wins'] + $data['total']['loss'] > 0) {
            $data['total']['avg'] = round($data['total']['wins'] / ($data['total']['wins'] + $data['total']['loss']), 3);
            $data['total']['cavg'] = round($data['total']['cwins'] / ($data['total']['wins'] + $data['total']['closs']), 3);
        }

        return view('pages.coach', $data);
    }

    public function showall(State $state)
    {
        $data['title'] = "{$state->name} Football Coaches :: ";
        $data['state'] = $state;
        $data['year'] = new Year();
        $data['coaches'] = [];

        $coaches = Season::select('coach.coachid', 'team.teamid')
            ->distinct()
            ->join('coach', 'coach.coachid', '=', 'season.coachid')
            ->whereNotNull('season.coachid')
            ->join('team', 'team.teamid', '=', 'season.teamid')
            ->where('team.stateid', $state->stateid)
            ->get();

        foreach ($coaches as $key => $coach) {
            if (array_key_exists($coach->coach->name, $data['coaches'])) {
                $data['coaches'][$coach->coach->name]['teams'][] = $coach->team->name;
            } else {
                $data['coaches'][$coach->coach->name] = array('urlname' => $coach->coach->urlname, 'teams' => array($coach->team->name));
            }
        }

        foreach ($data['coaches'] as $name => $value) {
            $data['coaches'][$name]['teams'] = implode(', ', $data['coaches'][$name]['teams']);
        }

        asort($data['coaches']);
        return view('pages.showAllCoaches', $data);
    }
}
