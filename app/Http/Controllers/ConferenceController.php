<?php
namespace App\Http\Controllers;

use App\Conference;
use App\Season;
use App\Year;
use App\State;
use URL;

class ConferenceController extends Controller
{
    public function showAll(State $state)
    {
        $title = "{$state->name} Football Conferences :: ";
        $year = new Year(\App\Helpers\Helpers::currentYear());

        $conferences = Conference::where(['stateid' => $state->stateid])->orderBy('name')->get();

        return view('pages.showAllConferences', compact(['title', 'state', 'year', 'conferences']));
    }
    
    public function standings(State $state, Conference $conference, Year $year)
    {
        $data['title'] = "{$year->year} {$state->name} Football {$conference->name} Conference Standings :: ";
        $data['state'] = $state;
        $data['conference'] = $conference;
        $data['year'] = $year;

        $seasons = Season::where('confid', $conference->confid)->get();
        foreach ($seasons as $key => $season) {
            $data['years'][$season->year] = $season->year == $year->year;
        }

        $teamList = $conference->teamList( $year );
        
        if (empty($teamList)) {
            $data['nodata'] = true;
            return view('pages.conference', $data);
        };

        $standings = [];
        $wins = [];
        $loss = [];
        $name = [];

        /** @var Season $season */
        foreach ($teamList as $key => $season) {
            $teamID = $season->team->teamid;
            if ($teamID === 0) {
                continue;
            }
            $data['team'][$teamID] = $season->team;

            $standings[$teamID]['wins'] = $wins[$teamID] = $season->getWins();
            $standings[$teamID]['loss'] = $loss[$teamID] = $season->getLoss();
            $standings[$teamID]['cwins'] = $season->getConferenceWins();
            $standings[$teamID]['closs'] = $season->getConferenceLoss();
            $standings[$teamID]['name'] = $name[$teamID] = $season->team->name;
            $standings[$teamID]['teamid'] = $teamID;
            $standings[$teamID]['url'] = URL::action('TeamController@team', [$state->urlname, $data['team'][$teamID]->urlname, $year->year]);
        }

        array_multisort($wins, SORT_DESC, $loss, SORT_ASC, $name, SORT_ASC, $standings);

        $data['ranking'] = $standings;

        // Trick to make it start at key=1 and not key=0
        array_unshift($data['ranking'], 'nothing');
        unset($data['ranking'][0]);

        return view('pages.conference', $data);
    }
}
