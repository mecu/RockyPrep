<?php namespace App\Http\Controllers;

use App\Article;
use App\State;

class HomeController extends Controller
{
    public function index()
    {
        $states = State::all();
        $articles = Article::all();

        return view('pages.home', [
            'states' => $states,
            'articles' => $articles,
        ])->render();
    }
}
