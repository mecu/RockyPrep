<?php
namespace App\Http\Controllers;

use App\Season;
use App\Team;
use App\State;
use App\Year;

/**
 * Class RateController
 * @package App\Http\Controllers
 */
class RateController extends Controller
{
    /**
     * @param State $state
     * @param Year $year
     *
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function srs(State $state, Year $year)
    {
        $data['state'] = $state;
        $data['year'] = $year;

        // List of teams in this State
        $teamsInState = Team::select('teamid')->where(['stateid' => $state->stateid])->get();

        $data['ranking'] = Season::ofYear($year)
            ->whereIn('teamid', $teamsInState)
            ->orderBy('srs')
            ->with('team', 'level')
            ->get();

        $data['levels'] = $data['ranking']->pluck('level')->unique('levelid')->sortByDesc('levelid')->pluck('name');

        return view('pages.srs', $data);
    }

    /**
     * @param State $state
     * @param Year $year
     *
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function rpi(State $state, Year $year)
    {
        $data['state'] = $state;
        $data['year'] = $year;

        // List of teams in this State
        $teamsInState = Team::select('teamid')
            ->where(['stateid' => $state->stateid])
            ->where('teamid', '!=', 0)
            ->get();

        $ranking = Season::ofYear($year)
            ->whereIn('teamid', $teamsInState)
            ->orderBy('rpi')
            ->orderBy('rpi_wp')
            ->orderBy('rpi_owp')
            ->orderBy('rpi_oowp')
            ->with('team', 'level')
            ->get();

        // Filter out teams with zero games played
        $ranking = $ranking->filter(function (Season $season) use ($year) {
            return $season->getTotalGames($year) > 0;
        });

        $data['ranking'] = $ranking;

        $data['levels'] = $data['ranking']->pluck('level')->unique('levelid')->sortByDesc('levelid')->pluck('name');

        return view('pages.rpi', $data);
    }
}
