<?php
namespace App\Http\Controllers;

use App\Year;
use App\State;

class ShowAllController extends Controller
{
    public function showAllStates(Year $year)
    {
        $data['year'] = $year ?? new Year();
        $data['states'] = State::all();

        return view('pages.showAllStates', $data)->render();
    }
}
