<?php
namespace App\Http\Controllers;

use App\State;
use App\User;
use Input;

class UserController extends Controller
{
    public function login()
    {
        if ( wire('user')->id && wire('user')->isLoggedin() ) {
            return \Redirect::to($_SERVER['HTTP_REFERER']);
        }
        
        $data['from'] = $_SERVER['HTTP_REFERER'];
        return view('pages.login', $data);
    }
    
    public function register()
    {
        if ( wire('user')->id && wire('user')->isLoggedin() ) {
            return \Redirect::to($_SERVER['HTTP_REFERER']);
        }
        
        $data['from'] = $_SERVER['HTTP_REFERER'];
        if (Input::has('from')) {
            $data['from'] = Input::get('from');
        }
        
        $data['states'] = State::all();
                
        return view('pages.register', $data);
    }

    public function registerAction()
    {
        //wire('user')
        $u =& wire('user');
        $u = new User();
        
        //Required
        $u->firstname = Input::get('firstname');
        $u->lastname = Input::get('lastname');
        $u->pass = Input::get('password');
        $u->email = Input::get('email');
        $u->name = Input::get('name');
        
        //Optional
        $u->user_title = Input::get('title');
        $u->user_location = Input::get('location');
        $u->timezone = Input::get('timezone');
        $u->favorite_state = Input::get('favorite_state');
        $u->favorite_sport = Input::get('favorite_sport');
        $u->favorite_team = Input::get('favorite_team');
        
        $u->addRole('guest');
        
        $u->save();
        
        $data['from'] = $_SERVER['HTTP_REFERER'];
        if (Input::has('from')) {
            $data['from'] = Input::get('from');
        }
        
        return view('pages.register', $data);
    }
}
