<?php declare(strict_types=1);
namespace App\Http\Controllers\Admin;

use App\Game;
use App\Http\Controllers\AdminController;

class WantToDeleteController extends AdminController
{
    public function index()
    {
        $games = Game::where('want_to_delete', true)->get();

        return view('admin.dashboard.wantToDelete', ['games' => $games]);
    }
}
