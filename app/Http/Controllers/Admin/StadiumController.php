<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Stadium;
use App\State;
use Illuminate\Http\Request;
use Session;

class StadiumController extends AdminController
{
    public function index()
    {
        $states = State::all();

        return view('admin.dashboard.conference', compact('states'));
    }

    public function createStadium()
    {
        $team = new Stadium();
        $states = State::all()->pluck('name', 'stateid')->push(['' => '']);

        return view('admin.dashboard.createStadium', compact('team', 'states'));
    }

    public function processStadium(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'stateid' => 'required|numeric',
                'location' => 'url',
                'zip' => 'digits_between,5,9',
            ]);
        } catch (\Exception $e) {
            return redirect()->back();
        }

        $stadium = Stadium::create($request->all());

        Session::flash('created', $stadium->stadiumid);

        return redirect('admin/create/stadium');
    }
}
