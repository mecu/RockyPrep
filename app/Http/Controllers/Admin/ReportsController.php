<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use File;

class ReportsController extends AdminController
{
    private $path = '/home/rp/storage/reports/';

    public function index()
    {
        $title = 'Reports Dashboard';

        $fileList = array_diff(scandir(storage_path('reports/')), ['..', '.']);

        $data = [];
        foreach ($fileList as $file) {
            $data[] = [
                'name' => ucwords(str_replace('_', ' ', substr($file, 0, -5))),
                'filename' => $file,
                'attr' => stat($this->path . $file),
            ];
        }

        return view('admin.dashboard.reports', compact('title', 'data'));
    }

    public function viewReport($filename)
    {
        return File::get($this->path . $filename);
    }
}
