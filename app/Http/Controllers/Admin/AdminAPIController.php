<?php namespace App\Http\Controllers\Admin;

use App\Game;
use App\Helpers\BoxScore;
use App\Http\Controllers\AdminController;
use App\Jobs\TweetGame;
use App\State;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminAPIController extends AdminController
{
    use BoxScore;
    /**
     * @param State $state
     * @return mixed
     * @deprecated
     */
    public function reportingGameList(State $state)
    {
        // Get List of Teams in the State
        $teams = Team::where('stateid', $state->stateid)->get();

        // Get List of Games up to today without a winner for the teams above
        $date = Carbon::now()->format('Y-m-d');
        $games = Game::where('date', '<=', $date)
            ->whereNull('winner')
            ->whereIn('hometeam', $teams->pluck('teamid')->toArray())
            ->orWhereIn('awayteam', $teams->pluck('teamid')->toArray())
            ->where('date', '<=', $date)
            ->whereNull('winner')
            ->orderBy('date', 'DESC')
            ->with('home', 'away')
            ->get();

        return $games;
    }

    /**
     * Reporting scores on a game
     *
     * @param Game $game
     * @param Request $request
     * @return JsonResponse
     */
    public function reportGame(Request $request, Game $game)
    {
        // Check for required values, homescore, awayscore, winner
        if (!$request->filled(['homescore', 'awayscore', 'winner'])) {
            return Response::json([
                'error' => 'You must provide a winner, home score and away score',
            ], 400);
        }

        $game->homescore = (int)$request->get('homescore');
        $game->awayscore = (int)$request->get('awayscore');

        $game->winner = (int)$request->get('winner');

        $game->homescore = (int)$request->get('homescore');
        $game->awayscore = (int)$request->get('awayscore');

        // Validate winner has the greater score
        if ($game->homescore > $game->awayscore) {
            if ($game->hometeam !== $game->winner) {
                return Response::json([
                    'error' => 'The winner provided did not match the scores provided.',
                ], 400);
            }
        } elseif ($game->homescore < $game->awayscore) {
            if ($game->awayteam !== $game->winner) {
                return Response::json([
                    'error' => 'The winner provided did not match the scores provided.',
                ], 400);
            }
        } elseif ($game->homescore === $game->awayscore) {
            if ($game->winner !== 0) {
                return Response::json([
                    'error' => 'The scores indicate a tie, but the winner was not set to 0.',
                ], 400);
            }
        }

        // Process Box Scores, if provided
        $scores = $request->get('game')['scores'];
        $scores = $this->arrayRemoveEmpty($scores);

        if (!empty($scores)) {
            BoxScore::processScores($scores, $game);
        }

        if ($request->filled('conference')) {
            $game->confid = (bool)$request->get('conference');
        }
        if ($request->filled('neutral')) {
            $game->neutral = (bool)$request->get('neutral');
        }
        if ($request->filled('forfeit')) {
            $game->forfeit = (bool)$request->get('forfeit');
        }
        if ($request->filled('playoff')) {
            $game->playoff = (bool)$request->get('playoff');
        }
        if ($request->filled('stadium')) {
            $game->stadiumid = $request->get('stadium');
        }

        $game->human = true;

        $saved = $game->save();

        $tweet = new TweetGame($game, $game->home->state);
        dispatch($tweet)->onQueue('tweet')->delay(Carbon::now()->addMinutes(3));

        if ($game->home->state !== $game->away->state) {
            $tweet = new TweetGame($game, $game->away->state);

            dispatch($tweet)->onQueue('tweet')->delay(Carbon::now()->addMinutes(3));
        }

        return Response::json([
            'success' => true,
            'game-updated' => true,
            'saved' => $saved,
        ], 200);
    }

    /**
     * Set team information (website, hudl, twitter, facebook)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function teamInfo(Request $request)
    {
        $team = Team::findOrFail($request->get('id'));

        $team->twitter = $request->get('twitter', $team->twitter);
        $team->hudl = $request->get('hudl', $team->hudl);
        $team->facebook = $request->get('facebook', $team->facebook);
        $team->facebook_id = $request->get('facebookid', $team->facebook_id);
        $team->website = $request->get('website', $team->website);
        $team->save();

        return Response::json([
            'success' => true,
        ], 200);
    }
}
