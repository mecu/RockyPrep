<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Helpers\URLName;
use App\Http\Controllers\AdminController;
use App\Http\Requests\StoreTeam;
use App\State;
use App\Team;
use App\Year;
use Illuminate\Http\Request;
use Session;

class TeamController extends AdminController
{
    public function create()
    {
        $team = new Team;
        $states = State::all()->pluck('name', 'stateid')->push(['' => '']);

        return view('admin.dashboard.createTeam', compact('team', 'states'));
    }

    public function store(StoreTeam $request)
    {
        $team = Team::create($request->all());

        // Be sure to fix bad chars in urlname
        str_replace(URLName::BADCHARS, '', $team->urlname, $count);
        if ($count > 0) {
            URLName::generate($team);
        }
        $team->save();

        Session::flash('created', $team->teamid);

        return $this->create();
    }

    public static function createAuto()
    {
        return view('admin.dashboard.createTeamAuto');
    }

    public static function storeAuto(Request $request)
    {
        $url = $request->get('url');

        // Scrape the MaxPreps URL
        $maxPrepsPage = html5qp($url);

        if ($maxPrepsPage === null) {
            Session::flash('error', 'I could not scrape that URL.');
        } else {
            $team = new Team();

            $name = $maxPrepsPage->branch()->find('#ctl00_NavigationWithContentOverRelated_ContentOverRelated_PageHeaderGenericControl_Header')->text();
            // Remove generic terms
            $team->name = trim(str_replace(['Football', 'High School', 'Preps', 'Schedule', (string)Helpers::currentYear()], '', $name));

            $fullAddress = $maxPrepsPage->branch()->find('#ctl00_NavigationWithContentOverRelated_ContentOverRelated_PageHeaderGenericControl_Location')->text();
            $matched = preg_match('/(.+), ([[:alpha:]]+), ([[:alpha:]]+) (\d+)/', $fullAddress, $address);

            if ($matched !== 0) {
                // Match on the RegEx
                $team->address = $address[1];
                $team->city = $address[2];

                $state = State::where('abbr', $address[3])->first();
                if ($state !== null) {
                    $team->stateid = $state->stateid;
                }

                $team->zip = $address[4];
            } else {
                // All we really need is the State Code
                $matched = preg_match('/[[:alpha:]]+, ([[:alpha:]]{2}+)/', $fullAddress, $address);

                if ($matched !== 0) {
                    $state = State::where('abbr', $address[1])->first();
                    if ($state !== null) {
                        $team->stateid = $state->stateid;
                    }
                }
            }

            if ($team->stateid === null) {
                Session::flash('error', 'I could not get the address parsed. Create manually.');

                return static::createAuto();
            }

            $mascot = $maxPrepsPage->branch()->find('#ctl00_NavigationWithContentOverRelated_ContentOverRelated_PageHeaderUserControl_MascotName')->text();
            if ($mascot) {
                $team->mascot = $mascot;
            }

            if (!URLName::generate($team)) {
                Session::flash('error', 'I could not generate a unique url name. Create manually.');

                return static::createAuto();
            }

            $canonicalUrl = $maxPrepsPage->branch()->find('link[rel="canonical"]')->attr('href');
            if (preg_match('/high-schools\/(.+)\/football/', $canonicalUrl, $maxPrepsName) === 1) {
                $team->maxpreps_name = $maxPrepsName[1];
            }

            $formAction = $maxPrepsPage->branch()->find('#aspnetForm')->attr('action');
            preg_match('/schoolid=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/', $formAction, $maxprepsID);

            // See if we have a team by this ID:
            $existingTeam = Team::where('maxpreps_id', $maxprepsID[1])->first();
            if ($existingTeam !== null) {
                Session::flash('error', 'A team with that MaxPrepsID already exists: ' . link_to_route('team',
                        $existingTeam->name, [
                            $existingTeam->state->urlname,
                            $existingTeam->urlname,
                            Helpers::currentYear(),
                        ]
                    )
                );
            } else {
                $team->maxpreps_id = $maxprepsID[1];

                $team->save();
                Session::flash('created', $team->teamid);
            }

        }

        return static::createAuto();
    }
}
