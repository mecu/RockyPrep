<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\Http\Controllers\AdminController;
use App\State;
use App\Team;
use Carbon\Carbon;

class ReportingController extends AdminController
{
    public function index()
    {
        $states = State::all();

        return view('admin.dashboard.reporting', compact('states'));
    }

    public function gameList(State $state)
    {
        // Get List of Teams in the State
        $teams = Team::where('stateid', $state->stateid)->get();

        // Get List of Games up to today without a winner for the teams above
        $date = Carbon::now()->format('Y-m-d');
        $games = Game::where('date', '<=', $date)
            ->whereNull('winner')
            ->whereIn('hometeam', $teams->pluck('teamid')->toArray())
            ->orWhereIn('awayteam', $teams->pluck('teamid')->toArray())
            ->where('date', '<=', $date)
            ->whereNull('winner')
            ->orderBy('date', 'DESC')
            ->with('home', 'away')
            ->get();

        return view('admin.dashboard.reportingState', compact('games', 'state'));
    }
}
