<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Season;
use App\State;
use App\Team;
use App\Year;

class RPIController extends AdminController
{
    public function reach()
    {
        $colorado = State::where('name', 'Colorado')->first();

        $teams = Team::where('stateid', $colorado->stateid)->get();

        $seasons = Season::whereIn('teamid', $teams->pluck('teamid'))
            ->where('year', (new Year())->year)
            ->get();

        $reach = [];

        foreach($seasons as $season) {
            $reach += $season->getReach();
        }

        $year = new Year();
        // Get the season of each of these teams
        $reachSeasons = [];
        foreach ($reach as $team) {
            $reachSeasons[] = Season::ofTeam($team)->ofYear($year)->first();
        }

        return view('admin.dashboard.reach', ['reach' => $reachSeasons]);
    }
}
