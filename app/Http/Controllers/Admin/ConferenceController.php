<?php namespace App\Http\Controllers\Admin;

use App\Conference;
use App\Helpers\Helpers;
use App\Http\Controllers\AdminController;
use App\Season;
use App\State;
use App\Team;
use Illuminate\Http\Request;
use Session;

class ConferenceController extends AdminController
{
    public function index()
    {
        $states = State::all();

        return view('admin.dashboard.conference', compact('states'));
    }

    public function teamList(State $state)
    {
        // Get List of Teams in the State
        $teams = Team::where('stateid', $state->stateid)->where('teamid', '!=', 0)->with('seasons')->orderBy('name')->get();
        $conferences = Conference::all()->pluck('name', 'confid')->push(['' => '']);

        return view('admin.dashboard.conferenceState', compact('teams', 'conferences', 'state'));
    }

    public function processPost(State $state, Request $request)
    {
        $data = $request->all();
        $success = 0;

        foreach ($data as $key => $conferenceID) {
            if (empty($conferenceID)) {
                continue;
            }

            $teamID = str_replace(['conference_'], '', $key);

            $season = Season::firstOrNew([
                'year' => Helpers::currentYear(),
                'teamid' => $teamID
            ]);

            if (str_contains($key, 'conference_')) {
                if ($season->confid == $conferenceID) {
                    continue;
                }
                $season->confid = $conferenceID;
            } else {
                continue;
            }

            if ($season->save()) {
                ++$success;
            }
        }

        Session::flash('updated', $success);

        return $this->teamList($state);
    }
}
