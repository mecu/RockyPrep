<?php namespace App\Http\Controllers\Admin;

use App\Helpers\Helpers;
use App\Http\Controllers\AdminController;
use App\Level;
use App\Season;
use App\State;
use App\Team;
use Illuminate\Http\Request;
use Session;

class LevelController extends AdminController
{
    public function index()
    {
        $states = State::all();

        return view('admin.dashboard.levels', compact('states'));
    }

    public function teamList(State $state)
    {
        // Get List of Teams in the State
        $teams = Team::where('stateid', $state->stateid)
            ->where('teamid', '!=', 0)
            ->whereHas('seasons', function ($query) {
                $query->where('season.levelid', null)
                ->where('season.year', Helpers::currentYear());
            })
            ->orderBy('name')
            ->get();

        $levels = Level::all()->pluck('name', 'levelid')->push(['' => '']);

        return view('admin.dashboard.levelsState', compact('teams', 'levels', 'state'));
    }

    public function processPost(State $state, Request $request)
    {
        $data = $request->all();
        $success = 0;

        foreach ($data as $key => $value) {
            if (empty($value)) {
                continue;
            }

            $teamID = str_replace(['ls_', 'l_', 'a_'], '', $key);

            $season = Season::firstOrNew([
                'year' => Helpers::currentYear(),
                'teamid' => $teamID,
            ]);

            if (str_contains($key, 'ls_')) {
                if ($season->levelid_state == $value) {
                    continue;
                }
                $season->levelid_state = $value;
            } elseif (str_contains($key, 'l_')) {
                if ($season->levelid == $value) {
                    continue;
                }
                $season->levelid = $value;
            } elseif (str_contains($key, 'a_')) {
                if ($value == 0 || $season->level_alter == $value) {
                    continue;
                }
                $season->level_alter = $value;
            } else {
                continue;
            }

            if ($season->save()) {
                ++$success;
            }
        }

        Session::flash('updated', $success);

        return $this->teamList($state);
    }
}
