<?php namespace App\Http\Controllers;

use App\Cutoff;
use App\Helpers\Navigation;
use App\State;
use App\Season;
use App\Level;
use App\Team;
use App\Wildcard;
use App\Year;
use Carbon\Carbon;

class StandingsController extends Controller
{
    /**
     * @param State $state
     * @param Level $level
     * @param Year $year
     * @return mixed
     */
    public function standings(State $state, Level $level, Year $year)
    {

        $data['title'] = "{$year->year} {$state->name} Football {$level->name} Standings :: ";
        $data['state'] = $state;
        $data['level'] = $level;
        $data['year'] = $year;

        $data['time'] = Carbon::now($state->timezone)->toDayDateTimeString() . ' ' . Carbon::now($state->timezone)->format('T');

        $data['years'] = Navigation::menuYearsShowAll($state);
        $data['menuLevels'] = Navigation::menuLevels($state, $year);

        $teams = Team::select('teamid')->where(['stateid' => $state->stateid])->get();
        $seasons = Season::ofYear($year)
            ->where('levelid_state', $level->levelid)
            ->whereIn('teamid', $teams)
            ->where('teamid', '!=', 0)
            ->get();

        if (($state->stateid === 6 && $level->levelid > 10) && ($year->year > 2010 && $year->year < 2016)) {
            $data['coloradoFootball'] = true;

            $standings = [];
            $standings_tiebreak = [];

            //Figure out the cutoff date for calculations
            $cutoff = Cutoff::select('date')->where(['year' => $year->year, 'levelid' => $level->levelid])->first();
            $year->setEnd($cutoff->date->format('Y-m-d'));

            $seasons->filter(function (Season $season) use ($year) {
                return $season->getTotalGames($year) > 0;
            });
            foreach ($seasons as $key => $season) {

                $data['team'][$season->team->teamid] = $season->team;
                $teamSchedule = $season->getSchedule();
                /** @var $seasons Season[] */
                $seasons[$season->team->teamid] = $season;

                if (!array_key_exists($season->team->teamid, $standings)) {
                    $standings[$season->team->teamid] = 0;
                }
                if (!array_key_exists($season->team->teamid, $standings_tiebreak)) {
                    $standings_tiebreak[$season->team->teamid] = 0;
                }

                $games = [];
                foreach ($teamSchedule as $gameKey => $game) {
                    /* @var \App\Game $game */
                    if (!array_key_exists($game->id, $games)) {
                        $games[$game->id] = $game;
                    }

                    if (!is_object($game)) {
                        continue;
                    }
                    if ($game->hasWinner() === false || $game->playoff === true) {
                        continue;
                    }
                    if ($game->date->format('Y-m-d') >= $year->getEnd()) {
                        continue;
                    }

                    $bonus = false;

                    $opponent = $game->opponent($season->team);

                    if (!array_key_exists($opponent->teamid, $data['team'])) {
                        $data['team'][$opponent->teamid] = $opponent;
                    }

                    if (!array_key_exists($opponent->teamid, $seasons)) {
                        $opponentSeason = Season::ofTeam($opponent)->ofYear($year)->first();
                    } else {
                        $opponentSeason = $seasons[$opponent->teamid];
                    }

                    if ($opponentSeason->level->levelid < $season->level->levelid && $season->hasLowerBonus()) {
                        $season->setLowerBonus(false);
                        $bonus = true;
                    }

                    $standings[$season->team->teamid] += Wildcard::givepoints($season, $opponentSeason, $game, $year, $bonus);

                    if ($game->status($season->team) === 'WIN') {
                        $standings_tiebreak[$season->team->teamid] += Wildcard::givetiebreakpoints($opponentSeason->level);
                        $standings_tiebreak[$season->team->teamid] += 0.5;
                        $standings_tiebreak[$season->team->teamid] += Wildcard::secondleveltiebreakpoints($opponentSeason, $year);
                    }
                }
            }

            $wins = [];
            $loss = [];
            $name = [];
            $pointsSort = [];
            $tiebreak = [];
            $ranking = [];

            foreach ($standings as $teamId => $points) {
                if ($seasons[$teamId]->getTotalGames($year) > 0) {
                    $ranking[$teamId]['points'] =
                    $pointsSort[$teamId] =
                        round($points / $seasons[$teamId]->getTotalGames($year), 3);
                    $ranking[$teamId]['tiebreak'] = $tiebreak[$teamId] = round($standings_tiebreak[$teamId] / $seasons[$teamId]->getTotalGames($year), 3);
                } else {
                    $ranking[$teamId]['points'] = $pointsSort[$teamId] = 0;
                    $ranking[$teamId]['tiebreak'] = $tiebreak[$teamId] = 0;
                }

                $ranking[$teamId]['wins'] = $wins[$teamId] = $seasons[$teamId]->getWins($year);
                $ranking[$teamId]['loss'] = $loss[$teamId] = $seasons[$teamId]->getLoss($year);
                $ranking[$teamId]['name'] = $name[$teamId] = $data['team'][$teamId]->name;
                $ranking[$teamId]['teamid'] = $teamId;
                $ranking[$teamId]['team'] = $data['team'][$teamId];
            }

            array_multisort($pointsSort, SORT_DESC, $tiebreak, SORT_DESC, $wins, SORT_DESC, $loss, SORT_ASC, $name, SORT_ASC, $ranking);

            $data['ranking'] = $ranking;
        } else {
            $data['coloradoFootball'] = false;
            $standings = [];
            $rpi = [];
            $wins = [];
            $loss = [];
            $name = [];

            /* @var Season $season */
            foreach ($seasons as $key => $season) {
                $teamId = $season->team->teamid;

                $data['team'][$teamId] = $season->team;
                $seasons[$teamId] = $season;

                $standings[$teamId]['rpi'] = $rpi[$teamId] = number_format($season->rpi, 4);
                $standings[$teamId]['wins'] = $wins[$teamId] = $season->getWins();
                $standings[$teamId]['loss'] = $loss[$teamId] = $season->getLoss();
                $standings[$teamId]['name'] = $name[$teamId] = $season->team->name;
                $standings[$teamId]['teamid'] = $teamId;
                $standings[$teamId]['team'] = $season->team;
                $standings[$teamId]['season'] = $season;
            }

            array_multisort($rpi, SORT_DESC, $wins, SORT_DESC, $loss, SORT_ASC, $name, SORT_ASC, $standings);

            $data['ranking'] = $standings;
        }

        // Make the list start at key = 1 and not key = 0
        array_unshift($data['ranking'], 'nothing');
        unset($data['ranking'][0]);

        $data['years'] = Navigation::menuYearsShowAll($state);

        foreach ($data['ranking'] as $ranking => $teamData) {
            $data['ranking'][$ranking]['playUp'] = $seasons[$teamData['teamid']]->isPlayUp();
            $data['ranking'][$ranking]['playDown'] = $seasons[$teamData['teamid']]->isPlayDown();
        }

        return view('pages.standings', $data);
    }
}
