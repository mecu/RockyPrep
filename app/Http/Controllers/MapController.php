<?php namespace App\Http\Controllers;

use App\Helpers\Navigation;
use App\Season;
use App\State;
use App\Year;
use Illuminate\View\View;

class MapController extends Controller
{
    public function map(State $state, Year $year): View
    {
        $data['state'] = $state;
        $data['year'] = $year;
        $data['years'] = Navigation::menuYearsShowAll($state);

        $teams = Season::join('team', 'team.teamid', '=', 'season.teamid')
            ->ofYear($year)
            ->whereNotNull('levelid_state')
            ->where('levelid_state', '!=', 0)
            ->where('team.stateid', $state->stateid)
            ->whereNotNull('lat')
            ->whereNotNull('lng')
            ->with('levelState')
            ->get();

        $data['noTeams'] = count($teams) === 0;
        $data['teams'] = $teams;

        return View('pages.map', $data);
    }
}
