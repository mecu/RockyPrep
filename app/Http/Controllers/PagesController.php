<?php
namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function about()
    {
        return view('static.about');
    }

    public function contact()
    {
        return view('static.contact');
    }

    public function twitterInfo()
    {
        return view('static.twitterInfo');
    }

    public function rpi()
    {
        return view('static.rpi');
    }

    public function srs()
    {
        return view('static.srs');
    }

    public function disclaimer()
    {
        return view('static.disclaimer');
    }

    public function privacy()
    {
        return view('static.privacy');
    }
}
