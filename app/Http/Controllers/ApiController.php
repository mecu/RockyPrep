<?php

namespace App\Http\Controllers;

use App\Helpers\BoxScore;
use App\Helpers\Helpers;
use App\Jobs\TweetGame;
use Auth;
use DB;
use App\Game;
use App\Team;
use App\Season;
use App\State;
use App\Stadium;
use App\Year;
use Illuminate\Http\JsonResponse;
use Response;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @package  ApiController
     *
     * Checks if the user is allowed to perform the requested task. Validates user credentials, and see if
     * they have been approved to use the API.
     *
     * @return bool
     */
    private function validAccess(): bool
    {
        return Auth::check();
    }

    private function badAccess(): JsonResponse
    {
        return Response::json([
            'error' => 'You do not have permission to use the API.',
        ], 403);
    }

    /**
     * Get a list of teams (limit to 10) for a state.
     *
     * @param $stateUrlName
     * @param int $offset OPTIONAL get more teams paged
     * @return string JSON array of 10 teams in state requested
     * @throws \InvalidArgumentException
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getTeams(int $offset = 0, string $stateUrlName = '')
    {
        if ($stateUrlName) {
            $state = State::where('urlname', $stateUrlName)->firstOrFail();

            return Team::where('stateid', $state->stateid)->orderBy('name')->paginate(10, [], 'page', $offset);
        }

        return Team::orderBy('name')->paginate(10, [], 'page', $offset);
    }

    /**
     * Get a single team information
     * @param int $id
     * @return Team
     */
    public function getTeam(int $id): Team
    {
        return Team::find($id);
    }

    /**
     * Search for teams by their beginning characters, optionally by a stateid
     *
     * @param Request $request
     *
     * @return string JSON array of teams that match search limited to 10, if found
     * @internal param string $term (POST) of search
     * @internal param int $stateid (POST) OPTIONAL stateid to restrict search
     *
     */
    public function getSearch(Request $request)
    {
        $search = $request->get('term');

        if ($request->filled('stateid')) {
            $teams = Team::where('name', 'LIKE', "$search%")
                ->where('stateid', $request->get('stateid'))
                ->orderBy('name')
                ->limit(10)
                ->get();
        } else {
            $teams = Team::where('name', 'LIKE', "$search%")->orderBy('name')->limit(10)->get();
        }

        if (count($teams) === 0) {
            return Response::json([
                [
                    'value' => 'No results.',
                    'loc' => 'Please remove characters and check the spelling.',
                ],
            ], 200);
        }

        $results = [];
        foreach ($teams as $key => $team) {
            if (!$team->state) {
                continue;
            }
            $results[] = [
                'urlname' => $team->urlname,
                'stateid' => $team->state->stateid,
                'loc' => $team->city . ', ' . $team->state->abbr,
                'state' => $team->state->urlname,
                'label' => $team->name,
                'id' => $team->teamid,
                'year' => Helpers::currentYear(),
            ];
        }

        return $results;
    }

    /**
     * Search for stadiums by their beginning characters, optionally by a stateid
     *
     * @param Request $request
     *
     * @return string JSON array of teams that match search limited to 10, if found
     * @internal param string $term (POST) of search
     * @internal param int $stateid (POST) OPTIONAL stateid to restrict search
     *
     */
    public function getStadiumSearch(Request $request)
    {
        $search = $request->get('term');

        if ($request->filled('stateid')) {
            $stadiums = Stadium::where('name', 'LIKE', "$search%")
                ->where('stateid', $request->get('stateid'))
                ->orderBy('name')
                ->limit(10)
                ->get();
        } else {
            $stadiums = Stadium::where('name', 'LIKE', "$search%")->orderBy('name')->limit(10)->get();
        }

        if (count($stadiums) === 0) {
            return Response::json([
                [
                    'value' => 'No results.',
                    'loc' => 'Please remove characters and check the spelling.',
                ],
            ], 200);
        }

        $results = [];
        foreach ($stadiums as $key => $stadium) {
            $results[] = [
                'label' => $stadium->name,
                'id' => $stadium->stadiumid,
            ];
        }

        return $results;
    }

    /**
     * Search for stadiums by their beginning characters, optionally by a stateid
     * @param Request $request
     *
     * @return string JSON array of stadiums that match search limited to 10, if found
     * @internal param string $term (POST) of search
     * @internal param int $stateid (POST) OPTIONAL stateid to restrict search
     *
     */
    public function getSearchStadium(Request $request)
    {
        $search = $request->get('term');
        if ($request->filled('stateid')) {
            // find teams that start with $serach with stateid
            $stadiums = Stadium::where('name', 'like', $search . '%')->where('stateid',
                $request->get('stateid'))->limit(10)->get();
        } else {
            // find teams that start with $serach
            $stadiums = Stadium::where('name', 'like', $search . '%')->limit(10)->get();
        }

        if (count($stadiums) === 0) {
            return Response::json([
                [
                    'value' => 'No results.',
                    'loc' => 'Please remove characters and check the spelling.',
                ],
            ],
                200);
        }

        $results = [];
        foreach ($stadiums as $key => $value) {
            $results[$key] = [
                'id' => $value['stadiumid'],
                'name' => $value['name'],
                'stateid' => $value['stateid'],
                'location' => $value['location'],
                'alt' => $value['alt'],
                'address' => $value['address'],
                'zip' => $value['zip'],
                'lat' => $value['lat'],
                'lng' => $value['lng'],
                'value' => $value['name'],
            ];
        }

        return $results;
    }

    /**
     * Modify a team by id.
     *
     * All data to modify shall be passed via POST. Only data to modify should be passed, but including it otherwise
     * will not harm anything.
     *
     * @package  ApiController
     *
     * @api
     *
     * @param Request $request
     * @param int $id teamid to modify
     *
     * @return string JSON array error = false and Code 200 if successful
     * @internal string $name (POST) OPTIONAL modified name
     * @internal int $stateid (POST) OPTIONAL modified stateid
     * @internal string $mascot (POST) OPTIONAL modified mascot
     * @internal string $city (POST) OPTIONAL modified city
     * @internal string $color1 (POST) OPTIONAL modified primary school color, in Hex format, must be 6 chars
     * @internal string $color2 (POST) OPTIONAL modified secondary school color, in Hex format, must be 6 chars
     * @internal string $address (POST) OPTIONAL modified address
     * @internal int $zip (POST) OPTIONAL modified zip
     * @internal string $lat (POST) OPTIONAL modified latitude
     * @internal string $lat (POST) OPTIONAL modified longitude
     * @internal string $urlname (POST) OPTIONAL modified urlname, must be globally unique
     *
     * {@internal MaxPreps variables able to be modified:
     * @internal string $maxpreps_name (POST) OPTIONAL MaxPreps URL name
     * @internal string $maxpreps_id (POST) OPTIONAL MaxPreps id UUID}
     *
     */
    public function putTeam(Request $request, $id)
    {
        if (!$this->validAccess()) {
            return $this->badAccess();
        }

        $team = Team::firstOrFail($id);

        $team->name = $request->get('name', $team->name);

        $this->setTeamOptions($request, $team);

        $team->save();

        return Response::json([
            'error' => false,
            'team-updated' => true,
        ], 200);
    }

    /**
     * Create a team.
     *
     * All data should be passed in a POST field.
     *
     * /api/v1/team
     *
     * @package  ApiController
     *
     * @api
     *
     * @internal string $name          (POST) name
     * @internal int    $stateid       (POST) stateid
     *
     * @internal string $urlname       (POST) OPTIONAL|Encouraged modified urlname, must be globally unique, will try to
     *                              auto generate, but may fail
     *
     * @internal string $mascot        (POST) OPTIONAL mascot
     * @internal string $city          (POST) OPTIONAL city
     * @internal string $color1        (POST) OPTIONAL primary school color, in Hex format, must be 6 chars
     * @internal string $color2        (POST) OPTIONAL secondary school color, in Hex format, must be 6 chars
     * @internal string $address       (POST) OPTIONAL address
     * @internal int    $zip           (POST) OPTIONAL zip
     * @internal string $lat           (POST) OPTIONAL latitude
     * @internal string $lat           (POST) OPTIONAL longitude
     *
     * {@internal MaxPreps variables stored:
     * @internal string $maxpreps_name (POST) OPTIONAL MaxPreps URL name
     * @internal string $maxpreps_id   (POST) OPTIONAL MaxPreps id UUID}
     *
     * @param Request $request
     *
     * @return string JSON array error = false, team = (int)teamid of newly generated team and Code 200 if successful
     */
    public function postTeam(Request $request)
    {
        if (!$this->validAccess()) {
            return $this->badAccess();
        }

        $team = new Team;

        // Required fields
        if (!$request->filled('name')) {
            return Response::json([
                'error' => 'name not provided. This is a required value.',
                'info' => 'name must be alpha numeric.',
            ], 400);
        }
        $team->name = $request->get('name');

        if (!$request->filled('stateid')) {
            return Response::json([
                'error' => 'stateid not provided. This is a required value.',
                'info' => 'stateid must be an integer.',
            ], 400);
        }

        $this->setTeamOptions($request, $team);

        if ($request->filled('urlname')) {
            $team->urlname = $request->get('urlname');
        } else {
            $result = $this->generateTeamURL($team);
            if (!$result) {
                return Response::json([
                    'error' => 'I could not generate a unique url name for that team. Please try again supplying a urlname value.',
                ], 409);
            }
        }

        $team->save();

        return Response::json([
            'error' => false,
            'team' => $team->teamid,
        ], 200);
    }

    /**
     * Delete a team. Currently not possible. Created for returning 403 Forbidden upon calling.
     *
     * @return string JSON array error = '' and 403 Forbidden response
     */
    public function deleteTeam()
    {
        return Response::json(['error' => 'Can not delete a team.'], 403);
    }

    /**
     * Get basic information about a specific game by id
     *
     * @param int $id
     *
     * @return string JSON array of requested game, if found
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getGame($id)
    {
        /** @var Game $game */
        $game = Game::findOrFail($id);

        $return = [
            'id' => $game->id,
            'home' => $game->home->name,
            'away' => $game->away->name,
            'date' => $game->date->format('Y-m-d'),
            'conference' => $game->confid,
            'neutral' => $game->neutral,
            'playoff' => $game->playoff,
            'forfeit' => $game->forfeit,
            'deleted' => $game->isDeleted(),
        ];

        return Response::json($return, 200);
    }

    /**
     * Edit a game
     * A soft deleted game cannot be modified. If needed, un-soft-delete with modification information, and (a second
     * request) re-soft-delete.
     *
     * @param Request $request
     *
     * @return string JSON array error = false and Code 200 if successful
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @internal param int $hometeam (POST) OPTIONAL home teamid
     * @internal param int $awayteam (POST) OPTIONAL away teamid
     * @internal param bool $undelete (POST) OPTIONAL undelete a game
     * @internal param string $date (POST) OPTIONAL date in format YYYY-MM-DD
     * @internal param bool $swap (POST) OPTIONAL This will swap the home/away
     * @internal param bool $neutral (POST) REQUIRED* true if played at a truly neutral site, assumed false if omitted
     * @internal param bool $conference (POST) REQUIRED* true if this is a conference game, assumed false if omitted
     * @internal param bool $playoff (POST) REQUIRED* true if the game is a playoff, assumed false if omitted
     * @internal param int $homescore (POST) OPTIONAL home score
     * @internal param int $awayscore (POST) OPTIONAL away score
     * @internal param int $winner (POST) REQUIRED if homescore and awayscore are provided, 0 if a tie
     * @internal param bool $forfeit (POST) OPTIONAL true if the game was won by a forfeit, must also provide
     *           homescore, awayscore and winner
     * @internal param int $stadiumid (POST) OPTIONAL stadiumid of the location
     * @internal param int $A1 (POST) OPTIONAL for all A1-A8 ... An, int of quarter score for away, if An provided, Hn
     *           must be provided
     * @internal param int $H1 (POST) OPTIONAL for all H1-H8 ... Hn, int of quarter score for home, if Hn provided, An
     *           must be provided
     *
     */
    public function putGame(Request $request)
    {
        if (!$this->validAccess()) {
            return $this->badAccess();
        }

        if (!$request->filled('id')) {
            return Response::json([
                'error' => 'No game ID provided.',
            ], 400);
        }

        /* @var Game $game */
        $game = Game::withTrashed()->findOrFail($request->get('id'));

        // See if it's soft deleted
        if ($game->isDeleted()) {
            // If they want to un-delete, do that
            if ($request->get('undelete', false)) {
                $game->restore();
            } else {
                // We don't otherwise modify soft deleted games, so abort
                return Response::json([
                    'error' => 'Can not modify soft deleted games. Please restore first (or restore in conjunction with modifications).',
                ], 403);
            }
        }

        // Record if the game already has a winner, for tweeting determination later
        $haveWinner = $game->winner !== null;

        // Modify however otherwise specified
        if ($request->filled('hometeam')) {
            $game->hometeam = (int)$request->get('hometeam');
        }
        if ($request->filled('awayteam')) {
            $game->awayteam = (int)$request->get('awayteam');
        }

        if ($request->filled('date')) {
            $game->date = $request->get('date');
        }

        if ($request->filled('homescore')) {
            $game->homescore = (int)$request->get('homescore');
        }
        if ($request->filled('awayscore')) {
            $game->awayscore = (int)$request->get('awayscore');
        }
        if ($request->filled('winner')) {
            $winner = (int)$request->get('winner');

            if (!$request->filled('homescore') || !$request->filled('awayscore')) {
                return Response::json([
                    'error' => 'If you provide a winner, you must provide both homescore and awayscore as well.',
                ], 400);
            } else {
                $homeScore = (int)$request->get('homescore');
                $awayScore = (int)$request->get('awayscore');

                if ($homeScore > $awayScore) {
                    if ($game->home->teamid === $winner) {
                        $game->setWinner($winner);
                    } else {
                        return Response::json([
                            'error' => 'The winner provided did not match the scores provided.',
                        ], 400);
                    }
                } elseif ($homeScore < $awayScore) {
                    if ($game->away->teamid === $winner) {
                        $game->setWinner($winner);
                    } else {
                        return Response::json([
                            'error' => 'The winner provided did not match the scores provided.',
                        ], 400);
                    }
                } elseif ($homeScore === $awayScore) {
                    if ($winner === 0) {
                        $game->setWinner(0);
                    } else {
                        return Response::json([
                            'error' => 'The scores indicate a tie, but the winner was not set to 0.',
                        ], 400);
                    }
                }

                // Process Box Scores, if provided
                $homeScores = $request->get('homescores', []);
                $awayScores = $request->get('awayscores', []);

                $homeScores = static::processFormScores($homeScores, 'homescores');
                $awayScores = static::processFormScores($awayScores, 'awayscores');

                $homeScores = BoxScore::arrayRemoveEmpty($homeScores);
                $awayScores = BoxScore::arrayRemoveEmpty($awayScores);

                if (!empty($homeScores) && !empty($awayScores)) {
                    $scores = [];
                    foreach ($homeScores as $quarter => $score) {
                        $scores[$quarter] = ['home' => (int)$score, 'away' => (int)$awayScores[$quarter]];
                    }
                    BoxScore::processScores($scores, $game);
                }
            }
        }

        if ($request->filled('conference')) {
            $game->confid = (bool)$request->get('conference');
        }
        if ($request->filled('neutral')) {
            $game->neutral = (bool)$request->get('neutral');
        }
        if ($request->filled('forfeit')) {
            $game->forfeit = (bool)$request->get('forfeit');
        }
        if ($request->filled('playoff')) {
            $game->playoff = (bool)$request->get('playoff');
        }

        if ($request->filled('stadiumid') && $request->filled('stadiumid')) {
            $game->stadiumid = $request->get('stadiumid', null);
        }

        if ($request->get('swap', false)) {
            $temp = $game->away;
            $game->awayteam = $game->home->teamid;
            $game->hometeam = $temp->teamid;

            // Swap the scores too!
            if ($game->hasWinner()) {
                // Swap the box scores too!
                if ($game->hasBoxscore()) {
                    return Response::json([
                        'error' => 'The game has boxscores which cannot be swapped.',
                    ], 400);
                }

                $temp = $game->awayscore;
                $game->awayscore = $game->homescore;
                $game->homescore = $temp;
            }
        }

        $game->human = true;

        $saved = $game->save();

        if (!$haveWinner && $game->winner !== null) {
            // Dispatching the job so it's not serial would be ideal, but the queue system doesn't work anyways
            $tweet = new TweetGame($game, $game->home->state);
            $tweet->handle();
            if ($game->home->state !== $game->away->state) {
                $tweet = new TweetGame($game, $game->away->state);
                $tweet->handle();
            }
        }

        return Response::json([
            'success' => true,
            'game-updated' => true,
            'saved' => $saved,
        ], 200);
    }

    /**
     * Create a game
     *
     * param int $hometeam (POST) home teamid
     * param int $awayteam (POST) away teamid
     * param int $team (POST) team teamid This/opponent or hometeam/awayteam must be used, but not both
     * param int $opponent (POST) opponent teamid
     * param string $date (POST) date in format YYYY-MM-DD
     * param bool $away (POST) OPTIONAL If the game is away. Will set the team as awayteam, and opponent as hometeam,
     *       ignored if hometeam/awayteam combo used
     * param bool $neutral (POST) OPTIONAL true if played at a truly neutral site
     * param bool $conference (POST) OPTIONAL true if this is a conference game
     * param bool $playoff (POST) OPTIONAL true if the game is a playoff
     * param int $homescore (POST) OPTIONAL home score
     * param int $awayscore (POST) OPTIONAL away score
     * param int $winner (POST) REQUIRED if homescore and awayscore are provided, 0 if a tie
     * param bool $forfeit (POST) OPTIONAL true if the game was won by a forfeit, must also provide homescore,
     *       awayscore and winner
     * param int $stadiumid (POST) OPTIONAL stadiumid of the location
     *
     * @param Request $request
     * @return string JSON array error = false, game = gameid of created game and Code 200 if successful
     */
    public function postGame(Request $request)
    {
        if (!$this->validAccess()) {
            return $this->badAccess();
        }

        /* @var Game $game */
        $game = new Game;

        // If they provide the hometeam, just set the values that way
        if ($request->filled('hometeam')) {
            $hometeam = Team::find((int)$request->get('hometeam'));
            $awayteam = Team::find((int)$request->get('awayteam'));
            $game->hometeam = $hometeam->teamid;
            $game->awayteam = $awayteam->teamid;
        } elseif ($request->filled('teamid')) {
            // Otherwise, set based on the away value
            $team = Team::find((int)$request->get('teamid'));
            $opponent = Team::find((int)$request->get('opponentid'));
            if ($request->filled('away') && (bool)$request->filled('away') === true) {
                $game->hometeam = $opponent->teamid;
                $game->awayteam = $team->teamid;
            } else {
                $game->awayteam = $opponent->teamid;
                $game->hometeam = $team->teamid;
            }
        } else {
            return Response::json([
                'error' => 'hometeam or team not provided. One of these is a required value.',
                'info' => 'hometeam or team must be an integer.',
            ], 400);
        }

        if (!$request->filled('date')) {
            return Response::json([
                'error' => 'date not provided. This is a required value.',
                'info' => 'date must be in the format YYYY-MM-DD.',
            ], 400);
        }
        $game->date = $request->get('date');

        // Dummy value
        $game->time = '00:00:00';

        // Optional fields
        // If we have 1 of these, we must have all of these:
        if ($request->filled('homescore') || $request->filled('awayscore') || $request->filled('winner') || $request->filled('forfeit')) {

            if (!$request->filled('homescore') || !$request->filled('awayscore') || !$request->filled('winner') || !$request->filled('forfeit')) {
                return Response::json([
                    'error' => 'You provided homescore and/or awayscore and/or winner and/or forfeit, but not all 4 values.',
                    'info' => 'If you provide 1 of these values, all 4 are required.',
                ], 400);
            }
            $game->homescore = $request->get('homescore');
            $game->awayscore = $request->get('awayscore');
            $game->winner = $request->get('winner');
        }

        $game->confid = (bool)$request->get('conference', false);
        $game->neutral = (bool)$request->get('neutral', false);
        $game->forfeit = (bool)$request->get('forfeit', false);
        $game->playoff = (bool)$request->get('playoff', false);
        $game->stadiumid = $request->get('stadiumid', null);
        $game->human = $request->get('human', false);

        $game->save();

        list($year) = explode('-', $game->date);
        // Check if they are added to the season
        Season::firstOrCreate([
            'teamid' => $game->hometeam,
            'year' => $year,
        ]);
        Season::firstOrCreate([
            'teamid' => $game->awayteam,
            'year' => $year,
        ]);

        return Response::json([
            'error' => false,
            'game' => $game->gameid,
        ], 200);
    }

    /**
     * This soft deletes a game
     *  /api/v1/game
     *
     * @package ApiController
     * @api
     * @param Request $request
     * @return string JSON array[success = true], Code 200 if successful
     * @throws \Exception
     */
    public function deleteGame(Request $request)
    {
        if (!$this->validAccess()) {
            return $this->badAccess();
        }

        if (!$request->filled('id')) {
            return Response::json(['error' => 'No ID provided.'], 400);
        }

        $game = Game::findOrFail($request->get('id'));
        $game->human = 1;
        $game->save();
        $game->delete();

        return Response::json(['success' => true], 200);
    }

    /**
     * Get the schedule of games for a specific team and season
     * (Optionally include soft deleted games.)
     *
     * @param int $id (POST) team id
     * @param int $year (POST) year (season) format: YYYY
     * @param string $all (POST) set to 'all' if soft deleted games should be included in the results
     *
     * @return string JSON array of schedule of games for specified team and season, just gameids and dates, and
     *                Code 200 if successful
     */
    public function getSchedule($id, $year, $all)
    {
        // get schedule for team for season
        // just provides gameids and dates, should it do more?

        $yearObj = new Year($year);

        $teamObj = Team::findOrFail($id);

        /* @var $season Season */
        $season = Season::ofTeam($teamObj)->ofYear($yearObj)->firstOrFail();

        return $season->getSchedule($all === 'all')->toJson();
    }

    /**
     * Get the schedule of games for a specific state and date
     *
     * @param string|State $state (POST) state urlname
     * @param string|int $year (POST) year in format YYYY
     * @param string|int $month (POST) month in format MM
     * @param string|int $day (POST) day in format DD
     *
     * @return string JSON array of games scheduled on that date and state provided and Code 200 if successful
     */
    public function getDay($state, $year, $month, $day)
    {
        // Schedule for a day for state
        $today = new \DateTime("$year-$month-$day");

        $state = State::where('urlname', $state)->firstOrFail();

        $gameListHome = DB::table('game')->where('date', $today->format('Y-m-d'))->where('deleted', 0)->where('stateid', $state->stateid)
            ->join('team', 'team.teamid', '=', 'game.hometeam')->select('gameid', 'date')->get();
        $gameListAway = DB::table('game')->where('date', $today->format('Y-m-d'))->where('deleted', 0)->where('stateid', $state->stateid)
            ->join('team', 'team.teamid', '=', 'game.awayteam')->select('gameid', 'date')->get();

        return array_merge($gameListHome, $gameListAway);
    }

    /**
     * Update season information
     *
     * @param Request $request
     *
     * @return string JSON array error = false and Code 200 if successful
     */
    public function putSeason(Request $request)
    {
        if (!$this->validAccess()) {
            return $this->badAccess();
        }

        if (!$request->filled('id')) {
            return Response::json(['error' => 'No team ID provided.'], 400);
        }

        if (!$request->filled('year')) {
            return Response::json(['error' => 'No year ID provided.'], 400);
        }

        $team = Team::find($request->get('id'));
        $year = new Year($request->get('year'));

        /** @var Season $season */
        $season = Season::ofTeam($team)->ofYear($year)->firstOrFail();

        if ($request->filled('level')) {
            $season->levelid = (int)$request->get('level');
        }
        if ($request->filled('levelState')) {
            $season->levelid_state = (int)$request->get('levelState');
        }

        $season->level_alter = null;

        if ($request->filled('playup')) {
            $season->level_alter = 1;
        } elseif ($request->filled('playdown')) {
            $season->level_alter = -1;
        }

        $season->save();

        return Response::json([
            'error' => false,
            'season-updated' => true,
        ], 200);
    }

    /**
     * Converts "homescore[0]=0&homescore[1]=7&homescore[2]=3&homescore[3]=21
     * to [1 => 0, 2 => 7, 3 => 3, 4 => 21]
     *
     * @param string $scores
     * @param string $match
     * @return array
     */
    private static function processFormScores(string $scores, string $match): array
    {
        $scores = urldecode($scores);
        preg_match_all("/$match\[\d\]=(\d+)/", $scores, $matches);
        $scores = $matches[1];
        array_unshift($scores, null);
        unset($scores[0]);

        return $scores;
    }

    private function generateTeamURL(Team $team): bool
    {
        $badChars = [' ', '\'', '.', '"', '\\', '/', '%', '!', '@', '&'];

        // Generate the url name
        $URLname = str_replace($badChars, '_', strtolower($team->name . '_' . $team->state->abbr));
        $urlCheck = Team::where('urlname', $URLname)->first();

        // Test the url name
        if ($urlCheck !== null) {
            // Fix the url name with the city
            $URLname = str_replace($badChars, '_', strtolower($team->name . '_' . $team->city . '_' . $team->state->abbr));
            $urlCheck = Team::where('urlname', $URLname)->first();
            if ($urlCheck !== null) {
                return false;
            }
        }

        $team->urlname = $URLname;

        return true;
    }

    private function setTeamOptions(Request $request, Team $team)
    {
        // Optional fields
        $team->stateid = $request->get('stateid', $team->stateid);
        $team->mascot = $request->get('mascot', $team->mascot);
        $team->city = $request->get('city', $team->city);
        $team->jv = $request->get('jv', $team->jv);
        $team->color1 = $request->get('color1', $team->color1);
        $team->color2 = $request->get('color2', $team->color2);
        $team->address = $request->get('address', $team->address);
        $team->zip = $request->get('zip', $team->zip);
        $team->lat = $request->get('lat', $team->lat);
        $team->lng = $request->get('lng', $team->lng);
        $team->maxpreps_name = $request->get('maxpreps_name', $team->maxpreps_name);
        $team->maxpreps_id = $request->get('maxpreps_id', $team->maxpreps_id);
    }
}
