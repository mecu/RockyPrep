<?php

namespace App\Faker;

use Faker\Provider\Base;

class CustomFakerProvider extends Base
{
    public function customName()
    {
        $file_arr = file(realpath(dirname(__FILE__)) . '/words.txt');
        $num_lines = count($file_arr);
        $last_arr_index = $num_lines - 1;
        $rand_index = rand(0, $last_arr_index);
        $rand_text = $file_arr[$rand_index];

        return strtolower(trim($rand_text));
    }
}
