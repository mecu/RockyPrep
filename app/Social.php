<?php namespace App;

use Config;
use Eloquent;
use Twitter;

class Social extends Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'social';

    public function tweet($state, $message)
    {
        $social = Social::where('stateid', $state->stateid)->get();
        Config::set('twitter::ACCESS_TOKEN', $social->accesstoken());
        Config::set('twitter::ACCESS_TOKEN_SECRET', $social->accesstokensecret());

        Twitter::postTweet(['status' => $message, 'format' => 'json']);
    }
}
