<?php namespace App;

use Eloquent;

/**
 * Class Tweet
 */
class Tweet extends Eloquent
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
