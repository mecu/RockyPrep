<?php

namespace App;

use App\Helpers\Helpers;

class Year
{
    public $year, $start, $display, $lasttwo, $end;

    /**
     * @param int $year
     */
    public function __construct(int $year = null)
    {
        if ($year === null) {
            $year = Helpers::currentYear();
        }

        $this->year = (string)$year;

        $this->start = $this->year . '-07-01';
        $this->end = ($year + 1) . '-06-30';
        $this->display = $this->year . '-' . ($year + 1);

        $this->lasttwo = substr($this->year, -2);

        $this->lasttwo2 = substr($this->year, -2) . '-' . substr((string)($year + 1), -2);
    }

    public function __toString()
    {
        return $this->year;
    }

    /**
     * @param string $end
     *
     * @throws \ErrorException
     */
    public function setEnd($end)
    {
        if (is_string($end)) {
            $this->end = $end;
        } else {
            throw new \ErrorException('$end was not a string');
        }
    }

    public function getEnd(): string
    {
        return $this->end;
    }
}
