<?php namespace App;

use Eloquent;

/**
 * Class Cutoff
 */
class Cutoff extends Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cutoff';

    /**
     * Columns to convert into Carbon date objects
     *
     * @var array
     */
    protected $dates = ['date'];
}
