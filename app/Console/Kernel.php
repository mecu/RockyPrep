<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    private $path = '/home/rp/storage/reports/';

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('Scrape:Today')
            ->cron('5 */2 * 8-12 *')
            ->withoutOverlapping()
            ->sendOutputTo($this->path . 'today.html');

        $schedule->command('Calc:RPI 2018')
            ->cron('0 */2 * 8-12 *')
            ->withoutOverlapping()
            ->sendOutputTo($this->path . 'rpi.html');

        $schedule->command('Calc:SRS 2018')
            ->cron('20 */4 * 8-12 *')
            ->withoutOverlapping()
            ->sendOutputTo($this->path . 'srs.html');

        $schedule->command('Calc:SRSPredict')
            ->cron('59 */4 * 8-12 *')
            ->withoutOverlapping();

        for ($i = 1; $i < 52; ++$i) {
            $minutes = $i * 10;
            $hour = 3 + floor($minutes / 60);
            $minute = $minutes % 60;
            $schedule->command("Scrape:State $i 2018")
                ->cron("$minute $hour * 6-12 0,2,4,5,6");
        }

        for ($i = 1; $i < 52; ++$i) {
            $minutes = $i * 10;
            $hour = (15 + floor($minutes / 60)) % 24;
            $minute = $minutes % 60;
            $schedule->command("Scrape:Boxscore $i")
                ->cron("$minute $hour * 8-12 0,4,5,6");
        }


        $schedule->command('horizon:snapshot')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
