<?php

namespace App\Console\Commands;

use App\Season;
use App\State;
use App\Team;
use App\Year;
use Illuminate\Console\Command;

class ScrapeStateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Scrape:State
							{state : State id}
							{season : Season (Year)}
							{--existing=true? : Only scrape teams that have existing games.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape the schedules for an entire state.';

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle()
    {
        $state = State::find($this->argument('state'));
        $year = new Year((int)$this->argument('season'));

        $teams = Team::where('stateid', $state->stateid)->get();

        $teamCount = count($teams);

        ob_start();
        $output = fopen(storage_path() . "/reports/{$state->urlname}.html", 'wb');

        $this->info("State: {$state->name} | Number of teams in this state: $teamCount.");

        fwrite($output, '<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Reports - '. $state->name .'</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link type="text/css" media="all" rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    </head>
    <body>');
        fwrite($output, '<h2>This file was generated: ' . date('Y-m-d H:m:s') . '</h2>');

        foreach ($teams as $key => $team) {
            if ($team->teamid === 0) {
                continue;
            }

            // See if this team actually has games this season, if request to only scan existing teams
            // The first time scanning a season, this flag should not be set.
            if ($this->option('existing') === 'true') {
                $season = Season::ofTeam($team)->ofYear($year)->first();

                if ($season === null || $season->getTotalGames($year) === 0) {
                    continue;
                }
            }

            $this->call('Scrape:Schedule', [
                'team' => $team->teamid,
                'season' => $year->year,
            ]);
        }

        fwrite($output, ob_get_flush());
        fwrite($output, '</body></html>');
        fclose($output);
        $this->info('Done.');

        return true;
    }
}
