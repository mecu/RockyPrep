<?php
namespace App\Console\Commands;

use App\Year;
use Carbon\Carbon;
use App\Game;
use Illuminate\Console\Command;

class ScrapeTodayCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Scrape:Today
                            {--date= : Date to consider "Today"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape for results for today\'s games';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');

        // Grab all of Today's games
        if ($this->option('date')) {
            $today = Carbon::createFromFormat('Y-m-d', $this->option('date'))->format('Y-m-d');
        } else {
            $today = Carbon::now('America/Los_Angeles')->format('Y-m-d');
        }

        $gameList = Game::where('date', $today)
            ->whereNull('winner')
            ->with('home', 'away')
            ->get();

        ob_start();
        echo '<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Daily Scrape Report</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link type="text/css" media="all" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    </head>
    <body>';

        $count = 0;
        foreach ($gameList as $key => $game) {
            /* @var $game Game */
            $year = new Year($game->date->format('Y'));

            echo '<div class="card"><div class="card-text">';

            $this->call('Scrape:Schedule', ['team' => $game->home->teamid, 'season' => $year->year]);
            echo '</div></div>';
            ++$count;
        }
        echo "<span class=\"label label-success\">All done. $count</span>";
        $file = fopen(storage_path() . '/reports/daily.html', 'wb');
        fwrite($file, ob_get_contents());
        ob_end_flush();
        fclose($file);
    }
}
