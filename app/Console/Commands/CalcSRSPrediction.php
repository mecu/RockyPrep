<?php declare(strict_types=1);
namespace App\Console\Commands;

use App\Game;
use App\Season;
use App\Year;
use Illuminate\Console\Command;

/**
 * Class CalcSRSPrediction
 * @package App\Console\Commands
 */
class CalcSRSPrediction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Calc:SRSPredict';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the SRS Predictions for remaining games for the current season';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        $year = new Year();
        $year->start = date('Y-m-d');

        // Get list of ALL games that are scheduled this season after today that do not have a winner
        $games = Game::whereNull('winner')
            ->whereBetween('date', [$year->start, $year->end])
            ->with('home', 'away')
            ->get();

        $this->info('SRS Predictor for: ' . $year->year . PHP_EOL);

        $bar = $this->output->createProgressBar(count($games));

        // Loop through the games and figure out the prediction
        foreach ($games as $key => $game) {
            $homeSeason = Season::firstOrCreate([
                'teamid' => $game->home->teamid,
                'year' => $year->year,
            ]);
            $awaySeason = Season::firstOrCreate([
                'teamid' => $game->away->teamid,
                'year' => $year->year,
            ]);

            if ($homeSeason->srs > $awaySeason->srs) {
                $game->srs_predict = $homeSeason->srs - $awaySeason->srs;
                $game->srs_teamid = $game->hometeam;
            } elseif ($homeSeason->srs < $awaySeason->srs) {
                $game->srs_predict = $awaySeason->srs - $homeSeason->srs;
                $game->srs_teamid = $game->awayteam;
            } else {
                $game->srs_predict = NULL;
                $game->srs_teamid = NULL;
            }

            $game->save();

            $bar->advance();
        }

        $this->info('All done.' . PHP_EOL);
    }
}
