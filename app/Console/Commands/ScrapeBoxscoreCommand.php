<?php namespace App\Console\Commands;

use App\Game;
use App\Helpers\BoxScore;
use App\State;
use App\Team;
use App\Year;
use Illuminate\Console\Command;
use QueryPath\DOMQuery;

class ScrapeBoxscoreCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Scrape:Boxscore
							{state : State id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Boxscores from MaxPreps';

    /**
     * Scrape boxscore pages, get results
     */
    public function handle()
    {
        $state = State::find($this->argument('state'));

        $year = new Year();

        $this->info('Scraping Boxscores for: ' . $state->name . PHP_EOL);

        if ($year->end > date('Y-m-d')) {
            $year->setEnd(date('Y-m-d'));
        }

        // Get a list of teams in this state
        $teams = $state->teams;

        // Get a list of game dates in this state for this season before today
        /** @var string[] $dateList */
        $dateList = Game::select('date')
            ->whereIn('hometeam', $teams->pluck('teamid'))
            ->whereBetween('date', [$year->start, $year->end])
            ->distinct()
            ->orderBy('date', 'DESC')
            ->get()
            ->pluck('date');

        foreach ($dateList as $date) {
            $url = "http://www.maxpreps.com/list/schedules_scores.aspx?state={$state->abbr}&gendersport=boys,football&date={$date->format('n/j/Y')}";
            $this->info("{$date->format('Y-n-d')}: ( $url ):");

            // QueryPath
            $qp = html5qp($url);
            /** @var DOMQuery $tables */
            $tables = qp($qp, 'li.c-s ul');

            /** @var DOMQuery $table */
            foreach ($tables->branch()->find('li.c') as $gameScore) {
                if ($gameScore->branch()->find('div.details')->text() !== ' Final') {
                    // Not a final score
                    continue;
                }
                $gameDetailLink = $gameScore->branch()->find('a.c-c')->attr('href');

                $gameDetail = html5qp($gameDetailLink);
                if (! $gameDetail instanceof DOMQuery) {
                    $this->error('MaxPreps page not loaded.');
                    continue;
                }

                $maxPrepsContestAction = $gameDetail->branch()->find('#aspnetForm')->attr('action');
                if ($maxPrepsContestAction === null) {
                    $this->error('MaxPreps contest action could not be extracted:' . $gameDetailLink);
                    continue;
                }

                $matches = preg_match('/contestid=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/', $maxPrepsContestAction, $maxPrepsContestID);
                if ($matches !== 1) {
                    $this->error('MaxPreps ID could not be extracted:' . $maxPrepsContestAction);
                    continue;
                }
                $maxPrepsContestID = $maxPrepsContestID[1];

                // Find the game by this ID
                $game = Game::where('contestid', $maxPrepsContestID)->first();
                if ($game === null || $game->winner === null) {
                    // Don't have this game or there isn't a winner, scrape schedule
                    $awayUrl = $gameDetail->branch()->find('th.team.first a')->attr('href');
                    $matches = preg_match('/schoolid=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/', $awayUrl, $awaySchoolID);

                    if ($matches === 1) {
                        $awayTeam = Team::select('teamid')->where(['maxpreps_id' => $awaySchoolID])->first();
                        if ($awayTeam !== null) {
                            $this->info("I don't have this game, so I'm scraping the away team.");
                            $this->call('Scrape:Schedule', ['team' => $awayTeam->teamid, 'season' => $year->year]);
                        } else {
                            $this->error("I don't have this game but I couldn't figure out the away team to scrape.");
                        }
                    }

                    continue;
                }

                if ($game === null || $game->hasBoxscore()) {
                    continue;
                }

                // Gather Boxscore
                $awayScores = [];
                $homeScores = [];

                $away = $gameDetail->branch()->find('table.boxscore tr.first');

                foreach ($away->branch()->find('td.score:not(.total)') as $score) {
                    $awayScores[] = $score->text();
                }

                $home = $gameDetail->branch()->find('table.boxscore tr.last');
                foreach ($home->branch()->find('td.score:not(.total)') as $score) {
                    $homeScores[] = $score->text();
                }

                $scores = [];
                $homeTotal = 0;
                $awayTotal = 0;
                foreach ($homeScores as $quarter => $score) {
                    $scores[$quarter] = ['home' => (int)$score, 'away' => (int)$awayScores[$quarter]];
                    $homeTotal += (int)$score;
                    $awayTotal += (int)$awayScores[$quarter];
                }

                // Make sure the scores add up to the current total we have
                if ($homeTotal === $game->homescore && $awayTotal === $game->awayscore) {
                    array_unshift($scores, null);
                    unset($scores[0]);

                    BoxScore::recordNewScores($scores, $game);
                }
            }
        }
    }
}
