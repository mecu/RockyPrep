<?php

namespace App\Console\Commands;

use App\Game;
use App\Helpers\Scraper;
use App\Http\Controllers\Admin\TeamController;
use App\Jobs\CalcRPIforTeam;
use App\Season;
use App\Stadium;
use App\Team;
use App\Year;
use Carbon\Carbon;
use Illuminate\Console\Command;
use QueryPath\DOMQuery;

// TODO: Remove HTML formatting
class ScrapeScheduleCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Scrape:Schedule
							{team : Team id}
							{season? : Season (year) optional, defaults to YEAR}
							{url? : The url to scrape (optional)';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape a schedule for specific team and sport and season.';

    /**
     * Get schedule information
     * Date, Opponent, Conference, Playoff, Tournament, Contest URL (22 chars)
     *
     * @return int Number of games added/updated
     */
    public function handle()
    {
        $team = Team::find($this->argument('team'));

        if ($team->teamid === 0 || !is_object($team)) {
            throw new \RuntimeException('Team not found? or Team was not an object!');
        }

        $year = new Year((int)$this->argument('season'));

        echo "<div class='card'>
                <div class='card-body'>
                    <h3 class='card-title'>{$team->name} for {$year->year}</h3>
                    <div class='card-text'>";

        if (!array_key_exists('url', $this->argument())) {
            if ($team->maxpreps_name === null) {
                echo 'No MaxPreps name for this team.</div></div>';

                return false;
            }
            /*
            ** generate the URL since it wasn't provided
            ** $mobileurlform = $sport_array[$sportid]['maxpreps_mobile_sport_form'];
            ** $season = $sport_array[$sportid]['maxpreps_season'];
            **
            ** mobile
            ** $url = 'http://www.maxpreps.com/' . urlencode( 'm/team/schedule.aspx?gendersport='.$mobileurlform.'&schoolid='.$teamarray[$teamid]->get_maxpreps_id().'&season='.$season.'&teamlevel=59a4c93d-374c-42ea-9b7f-87e1e51f1336' );
            **
            ** regular
            ** $url = "http://www.maxpreps.com/high-schools/{$team->maxpreps_name}/{$sport->maxpreps_url_form}/schedule.htm";
            */
            $url = "http://www.maxpreps.com/high-schools/{$team->maxpreps_name}/football-fall-{$year->lasttwo}/schedule.htm";
        } else {
            $url = $this->argument('url');
        }

        echo "Loading schedule for {$team->name} (teamid= {$team->teamid} ) <a href=\"$url\" target=\"_blank\">MaxPreps Link</a> <a target=\"_blank\" href=\"https://rockyprep.com/{$team->state->urlname}/team/{$team->urlname}/{$year->year}\">RockyPrep Link</a>";

        $page = Scraper::fetch($url);
        if (empty($page)) {
            echo '<span class="label label-warning">The scraper did not retrieve the page.</span></div></div></div>';
            return 0;
        }

        $games = [];
        $preventDeletion = false;

        /** @var DOMQuery $qp */
        $qp = \QueryPath::withHTML5($page);

        /** @var DOMQuery $tr */
        foreach ($qp->find('#schedule tr.dual-contest, #schedule tr.tac-close-row') as $tr) {
            echo '<div>';

            if (strripos($tr->branch()->find('.contest-location')->text(), 'scrimmage') !== false) {
                echo '<span class="label label-warning">Scrimmage game.</span></div>';
                continue;
            }

            $eventTimeMaxPreps = $tr->branch()->find('.event-time')->attr('title');
            if (empty($eventTimeMaxPreps)) {
                echo '<span class="label label-warning">No date.</span></div>';
                continue;
            }

            // maxpreps datetime: 2014-05-23T07:00:00
            // See if the time is TBA
            if ($eventTimeMaxPreps === 'To be Announced' || $tr->branch()->find('.event-time')->text() === 'TBA') {
                echo '<span class="label label-info">TBA game</span> ';

                $rawDate = $tr->branch()->find('.event-date')->text();

                if (strpos($rawDate, 'TBA') !== false) {
                    $this->info('TBA game date.');
                    continue;
                }

                $eventDateMaxPreps = explode('/', $rawDate);

                if (count($eventDateMaxPreps) < 2) {
                    $this->error('Could not split date: ' . $rawDate);
                    continue;
                }
                $maxprepsDate = Carbon::createFromDate($year->year, $eventDateMaxPreps[0], $eventDateMaxPreps[1], 'UTC');
            } else {
                // A proper event-time datetime was found
                $maxprepsDate = new Carbon($eventTimeMaxPreps, 'UTC');
                $maxprepsDate->timezone($team->state->timezone);
            }

            if (!is_object($maxprepsDate)) {
                throw new \RuntimeException('Date object not created in ScapeScheduleCommand: E1');
            }

            if ($maxprepsDate->format('Y-m-d') === '1969-12-31') {
                echo '<span class="label label-warning">Bad Date</span></div>';
                continue;
            }

            $date = $maxprepsDate->format('Y-m-d');

            $maxprepsOpponent = $tr->branch()->find('.contest-type-indicator')->text();

            // Grab maxpreps-id for comparison
            $opponentMaxPrepsLink = $tr->branch()->find('a.contest-type-indicator')->attr('href');
            if (empty($opponentMaxPrepsLink)) {
                echo '<span class="label label-warning">Opponent MaxPreps link not found</span></div>';
                continue;
            }

            $matches = preg_match('/schoolid=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/', $opponentMaxPrepsLink, $opponentMaxPrepsID);
            if ($matches !== 1) {
                echo '<span class="label label-warning">Opponent MaxPreps ID not found</span></div>';
                continue;
            }
            $opponentMaxPrepsID = $opponentMaxPrepsID[1];

            //This ID is "Varsity Opponent", no reason to bother storing that
            if ($opponentMaxPrepsID === '23bae8a4-95f3-4f43-99bb-9c0a28566b66') {
                echo '<span class="label label-warning">Varsity Opponent</span></div>';
                continue;
            }
            //This ID is "Non Varsity Opponent", no reason to bother storing that
            if ($opponentMaxPrepsID === '4fc9dcd1-3d2c-4d0a-998e-4340bac0ac02') {
                echo '<span class="label label-warning">Non Varsity Opponent</span></div>';
                continue;
            }
            //This ID is "Non Varsity Tournament Opponent", no reason to bother storing that
            if ($opponentMaxPrepsID === '001393ea-2648-404b-8205-6e19ac70a214') {
                echo '<span class="label label-warning">Non Varsity Tournament Opponent</span></div>';
                continue;
            }
            //This ID is "Non JV Opponent", no reason to bother storing that
            if ($opponentMaxPrepsID === 'd1bd084e-1f5b-4e7c-8f99-6c63e0e9e855') {
                echo '<span class="label label-warning">Non JV Opponent</span></div>';
                continue;
            }

            if ($maxprepsOpponent === 'TBA') {
                echo ' TBA opponent';
                $opponent = Team::find(0);
            } else {
                // Find maxpreps_id in database
                $opponent = Team::where(['maxpreps_id' => $opponentMaxPrepsID])->first();
                // If we don't have this team, try to auto-create it
                if ($opponent === null) {
                    // Fake a request
                    $request = \Illuminate\Http\Request::create('', 'GET', ['url' => 'http://www.maxpreps.com' . $opponentMaxPrepsLink]);
                    try {
                        TeamController::storeAuto($request);
                    } catch (\ErrorException $e) {
                        echo '<span class="label label-warning">I tried to create a team and failed: ' . $opponentMaxPrepsLink . '</span></div>';
                    }

                    // It should have created, so, try and find the team now
                    $opponent = Team::where(['maxpreps_id' => $opponentMaxPrepsID])->first();

                    // If it doesn't exist here, log it and move on
                    if ($opponent === null) {
                        echo '<span class="label label-warning">I tried to create a team and failed: ' . $opponentMaxPrepsLink . '</span></div>';

                        $failedAutoAddTeams = fopen(storage_path('reports/failed_auto_add_teams.html'), 'ab');
                        fwrite($failedAutoAddTeams, '<li>' . date('Y-m-d') .
                            ': <a href="http://www.maxpreps.com' . $opponentMaxPrepsLink . '">' . $opponentMaxPrepsLink . '</a></li>' . PHP_EOL);
                        fclose($failedAutoAddTeams);
                    }

                    echo '<span class="label label-info">I just created this team.</span></div>';
                }
            }

            if (!is_object($opponent)) {
                continue;
            }

            $away = false;
            if ($tr->branch()->find('.away-indicator')->text()) {
                $away = true;
            }

            if ($away) {
                $awayteam =& $team;
                $hometeam =& $opponent;
            } else {
                $awayteam =& $opponent;
                $hometeam =& $team;
            }

            if (!is_object($opponent)) {
                continue;
            }

            if ($opponent->teamid) {
                echo "date: $date | opponent: ";
                if ($away) {
                    echo '@';
                }
                echo " $maxprepsOpponent (<small> $opponentMaxPrepsID </small>) | opponent ID: {$opponent->teamid}";
            } else {
                echo "date: $date | opponent: $away $maxprepsOpponent (<small> $opponentMaxPrepsID </small>) | <div class=\"label label-danger\">Opponent not found in database</div>.";
                continue;
            }

            $contestHref = $tr->branch()->find('.result a')->attr('href');
            if ($contestHref === null) {
                $preventDeletion = true;
                echo '<div class="label label-error">ERROR: No contest href found.</div>';
                continue;
            }

            $result = preg_match('/contestid=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/', $contestHref, $contestId);
            if ($result === 0) {
                $preventDeletion = true;
                echo '<div class="label label-error">ERROR: No contest ID found.</div>';
                continue;
            }
            $contestId = $contestId[1];

            // See if we have this game
            $game = Game::where(['contestid' => $contestId])->withTrashed()->first();

            if ($game === null) {
                // Maybe we have the date, but not the ID?
                $game = Game::where([
                    'hometeam' => $hometeam->teamid,
                    'awayteam' => $awayteam->teamid,
                    'date' => $maxprepsDate->format('Y-m-d'),
                ])->withTrashed()->first();

                // If we find it here, add the contest ID
                if ($game !== null) {
                    $game->contestid = $contestId;
                    $game->save();
                }
            }

            if ($game === null) {
                // Maybe swapped?
                $game = Game::where([
                    'awayteam' => $hometeam->teamid,
                    'hometeam' => $awayteam->teamid,
                    'date' => $maxprepsDate->format('Y-m-d'),
                ])->withTrashed()->first();

                if ($game !== null) {
                    // If we find it now, then the hometeam and awayteam we have in the DB is opposite of MaxPreps
                    if ($hometeam === $team) {
                        $awayteam =& $team;
                        $hometeam =& $opponent;
                    } else {
                        $hometeam =& $team;
                        $awayteam =& $opponent;
                    }

                    // If we find it here, add the contest ID
                    $game->contestid = $contestId;
                    $game->save();
                }
            }

            if ($game === null) {
                echo '<span class="label label-warning"> Game not found.</span></div>';
                echo ' New game ';
                $game = new Game;
            }

            if ($game->human) {
                echo '<span class="label label-info">This is a human edited game. Human Protection currently on.</span></div>';
                $games[] = $game->gameid;
                continue;
            }

            if ($game->isDeleted()) {
                // This needs to happen after the human check
                echo '<div class="label label-info">Game was deleted ... restoring ...</div>';
                $game->restore();
            }

            $game->playoff = false;
            if ($tr->branch()->is('a.contest-type-playoff')) {
                $game->playoff = true;
            }

            $game->confid = false;
            if ($tr->branch()->is('a.contest-type-conference')) {
                $game->confid = true;
            }

            $game->hometeam = $hometeam->teamid;
            $game->awayteam = $awayteam->teamid;

            $game->contestid = $contestId;
            $game->date = $maxprepsDate->format('Y-m-d');

            // Stadium
            $stadiumText = $tr->branch()->find('.content-location')->text();
            $stadiumText = trim(str_replace(['Game Details: ', 'High School', 'HS', 'Location: '], '', trim($stadiumText)));
            if (strlen($stadiumText) > 0) {
                $stadium = Stadium::where('name', 'like', "%$stadiumText%")->first();
                if ($stadium !== null) {
                    $game->stadiumid = $stadium->stadiumid;
                }
            }

            // Figure out if on +/- 1 day, move game instead
            $dateTomorrow = date('Y-m-d', strtotime("$date + 1 day"));
            $dateYesterday = date('Y-m-d', strtotime("$date - 1 day"));
            $gameDateRangeSearch = Game::whereBetween('date', [$dateYesterday, $dateTomorrow])
                ->where(['hometeam' => $hometeam])
                ->where('awayteam', $awayteam)
                ->get();
            if (!$gameDateRangeSearch->isEmpty()) {
                // Move the game to scheduled date
                $game->date = $gameDateRangeSearch->date;
                echo ' <span class="label label-info">Game moved 1 day forward/backward.</span>';
            }

            $game->save();

            $games[] = $game->gameid;

            $result = $this->call('Process:Game', [
                'gameobj' => $game,
                'tabletr' => $tr,
                'team' => $team,
                'opp' => $opponent,
            ]);

            if ($result) {
                // Recalculate RPI for this home and away, opponents, and opponents opponents
                // By just iterating through the home_ and away_ season's, we'll cover all the above teams
                $homeSeason = $game->homeSeason($year);
                $awaySeason = $game->awaySeason($year);

                foreach ($homeSeason->schedule() as $seasonGame) {
                    self::dispatchRPIforGame($seasonGame);
                }
                foreach ($awaySeason->schedule() as $seasonGame) {
                    self::dispatchRPIforGame($seasonGame);
                }
            }
            echo '</div>';
            unset ($maxprepsOpponent, $date, $away, $awayteam, $hometeam, $stadiumid);
        }

        echo '<p>Checking if I want to delete games...</p>';
        if (count($games) > 0) {
            // We just created or have a game for this team in this year. They should be in the season.
            $season = Season::firstOrCreate(['teamid' => $team->teamid, 'year' => $year->year]);
            if ($preventDeletion) {
                echo '<p>I have been prevented from deleting games for this run.</p>';
            } else {
                /** @var Season $season */
                $gamesToDelete = array_diff($season->getSchedule()->pluck('gameid')->toArray(), $games);

                echo '<ul>';
                foreach ($gamesToDelete as $key => $gameID) {
                    $gameToDelete = Game::findOrFail($gameID);
                    if ($gameToDelete->human) {
                        $gameToDelete->want_to_delete = true;
                        $gameToDelete->save();
                        echo "I wanted to delete this game: $gameID";
                        continue;
                    }
                    $gameToDelete->delete();

                    echo "<li>Deleted $gameID.</li>";
                }
            }
            echo '</ul>';
        } else {
            echo '<p>Nothing to delete.</p>';
        }

        echo $team->name . ' all done.</div></div>';

        return count($games);
    }

    private function dispatchRPIforGame(Game $game)
    {
        $calcRPI = new CalcRPIforTeam($game->hometeam, $game->year()->year);
        $calcRPI->onQueue('rpi');
        dispatch($calcRPI)->delay(Carbon::now()->addSeconds(180));

        $calcRPI = new CalcRPIforTeam($game->awayteam, $game->year()->year);
        $calcRPI->onQueue('rpi');
        dispatch($calcRPI)->delay(Carbon::now()->addSeconds(180));
    }
}
