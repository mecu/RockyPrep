<?php declare(strict_types=1);
namespace App\Console\Commands;

use App\Game;
use App\Season;
use App\Team;
use App\Year;
use DB;
use Illuminate\Console\Command;

/**
 * Class CalcSRS
 * @package App\Console\Commands
 */
class CalcSRS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Calc:SRS
                            {season : The season (year) to calculate}
                            {date? : The Date to cutoff data (for seeing SRS at a certain date) YYYY-MM-DD}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the SRS for the provided season (and date)';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        $year = new Year((int)$this->argument('season'));

        $date = $this->argument('date');

        if ($date) {
            $year->end = $date;
        }
        if ($year->end > date('Y-m-d')) {
            $year->end = date('Y-m-d');
        }

        // Get list of ALL games that are scheduled this season by end date that have a winner
        $games = Game::whereNotNull('winner')
            ->whereBetween('date', [$year->start, $year->end])
            ->get();

        $this->info('<span class="label label-info">SRS Calculation for:' . $year->year . '</span>');

        $srsTeamIds = [];

        // Loop through the results to make unique values
        foreach ($games as $game) {
            $srsTeamIds[] = $game->hometeam;
            $srsTeamIds[] = $game->awayteam;
        }
        $srsTeamIds = array_unique($srsTeamIds);
        unset($games);

        ob_start();

        $this->info(PHP_EOL . '<html><head><script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link type="text/css" media="all" rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"></head><body>');

        if (count($srsTeamIds) === 0) {
            $this->info('<span class="label label-danger">No data yet...</span>' . PHP_EOL);
        } else {
            /** @var Season[] $srsTeams */
            $srsTeams = Season::whereIn('teamid', $srsTeamIds)->ofYear($year)->get();

            $bar = $this->output->createProgressBar(count($srsTeams));
            $srs = [];

            // Have to run through an initial setting of the average point differential (avgpd)
            foreach ($srsTeams as $teamSeason) {
                $teamSeason->srs_numgames = $teamSeason->getSchedule()->count();
                $teamSeason->calcMOV();
                $teamSeason->save();

                $srs[$teamSeason->teamid]['apd'] = $teamSeason->srs_avgpd;
                $srs[$teamSeason->teamid]['numg'] = $teamSeason->srs_numgames;
                if ($srs[$teamSeason->teamid]['numg'] === 0) {
                    $srs[$teamSeason->teamid]['srs'] = 0.0;
                } else {
                    /** @var Team $game */
                    foreach ($teamSeason->getOpponents() as $team) {
                        $srs[$teamSeason->teamid]['opps'][] = $team->teamid;
                    }
                    $srs[$teamSeason->teamid]['srs'] = $srs[$teamSeason->teamid]['apd'];
                }

                $bar->advance();
            }
            unset($srsTeams, $srsTeamIds);

            $this->info(PHP_EOL . 'Average Point Differential calculation complete.');

            $averageChange = 1;
            $iteratorCount = 0;
            $count = count($srs);
            $maxIterations = 160;

            while ($count > 0 && abs($averageChange) > 0.01 && $iteratorCount < $maxIterations) {
                ++$iteratorCount;
                $this->info(PHP_EOL . "Cycle $iteratorCount of MAX($maxIterations): AvgChange: $averageChange");
                $averageChange = 0;

                $bar = $this->output->createProgressBar($count);

                /** @var array $srsValues */
                foreach ($srs as $teamID => $srsValues) {
                    if (count($srsValues['opps']) === 0) {
                        continue;
                    }

                    $points = 0;
                    $srs[$teamID]['oldsrs'] = $srs[$teamID]['srs'];

                    foreach ($srsValues['opps'] as $oppId) {
                        if (!array_key_exists($oppId, $srs)) {
                            $srs[$oppId] = [
                                'srs'  => 0,
                                'numg' => 0,
                                'apd'  => 0,
                                'aopd' => 0,
                                'opps' => [],
                            ];
                        }
                        $points += $srs[$oppId]['srs'];
                    }
                    $srs[$teamID]['aopd'] = round($points / count($srsValues['opps']), 1);
                    $srs[$teamID]['srs'] = round($srs[$teamID]['apd'] + $srs[$teamID]['aopd'], 1);
                    $averageChange += $srs[$teamID]['srs'] - $srs[$teamID]['oldsrs'];

                    $bar->advance();
                }
                $averageChange /= $count;
            }

            $this->info(PHP_EOL . 'Done! Saving ... ');

            $updated = 0;
            foreach ($srs as $teamID => $srsValues) {
                if ($srsValues['numg'] === 0) {
                    continue;
                }
                $params = [
                    'srs'          => $srsValues['srs'],
                    'srs_avgpd'    => $srsValues['apd'],
                    'srs_oppavg'   => $srsValues['aopd'],
                    'srs_numgames' => $srsValues['numg'],
                ];
                if (DB::table('season')->where('year', $year->year)->where('teamid', $teamID)->update($params)) {
                    ++$updated;
                }
            }

            $this->info(PHP_EOL . "Final average change: $averageChange. Teams updated: $updated.");
            $this->info("Completed SRS runs: Took $iteratorCount iterations");
            $this->info('Max memory: ' . memory_get_peak_usage(true));
            $this->info('<span class="label label-success">Successfully calculated SRS.</span>');
        }

        $file = fopen(storage_path() . '/reports/SRS-calc.html', 'w');
        fwrite($file, ob_get_flush());
        fclose($file);
    }
}
