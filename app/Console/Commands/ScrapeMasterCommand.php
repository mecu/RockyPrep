<?php
namespace App\Console\Commands;

use App\State;
use Illuminate\Console\Command;

class ScrapeMasterCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Scrape:Master
	                        {season : Year to scrape}
	                        {--offset= : If should skip some states}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Master Scrape controller.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');

        $offset = $this->option('offset') !== null ?? 0;

        $states = State::all();

        foreach ($states as $key => $state) {
            if ($key < $offset) {
                continue;
            }

            $this->call('Scrape:State', [
                'state'      => $state->stateid,
                'season'     => $this->argument('season'),
                '--existing' => false,
            ]);
        }

        return true;
    }
}
