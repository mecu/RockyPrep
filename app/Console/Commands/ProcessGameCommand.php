<?php

namespace App\Console\Commands;

use App\Game;
use App\Jobs\TweetGame;
use App\Team;
use Carbon\Carbon;
use Illuminate\Console\Command;
use QueryPath\DOMQuery;

class ProcessGameCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Process:Game
	                        {gameobj : The game object}
	                        {tabletr : The table row (TR) to process, in a QueryPath object}
	                        {team : The team object for the page}
	                        {opp : The opponent object for this row}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes a single game from a schedule page.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* @var Game $game */
        $game = $this->argument('gameobj');
        /** @var DOMQuery $tr */
        $tr = $this->argument('tabletr');

        /* @var Team $team */
        $team = $this->argument('team');
        /* @var Team $opp */
        $opp = $this->argument('opp');

        // Already have winner?, validate or just return?

        // See if there's a box score option implying a score result
        $gameResult = $tr->branch()->find('td.result a.score')->text();

        if (strpos($gameResult, '(W)') !== false ||
            strpos($gameResult, '(L)') !== false ||
            strpos($gameResult, '(T)') !== false ||
            strpos($gameResult, '(FFL)') !== false ||
            strpos($gameResult, '(FFW)') !== false
        ) {
            $gameScore = str_replace(' ', '', $gameResult);

            // Strip everything from Recap aferwards
            $recapPosition = strpos($gameScore, 'Recap');
            if ($recapPosition !== false) {
                $gameScore = trim(substr($gameScore, $recapPosition));
            }

            $gameResult = $gameScore;
            $gameScore = str_replace(['(W)', '(L)', '(T)', '(FFW)', '(FFL)'], '', $gameScore);

            if (strpos($gameResult, '(FFW)') === false && strpos($gameResult, '(FFL)') === false) {
                list($highScore, $lowScore) = explode('-', $gameScore);
                $highScore = (int)$highScore;
                $lowScore = (int)$lowScore;

                if (strpos($gameResult, '(L)') !== false) {
                    $game->setWinner($opp->teamid);
                    if ($team->teamid === $game->away->teamid) {
                        $game->homescore = $highScore;
                        $game->awayscore = $lowScore;
                    } else {
                        $game->homescore = $lowScore;
                        $game->awayscore = $highScore;
                    }
                } elseif (strpos($gameResult, '(W)') !== false) {
                    $game->setWinner($team->teamid);
                    if ($team->teamid === $game->home->teamid) {
                        $game->homescore = $highScore;
                        $game->awayscore = $lowScore;
                    } else {
                        $game->homescore = $lowScore;
                        $game->awayscore = $highScore;
                    }
                } elseif (strpos($gameResult, '(T)') !== false) {
                    $game->setWinner(0);
                    $game->homescore = $highScore;
                    $game->awayscore = $lowScore;
                } else {
                    echo(' <span class=\"label label-danger\">found game: not a win, loss or tie...</span>');
                    if ($highScore === 0) {
                        return false;
                    }
                }
            } else {
                $game->forfeit = true;

                if (strpos($gameResult, '(FFL)') !== false) {
                    $game->winner = $opp->teamid;
                } elseif (strpos($gameResult, '(FFW)') !== false) {
                    $game->winner = $team->teamid;
                }

                // Make sure the home/away score is correct for a bug I can't find in the commented out code above
                if ($game->winner === $game->home->teamid) {
                    $game->homescore = 2;
                    $game->awayscore = 0;
                } elseif ($game->winner === $game->away->teamid) {
                    $game->homescore = 0;
                    $game->awayscore = 2;
                }
            }

            echo " <span class='label label-success'>found game: a $gameResult </span>";

            if ($game->isDirty()) {
                $game->save();
                echo ' <span class="label label-success">Game Updated successfully.</span>';

                if ($game->home->state !== null) {
                    $tweet = new TweetGame($game, $game->home->state);
                    $tweet->onQueue('tweet');
                    dispatch($tweet)->delay(Carbon::now()->addSeconds(180));
                    if ($game->away->state !== null && $game->home->state->stateid != $game->away->state->stateid) {
                        $tweet = new TweetGame($game, $game->away->state);
                        $tweet->onQueue('tweet');
                        dispatch($tweet)->delay(Carbon::now()->addSeconds(180));
                    }
                }
            } else {
                echo ' <span class="label label-default">No changes.</span>';
            }
        } else {
            echo ' <span class="label label-default">found game: no scores ...</span>';

            return false;
        }

        return true;
    }
}
