<?php
namespace App\Console\Commands;

use App\Game;
use App\Season;
use App\Team;
use Illuminate\Console\Command;
use App\Year;

// http://www.collegerpi.com/rpifaq.html
// http://chsaanow.com/rpi/faqs/
class CalcCRPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Calc:CRPI
                            {season : The season (year) to calculate} 
                            {--team= : Calculate for just the team provided, will not execute if state is declared}
                            {--debug=false : Whether to show debug information}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the CHSAA RPI for the provided season';

    /**
     * Execute the console command
     */
    public function handle()
    {
        $debug = $this->option('debug') === 'true';
        if ($debug) {
            $this->info('Debug mode enabled!' . PHP_EOL);
        }

        $year = new Year($this->argument('season'));

        if ($year->end > date('Y-m-d')) {
            $year->end = date('Y-m-d');
        }

        if ($this->option('team')) {
            if ($debug) {
                $this->info('Only Single Team will be processed' . PHP_EOL);
            }
            // Only do the specific team provided
            /** @var Season $season */
            $season = Season::ofYear($year)
                ->where('teamid', $this->option('team'))
                ->first();

            $games = $season->getSchedule();
        } else {
            // Get list of ALL games that are scheduled this season by end date that have a winner
            if ($debug) {
                $this->info('Every Colorado team will be processed.' . PHP_EOL);
            }

            $teamsInState = Team::select('teamid')->where(['stateid' => 6])->get();
            $games = Game::whereNotNull('winner')
                ->whereBetween('date', [$year->start, $year->end])
                ->whereIn('awayteam', $teamsInState)
                ->orWhereIn('hometeam', $teamsInState)
                ->whereNotNull('winner')
                ->whereBetween('date', [$year->start, $year->end])
                ->get();
        }

        $rpiTeamIds = [];
        $count = 0;

        // Loop through the results to make unique values
        foreach ($games as $key => $game) {
            ++$count;
            $rpiTeamIds[] = $game->hometeam;
            $rpiTeamIds[] = $game->awayteam;
        }
        $rpiTeamIds = array_unique($rpiTeamIds);
        unset($games);

        ob_start();

        if ($debug) {
            echo('<head><title>CRPI Calc Results</title></head><body><span class="label label-info">CRPI Calculation for: ' . $year->year . '</span>');
            echo '
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
                ';
        } else {
            $this->info(PHP_EOL . 'CRPI Calculation for ' . $year->year . PHP_EOL);
        }

        if (count($rpiTeamIds) === 0) {
            if ($debug) {
                echo('<span class="label label-danger">No data yet...</span>' . PHP_EOL);
            } else {
                $this->info('No data yet...' . PHP_EOL);
            }
        } else {
            foreach ($rpiTeamIds as $key => $teamId) {
                $team = Team::find($teamId);

                if ($debug) {
                    echo '<div class="card"><div class="card-body">';
                    echo("<div class='card-title'>Team: {$team->name}</div><table class='table'>");
                }

                /** @var Season $teamSeason */
                $teamSeason = Season::ofTeam($team)->ofYear($year)->firstOrFail();

                $teamSeason->crpi_wp = $teamSeason->cwp($debug);
                $teamSeason->crpi_owp = $teamSeason->cowp($debug);
                $teamSeason->crpi_oowp = $teamSeason->coowp($debug);
                $teamSeason->crpi = round($teamSeason->crpi_wp * 0.3 + $teamSeason->crpi_owp * 0.4 + $teamSeason->crpi_oowp * 0.3, 4);

                if ($debug) {
                    $this->info('Team: ' . $team->name);
                    $this->info(print_r($teamSeason->getCRPI(), true));
                    $this->info('Saved: ' . $teamSeason->save());
                    if ($teamSeason->save()) {
                        echo("</table><div class='card-footer'><span class='label label-success'>Saved</span></div>");
                    } else {
                        echo("</table><div class='card-footer'><span class='label label-danger'>Not saved</span></div>");
                    }
                    echo '</div></div>';
                } else {
                    $teamSeason->save();
                }
            }
        }
        if ($debug) {
            echo '<span class="label label-success">Successfully calculated CRPI.</span>' . PHP_EOL;
        } else {
            $this->info('Successfully calculated CRPI.' . PHP_EOL);
        }

        // 1) Generate 1 array where each team's WP is calculated and the number of games is recorded and their opponents.
        // 2) Calculate OWP by lookups in array of opponents and the OWP formula you had
        // 3) Calculate OOWP by array lookups and summing total WP's and games and then just divide.

        $file = fopen(storage_path() . '/reports/CRPI-calc.html', 'w');
        fwrite($file, ob_get_flush());
        fclose($file);
    }
}
