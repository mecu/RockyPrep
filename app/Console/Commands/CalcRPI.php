<?php namespace App\Console\Commands;

use App\Game;
use App\Season;
use App\Team;
use App\Year;
use Illuminate\Console\Command;
use Illuminate\Contracts\Queue\ShouldQueue;

/*
 * @link http://www.collegerpi.com/rpifaq.html
 */
class CalcRPI extends Command implements ShouldQueue
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Calc:RPI
                            {season : The season (year) to calculate} 
                            {date? : The Date to cutoff data (for seeing RPI at a certain date) YYYY-MM-DD}
                            {--state= : Calculate for just the state ID provided}
                            {--team= : Calculate for just the team provided, will not execute if state is declared}
                            {--debug=false : Whether to show debug information}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the RPI for the provided season and sport (and date)';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        $debug = $this->option('debug') === 'true';
        if ($debug) {
            $this->info('Debug mode enabled!' . PHP_EOL);
        }

        $year = new Year($this->argument('season'));
        $date = $this->argument('date');

        if ($year->end > date('Y-m-d')) {
            $year->end = date('Y-m-d');
        }
        if ($date) {
            $year->end = $date;
        }

        // Get list of ALL games that are scheduled this season by end date that have a winner
        if ($this->option('state')) {
            // Only include teams in the State provided
            if ($debug) {
                $this->info('Only State will be processed' . PHP_EOL);
            }
            $teams = Team::where('stateid', $this->option('state'))->get();

            $away = Game::whereIn('awayteam', $teams)
                ->whereBetween('date', [$year->start, $year->end])
                ->orderBy('date')
                ->get();

            $home = Game::whereIn('hometeam', $teams)
                ->whereBetween('date', [$year->start, $year->end])
                ->orderBy('date')
                ->get();

            $games = $away->merge($home)->sortBy('date');
        } elseif ($this->option('team')) {
            if ($debug) {
                $this->info('Only Single Team will be processed' . PHP_EOL);
            }
            // Only do the specific team provided
            /** @var Season $season */
            $season = Season::ofYear($year)
                ->where('teamid', $this->option('team'))
                ->first();

            $games = $season->getSchedule();
        } else {
            // Get list of ALL games that are scheduled this season by end date that have a winner
            if ($debug) {
                $this->info('Everything will be processed.' . PHP_EOL);
            }
            $games = Game::whereNotNull('winner')
                ->whereBetween('date', [$year->start, $year->end])
                ->get();
        }

        $rpiTeamIds = [];

        // Loop through the results to make unique values
        foreach ($games as $key => $game) {
            $rpiTeamIds[] = $game->hometeam;
            $rpiTeamIds[] = $game->awayteam;
        }
        unset($games);
        $rpiTeamIds = array_unique($rpiTeamIds);

        ob_start();

        $this->info(PHP_EOL . '<span class="label label-info">RPI Calculation for:' . $year->year . '</span>' . PHP_EOL);

        if (count($rpiTeamIds) === 0) {
            $this->info('<span class="label label-danger">No data yet...</span>' . PHP_EOL);
        } else {
            foreach ($rpiTeamIds as $key => $teamId) {
                $team = Team::find($teamId);

                /** @var Season $teamSeason */
                $teamSeason = Season::ofTeam($team)->ofYear($year)->firstOrFail();

                $teamSeason->rpi_wp = $teamSeason->wp(true, $debug);
                $teamSeason->rpi_owp = $teamSeason->owp($debug);
                $teamSeason->rpi_oowp = $teamSeason->oowp($debug);
                $teamSeason->rpi = round($teamSeason->rpi_wp / 4 + $teamSeason->rpi_owp / 2 + $teamSeason->rpi_oowp / 4, 4);

                $teamSeason->save();

                if ($debug) {
                    $this->info('Team: ' . $team->name);
                    $this->info(dump($teamSeason->getRPI()));
                    break;
                }
            }
        }
        $this->info('<span class="label label-success">Successfully calculated RPI.</span>');

        //1) Generate 1 array where each team's WP is calculated and the number of games is recorded and their opponents.
        //2) Calculate OWP by lookups in array of opponents and the OWP formula you had
        //3) Calculate OOWP by array lookups and summing total WP's and games and then just divide.

        $file = fopen(storage_path() . '/reports/RPI-calc.html', 'w');
        fwrite($file, ob_get_flush());
        fclose($file);
    }
}
