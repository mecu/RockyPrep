<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Helpers\Scraper;
use App\Helpers\URLName;
use App\Level;
use App\Season;
use App\State;
use App\Team;
use App\Year;
use Illuminate\Console\Command;

class ScrapeClassCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'Scrape:Class
							{state : State ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape the classifications for a state.';

    /**
     * Execute the console command
     */
    public function handle()
    {
        $state = State::find($this->argument('state'));
        $this->info('Working on ' . $state->name);

        $year = new Year();

        $urlName = str_replace('_', '-', $state->urlname);
        if ($state->stateid === 48) {
            $urlName = 'washington,-dc';
        }

        $page = Scraper::fetch("http://www.maxpreps.com/state/football/$urlName.htm");

        $statePage = \QueryPath::withHTML5($page, '#navigation');
        $classes = $statePage->branch()->find('a[data-la="division"]');

        foreach ($classes as $class) {
            $levelText = $class->branch()->text();
            $levelName = trim(str_replace(['Class', 'League', 'Division', 'Region'], '', $levelText));
            $level = Level::firstOrNew([
                'name' => $levelName,
                'stateid' => $state->stateid
            ]);

            if (!$level->exists) {
                $level->urlname = URLName::provideString($level->name);
                $level->save();
                $this->info('I created ' . $level->name);
            }

            $this->info('Working on ' . $level->name);

            $classUrl = $class->attr('href');

            $classPage = \QueryPath::withHTML5("http://www.maxpreps.com$classUrl", '#standings');
            if (count($classPage) === 0) {
                $this->info('I could not retrieve ' . $level->name . ' page.');
                continue;
            }

            foreach ($classPage->branch()->find('#standings tbody tr') as $tr) {
                $dataHoverCard = $tr->find('th.school a')->attr('href');

                $this->info('Trying to find ' . $dataHoverCard);

                $matches = preg_match('/schoolid=([[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12})/', $dataHoverCard, $MaxPrepsID);
                if ($matches === 1) {
                    $team = Team::where('maxpreps_id', $MaxPrepsID[1])->first();

                    if ($team === null) {
                        $this->error('I could not find team: ' . $MaxPrepsID[1]);
                        continue;
                    }
                } else {
                    // Try to match based on URL name
                    $matches = preg_match('/\/high-schools\/(.*)\/football.*\/home.htm/', $dataHoverCard, $MaxPrepsURLName);
                    if ($matches === 1) {
                        $team = Team::where('maxpreps_name', $MaxPrepsURLName[1])->first();

                        if ($team === null) {
                            $this->error('I could not find team: ' . $MaxPrepsURLName[1]);
                            continue;
                        }
                    }
                }

                $season = Season::firstOrCreate([
                    'teamid' => $team->teamid,
                    'year' => $year,
                ]);

                if ($season->exists &&
                    ($season->levelid_state != 0 || $season->levelid_state !== null) &&
                    $season->levelid_state !== $level->levelid
                ) {
                    $this->error($team->name . ' already has a different level. ' . $team->teamid . '|' . $season->levelid_state . '|' . $level->levelid);
                }

                $season->levelid_state = $level->levelid;
                $season->save();
            }
        }

        $this->info('Done.');
    }
}
