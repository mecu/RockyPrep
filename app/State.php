<?php namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class State
 */
class State extends Eloquent
{
    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'stateid';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'state';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public function teams(): HasMany
    {
        return $this->hasMany('App\Team', 'stateid', 'stateid')->orderBy('name');
    }

    public function seasons(): HasMany
    {
        return $this->hasMany('App\Season');
    }

    public function levels(): HasMany
    {
        return $this->hasMany('App\Level', 'stateid', 'stateid');
    }
}
