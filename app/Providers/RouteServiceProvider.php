<?php declare(strict_types=1);
namespace App\Providers;

use App\Conference;
use App\Game;
use App\Level;
use App\Year;
use App\Coach;
use App\State;
use App\Team;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::pattern('id', '[0-9]+');
        Route::pattern('state', '[a-z\-_]+');
        Route::pattern('conference', '[a-z\-_0-9()]+');
        Route::pattern('team', '[A-Za-z\-_.0-9]+');
        Route::pattern('year', '[0-9]{4}+');
        Route::pattern('month', '[0-9]{1,2}+');
        Route::pattern('day', '[0-9]{1,2}+');


        Route::bind('state', function($URLName)
        {
            return State::where(['urlname' => $URLName])->firstOrFail();
        });

        Route::bind('game', function($id)
        {
            return Game::findOrFail($id);
        });
        
        Route::bind('team', function($URLName)
        {
            return Team::where(['urlname' => $URLName])->with('state')->firstOrFail();
        });
        
        Route::bind('coach', function($URLName)
        {
            return Coach::where(['name' => str_replace( '_' , ' ', $URLName )])->firstOrFail();
        });
        
        Route::bind('year', function($year)
        {
            return new Year((int)$year);
        });
        
        Route::bind('conference', function($URLName)
        {
            return Conference::where(['urlname' => $URLName])->firstOrFail();
        });
        
        Route::bind('level', function($urlname)
        {
            $state = $this->app->request->route('state');

            return Level::where([
                'urlname' => $urlname,
                'stateid' => $state->stateid,
            ])->firstOrFail();
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        //
    }
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
