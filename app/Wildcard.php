<?php namespace App;

use Eloquent;

/**
 * Class Wildcard
 */
class Wildcard extends Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wc';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Give point values for wins and losses
     * @param Season $teamSeason
     * @param Season $opponentSeason
     * @param Game $game
     * @param Year $year
     * @param bool $bonus
     * @return int|mixed
     */
    public static function givepoints(Season $teamSeason, Season $opponentSeason, Game $game, Year $year, $bonus = false)
    {
        if ($opponentSeason->levelid === null) {
            return 0;
        }

        $opponentLevel = $opponentSeason->levelid;

        if ($bonus && $opponentSeason->levelid < $teamSeason->levelid) {
            switch ($opponentLevel) {
                case 6:
                    $opponentLevel = 7;
                    break;
                case 7:
                    $opponentLevel = 11;
                    break;
                case 11:
                    $opponentLevel = 22;
                    break;
                case 22:
                    $opponentLevel = 33;
                    break;
                case 33:
                    $opponentLevel = 44;
                    break;
                case 44:
                    $opponentLevel = 55;
                    break;
            }
        }

        switch ($game->status($teamSeason->team)) {
            case 'WIN':
                $win = 1;
                break;
            case 'LOSE':
                $win = 0;
                break;
            case 'TIE':
                throw new \RuntimeException('Cannot have ties for WildCard Points. Team:' . $teamSeason->team->name);
        }

        $points = Wildcard::select('points')->where([
            'year'     => (int)$year->year,
            'levelid'  => $teamSeason->levelid,
            'opplevel' => $opponentLevel,
            'jv'       => (int)$opponentSeason->team->isJV(),
            'win'      => $win,
            'oppwins'  => $opponentSeason->getWins($year),
        ])->first();

        return $points !== null ? $points->points : 0;
    }

    /**
     * Give points for tiebreak
     * @param Level|null $level
     * @return float|int
     */
    public static function givetiebreakpoints(Level $level = null)
    {
        if ($level === null) {
            return 0;
        }

        switch ((int)$level->levelid) {
            case 55:
                return 4;
                break;
            case 44:
                return 3.5;
                break;
            case 33:
                return 3;
                break;
            case 22:
                return 2.5;
                break;
            case 11:
                return 2;
                break;
            case 7:
            case 6:
                return 1.5;
                break;
            default:
                return 0;
        }
    }

    /**
     * @param Season $season
     * @param Year $year
     * @return float|int
     */
    public static function secondleveltiebreakpoints(Season $season, Year $year)
    {
        $points = 0;
        $games = $season->getSchedule();

        // Grab all games where Team was winner
        $teamWonGames = $games->filter(function (Game $game) use ($season) {
            return $game->hasWinner() && $game->isWinner($season->team);
        });

        $secondlowerbonus = true;

        /** @var Game $game */
        foreach ($teamWonGames as $key => $game) {
            $opponent = $game->opponent($season->team);
            $opponentSeason = Season::ofYear($season->thisYear())->ofTeam($opponent)->first();

            $points += self::givetiebreakpoints($opponentSeason->level);

            // See if they have used their 2nd lower bonus yet
            if ($secondlowerbonus && $opponentSeason->levelid < $season->levelid) {
                $secondlowerbonus = false;
                $points += 0.5;
            }
        }

        return $points;
    }
}
