<?php namespace App;

use Eloquent;
use Iatstuti\Database\Support\NullableFields;

/**
 * Class Stadium
 */
class Stadium extends Eloquent
{
	use NullableFields;
	/**
	 * The field used as the primary key
	 *
	 * @var int
	 */
	protected $primaryKey = 'stadiumid';

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'stadium';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /*
     * Sets that only the stadiumid cannot be mass assigned
     */
    protected $guarded = ['stadiumid'];

    protected $nullable = [
        'mascot',
        'location',
        'alt',
        'address',
        'zip',
        'lat',
        'lng',
    ];

	public function fullAddress($abbr)
	{
		return "{$this->name}, {$this->address}, {$this->city}, {$abbr}, {$this->zip}";
	}
	
	public function googleMap($abbr)
	{
		return 'http://maps.googleapis.com/maps/api/staticmap?center=' . $this->fullAddress($abbr) . '&zoom=15&size=180x180&maptype=hybrid&sensor=false&markers=' . $this->fullAddress($abbr);
	}
}
