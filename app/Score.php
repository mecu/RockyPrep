<?php namespace App;

use Eloquent;

/**
 * Class Score
 */
class Score extends Eloquent
{
    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'gameid';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scores';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
