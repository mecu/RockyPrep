<?php
namespace App;

use App\Jobs\TweetGame;
use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class Game
 */
class Game extends Eloquent
{
    use DispatchesJobs;
    use SoftDeletes;

    /**
     * The field used as the primary key
     *
     * @var int
     */
    protected $primaryKey = 'gameid';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'game';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'homescore'   => 'integer',
        'awayscore'   => 'integer',
        'hometeam'    => 'integer',
        'awayteam'    => 'integer',
        'neutral'     => 'boolean',
        'srs_predict' => 'integer',
        'srs_teamid'  => 'integer',
        'winner'      => 'integer',
        'forfeit'     => 'boolean',
        'tournament'  => 'boolean',
        'playoff'     => 'boolean',
        'human'       => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date',
    ];

    public function away()
    {
        return $this->hasOne('App\Team', 'teamid', 'awayteam');
    }

    public function home()
    {
        return $this->hasOne('App\Team', 'teamid', 'hometeam');
    }

    /**
     * See if the provided team is the away team
     *
     * @param Team $team
     *
     * @return boolean
     */
    public function isAway(Team $team): bool
    {
        if ($this->neutral) {
            return false;
        }

        return $this->away->teamid === $team->teamid;
    }

    /**
     * The opponent Team of the Team provided
     *
     * @param Team $team
     *
     * @return Team|null
     */
    public function opponent(Team $team)
    {
        if ($this->hometeam === $team->teamid) {
            return $this->away;
        } elseif ($this->awayteam === $team->teamid) {
            return $this->home;
        }

        return null;
    }

    /**
     * The opponent TeamSeason of the Team provided
     *
     * @param Team $team
     * @param Year $year
     *
     * @return Season|null
     */
    public function opponentSeason(Team $team, Year $year)
    {
        $opponent = $this->opponent($team);

        if ($opponent === null) {
            return null;
        }

        return Season::with('team')->firstOrCreate([
            'teamid' => $opponent->teamid,
            'year'   => $year->year,
        ]);
    }

    /**
     * The Season of the home team
     *
     * @param Year $year
     *
     * @return Season|null
     */
    public function homeSeason(Year $year)
    {
        return $this->hasMany('App\Season', 'teamid', 'hometeam')
            ->where('year', $year->year)
            ->first();
    }

    /**
     * The Season of the away team
     *
     * @param Year $year
     *
     * @return Season|null
     */
    public function awaySeason(Year $year)
    {
        return $this->hasMany('App\Season', 'teamid', 'awayteam')
            ->where('year', $year->year)
            ->first();
    }

    public function isDeleted(): bool
    {
        return $this->deleted_at !== null;
    }

    public function hasWinner(): bool
    {
        return $this->winner !== null;
    }

    public function isWinner(Team $team): bool
    {
        return $this->winner === $team->teamid;
    }

    public function hasPredict(): bool
    {
        return $this->srs_teamid !== null;
    }

    public function hasBoxscore(): bool
    {
        return $this->scores->count() > 0;
    }

    //This Function takes the provided teamid and compares to the winner to determine if that was the winning team and returns 'WIN' or 'LOSE'
    //As an example, teamid 14 beat teamid 13, if you call ->status(14) you would get 'WIN', but ->status(13) would get 'LOSE'
    //It will return TIE regardless of the teamid supplied, if it was a tie.
    /**
     * @param Team $team
     *
     * @return null|string
     */
    public function status(Team $team)
    {
        // If winner is null, no winner
        if ($this->winner === null) {
            return null;
        }

        if ($this->winner === 0 && $this->homescore === $this->awayscore) {
            return 'TIE';
        } elseif ($this->winner === $team->teamid) {
            return 'WIN';
        } elseif ($this->winner !== $team->teamid) {
            return 'LOSE';
        }

        return null;
    }

    //This function will provide the result for the team specified in the format: W, xx-xx or L, xx-xx or T, xx-xx
    /**
     * @param Team $team
     *
     * @return string
     */
    public function result(Team $team): string
    {
        if ($this->status($team) === 'LOSE') {
            if ($this->awayscore < $this->homescore) {
                return 'L, ' . $this->awayscore . '-' . $this->homescore;
            }

            return 'L, ' . $this->homescore . '-' . $this->awayscore;
        } elseif ($this->status($team) === 'WIN') {
            if ($this->awayscore > $this->homescore) {
                return 'W, ' . $this->awayscore . '-' . $this->homescore;
            }

            return 'W, ' . $this->homescore . '-' . $this->awayscore;
        } elseif ($this->status($team) === 'TIE') {
            return 'T, ' . $this->awayscore . '-' . $this->homescore;
        }

        return '';
    }

    /**
     * @return \App\Team|null
     */
    public function getWinnerObject()
    {
        if ($this->winner === null) {
            return null;
        }

        if ($this->winner === $this->hometeam) {
            return $this->home;
        } elseif ($this->winner === $this->awayteam) {
            return $this->away;
        }

        return null;
    }

    /**
     * @return \App\Team|null
     */
    public function getLoserObject()
    {
        if ($this->winner === null) {
            return null;
        }

        if ($this->winner === $this->hometeam) {
            return $this->away;
        } elseif ($this->winner === $this->awayteam) {
            return $this->home;
        }

        return null;
    }

    /**
     * @param int $winner
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
        $this->save();

        $today = new \DateTime();
        if ($today->sub(new \DateInterval('P4D')) < $this->date->format('Y-m-d')) {
            // Check if the team is in a State
            if (!is_a($this->home->state, 'State')) {
                return;
            }
            $job = (new TweetGame($this, $this->home->state))->onQueue('twitter')->delay(60);
            $this->dispatch($job);

            if (!is_a($this->away->state, 'State')) {
                return;
            }
            if ($this->home->state->id !== $this->away->state->id) {
                $job = (new TweetGame($this, $this->away->state))->onQueue('twitter')->delay(60);
                $this->dispatch($job);
            }
        }
    }

    public function stadium()
    {
        return $this->hasOne('App\Stadium', 'stadiumid', 'stadiumid');
    }

    /**
     * @return bool
     */
    public function isOT(): bool
    {
        return $this->scores->count() > 4;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scores()
    {
        return $this->hasMany('App\Score', 'gameid', 'gameid');
    }

    /**
     * @return string
     */
    public function numberOT(): string
    {
        if (!$this->isOT()) {
            return '';
        }

        $scores = $this->scores;
        $number = $scores->count() - 4;

        if ($number === 1) {
            return ' (OT)';
        }

        return " ({$number}OT)";
    }

    /**
     * Only sets the final home and away score and sets the winner based on the score
     *
     * @param array $homeScore
     * @param array $awayScore
     * @return bool
     */
    public function setScores(array $homeScore, array $awayScore): bool
    {
        if ($this->homescore !== end($homeScore) || $this->awayscore !== end($awayScore)) {
            $this->homescore = end($homeScore);
            $this->awayscore = end($awayScore);
        } else {
            return false;
        }

        if ($this->homescore > $this->awayscore) {
            $this->setWinner($this->hometeam);
        } elseif ($this->homescore < $this->awayscore) {
            $this->setWinner($this->awayteam);
        } elseif ($this->homescore === $this->awayscore) {
            $this->setWinner(0);
        }

        $this->save();

        return true;
    }

    /**
     * Update boxscores
     * @param array $homeScores
     * @param array $awayScores
     * @return bool
     */
    public function updateBoxScore(array $homeScores, array $awayScores): bool
    {
        $quarters = count($homeScores) - 1;
        if (count($awayScores) - 1 !== $quarters) {
            return false;
        }
        if ($quarters < 3) {
            return false;
        }

        for ($k = 0; $k < $quarters; ++$k) {
            $score = Score::firstOrNew([
                'gameid'  => $this->gameid,
                'quarter' => $k + 1,
            ]);
            $score->away = $awayScores[$k];
            $score->home = $homeScores[$k];
            $score->save();
        }

        return true;
    }

    /**
     * Validate scores, replace - with 0
     * @param array $awayScores
     * @param array $homeScores
     * @return array
     */
    public static function processScores(array $awayScores, array $homeScores)
    {
        foreach ($awayScores as $key => $value) {
            if ($value === '-') {
                $awayScores[$key] = 0;
            }
        }
        foreach ($homeScores as $key => $value) {
            if ($value === '-') {
                $homeScores[$key] = 0;
            }
        }

        return [$awayScores, $homeScores];
    }

    /**
     * Validate scores for quarters 1-4
     * @param array $awayScores
     * @param array $homeScores
     * @return bool
     */
    public static function validateScores(array $awayScores, array $homeScores): bool
    {
        return (array_sum($awayScores) - end($awayScores)) === end($awayScores) && (array_sum($homeScores) - end($homeScores)) === end($homeScores);
    }

    /**
     * Validate scores for just OT, if they are 0-0, with nothing after, remove
     * @param array $awayScores
     * @param array $homeScores
     * @return array
     */
    public static function validateOTScores(array $awayScores, array $homeScores): array
    {
        // See if the OT scores are all just 0
        if (count($awayScores) < 6) {
            return [$awayScores, $homeScores];
        }

        $allZero = true;

        $awayScoresCount = count($awayScores);

        for ($i = 4; $i < $awayScoresCount; ++$i) {
            if ($awayScores[$i] === 0 && $homeScores[$i] === 0) {
                continue;
            }
            $allZero = false;
        }

        if ($allZero) {
            for ($i = 4; $i < $awayScoresCount; ++$i) {
                $awayScores = array_splice($awayScores, $i, 1);
                $homeScores = array_splice($homeScores, $i, 1);
            }
        }

        return [$awayScores, $homeScores];
    }

    public function classificationSort(Year $year): string
    {
        $value = '';
        if ($this->awaySeason($year)->levelid) {
            $value .= $this->awaySeason($year)->level->levelid;
        }
        if ($this->homeSeason($year)->levelid) {
            $value .= $this->homeSeason($year)->level->levelid;
        }

        return "data-order=$value";
    }

    public function highlightWinner(string $team)
    {
        $awayScore = (int)$this->awayscore;
        $homeScore = (int)$this->homescore;

        if ($awayScore === 0 && $homeScore === 0) {
            return '';
        }
        if ($awayScore > $homeScore && $team === 'away') {
            return ' alert-success';
        }
        if ($awayScore < $homeScore && $team === 'away') {
            return ' alert-danger';
        }
        if ($awayScore > $homeScore && $team === 'home') {
            return ' alert-danger';
        }
        if ($awayScore < $homeScore && $team === 'home') {
            return ' alert-success';
        }

        return '';
    }

    public function year(): Year
    {
        return new Year($this->date->format('Y'));
    }
}
