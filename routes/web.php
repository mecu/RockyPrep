<?php 
namespace App;

use Route;

Route::view('/', 'thanks');

Route::fallback(function () {
    return view('thanks');
});
