<?php declare(strict_types=1);
namespace App;

use Route;

Route::get('v1/search', 'ApiController@getSearch')->name('apiSearch');
Route::post('v1/game', 'ApiController@postGame')->name('apiPostGame');
Route::delete('v1/game', 'ApiController@deleteGame')->name('apiDeleteGame');
Route::put('v1/game', 'ApiController@putGame')->name('apiPutGame');
Route::put('v1/season', 'ApiController@putSeason')->name('apiPutSeason');
Route::get('v1/stadium/search', 'ApiController@getStadiumSearch')->name('apiStadiumSearch');

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function () {
    Route::get('reporting/{state}', 'AdminAPIController@reportingGameList')->name('adminAPIReportList');
    Route::post('report/game/{game}', 'AdminAPIController@reportGame')->name('adminAPIReportGame');

    Route::put('teaminfo', 'AdminAPIController@teamInfo')->name('adminAPITeamInfo');
});
