<?php

use Faker\Generator as Faker;

$factory->define(App\Team::class, function (Faker $faker) {
    $faker->addProvider(new \App\Faker\CustomFakerProvider($faker));

    return [
        'name' => $faker->company,
        'urlname' => $faker->unique()->customName,
        'mascot' => $faker->catchPhrase,
        'city' => $faker->city,
        'stateid' => rand(1, 51),
        'jv' => 0,
        'color1' => substr($faker->hexcolor, 1),
        'color2' => substr($faker->hexcolor, 1),
        'address' => $faker->streetAddress,
        'zip' => $faker->postcode,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'twitter' => '', // TODO
        'maxpreps_name' => '', // TODO
        'maxpreps_id' => strtolower(str_replace(' ', '_', $faker->unique()->word)),
        'facebook_id' => '', // TODO
        'facebook' => '', // TODO
        'website' => $faker->url,
        'hudl' => '', // TODO
    ];
});
