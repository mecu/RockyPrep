<?php

use Faker\Generator as Faker;

$factory->define(App\Season::class, function (Faker $faker) {
    return [
        'levelid' => 55,
        'levelid_state' => 55,
        'enrollment' => rand(25, 2000),
        'rpi' => random_int(0, PHP_INT_MAX-1)/PHP_INT_MAX,
        'rpi_wp' => 0,
        'rpi_owp' => 0,
        'rpi_oowp' => 0,
        'srs' => 0,
        'srs_avgpd' => 0,
        'srs_oppavg' => 0,
        'srs_numgames' => 0,
    ];
});
