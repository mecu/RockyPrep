<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
		$this->call(ArticleTableSeeder::class);
		$this->call(StateTableSeeder::class);
		$this->call(LevelTableSeeder::class);
		$this->call(TeamTableSeeder::class);
		$this->call(GameTableSeeder::class);
    }
}
