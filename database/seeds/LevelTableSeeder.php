<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    private $levels = [
        ['levelid' => '6','name' => 'A-6 man','stateid' => '6','urlname' => '6man','image' => 'orange','order' => '1','private' => '0'],
        ['levelid' => '7','name' => 'A-8 man','stateid' => '6','urlname' => '8man','image' => 'yellow','order' => '2','private' => '0'],
        ['levelid' => '11','name' => '1A','stateid' => '6','urlname' => '1A','image' => 'purple','order' => '3','private' => '0'],
        ['levelid' => '22','name' => '2A','stateid' => '6','urlname' => '2A','image' => 'brown','order' => '4','private' => '0'],
        ['levelid' => '33','name' => '3A','stateid' => '6','urlname' => '3A','image' => 'green','order' => '5','private' => '0'],
        ['levelid' => '44','name' => '4A','stateid' => '6','urlname' => '4A','image' => 'blue','order' => '6','private' => '0'],
        ['levelid' => '55','name' => '5A','stateid' => '6','urlname' => '5A','image' => 'red','order' => '7','private' => '0']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('level')->delete();

        foreach ($this->levels as $level) {
            \App\Level::create([
                'levelid' => $level['levelid'],
                'name' => $level['name'],
                'stateid' => $level['stateid'],
                'urlname' => $level['urlname'],
                'image' => $level['image'],
                'order' => $level['order'],
                'private' => $level['private'],
            ]);
        }
    }
}
