<?php

use App\Team;
use App\Game;
use App\Year;
use Illuminate\Database\Seeder;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('game')->delete();
        $year = new Year();
        $teams = Team::all();

        // Now loop all the teams and make 10 games for each
        /** @var Team $team */
        foreach ($teams as $team) {
            // Get their season to get a list of current opponents
            $season = $team->seasons()
                ->where('year', $year->year)
                ->first();
            /** @var \Illuminate\Support\Collection $opponents */
            $opponents = $season->getOpponents();

            $alreadyScheduled = $opponents->count();

            if ($alreadyScheduled > 9) {
                continue;
            }

            // find up to 10 other teams
            $opponents = Team::where('teamid', '!=',  $team->teamid)
                ->whereNotIn('teamid', $opponents->pluck('teamid'))
                ->inRandomOrder()
                ->limit(10 - $alreadyScheduled)
                ->get();

            foreach ($opponents as $opponent) {
                $game = new Game();
                if (rand(0, 1)) {
                    $game->hometeam = $team->teamid;
                    $game->awayteam = $opponent->teamid;
                } else {
                    $game->hometeam = $opponent->teamid;
                    $game->awayteam = $team->teamid;
                }

                $game->date = (new Carbon())->addWeeks(rand(-10, 10));
                $game->confid = rand(0, 1);
                $game->playoff = rand(0, 10) > 8;
                $game->forfeit = rand(0, 100) > 89;
                $game->neutral = rand(0, 100) > 89;

                if (rand(0, 1)) {
                    $game->winner = $team->teamid;
                    if ($game->hometeam === $team->teamid) {
                        $game->homescore = rand(7, 50);
                        $game->awayscore = rand(0, $game->homescore - 1);
                    } else {
                        $game->awayscore = rand(7, 50);
                        $game->homescore = rand(0, $game->awayscore - 1);
                    }
                } else {
                    $game->winner = $opponent->teamid;
                    if ($game->hometeam === $team->teamid) {
                        $game->awayscore = rand(7, 50);
                        $game->homescore = rand(0, $game->awayscore - 1);
                    } else {
                        $game->homescore = rand(7, 50);
                        $game->awayscore = rand(0, $game->homescore - 1);
                    }
                }

                $game->save();

                // Sometimes generate box scores
                if (rand(0, 1)) {
                    $homeScore = $game->homescore;
                    $awayScore = $game->awayscore;

                    $homeScores = $awayScores = [];

                    while ($homeScore > 0 && $awayScore > 0) {
                        $homeScores[] = $homeScore -= rand(0, $homeScore);
                        $awayScores[] = $awayScore -= rand(0, $awayScore);
                    }
                    $game->updateBoxScore($homeScores, $awayScores);
                }
                unset($homeScores, $awayScores);
            }
        }

        // Now duplicate all the games, but subtract a year
        /*
         * SQL CREATE TEMPORARY TABLE temporary_table AS SELECT * FROM original_table WHERE <conditions>;

                UPDATE temporary_table SET <auto_inc_field>=NULL, <fieldx>=<valuex>, <fieldy>=<valuey>, ...;

                INSERT INTO original_table SELECT * FROM temporary_table;
         */
        \DB::statement('CREATE TEMPORARY TABLE temporary_game AS SELECT * FROM game;
          ALTER TABLE temporary_game MODIFY gameid INT;
          UPDATE temporary_game SET gameid=NULL, `date` = DATE_SUB(`date`, INTERVAL 1 YEAR);
          INSERT INTO game SELECT * FROM temporary_game;
          DROP TABLE temporary_game;
          ');
    }
}
