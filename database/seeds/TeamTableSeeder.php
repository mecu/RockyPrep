<?php

use App\Season;
use App\State;
use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('team')->delete();

        $states = State::limit(10)->get();
        $year = new \App\Year();
        $lastYear = new \App\Year($year->year - 1);

        $faker = Faker\Factory::create();

        foreach ($states as $state) {
            $faker->unique(true)->jobTitle;
            $faker->unique(true)->word;

            $teams = factory(App\Team::class, 10)->create([
                'stateid' => $state->stateid,
            ]);

            /** @var Season $season */
            foreach ($teams as $team) {
                factory(Season::class)->create([
                    'year' => $year->year,
                    'teamid' => $team->teamid,
                ]);
                factory(Season::class)->create([
                    'year' => $lastYear->year,
                    'teamid' => $team->teamid,
                ]);
            }
        }
    }
}
