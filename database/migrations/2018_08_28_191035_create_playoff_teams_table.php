<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayoffTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playoff_teams', function(Blueprint $table)
		{
			$table->year('year')->index();
			$table->smallInteger('levelid')->index();
			$table->smallInteger('teams');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playoff_teams');
	}

}
