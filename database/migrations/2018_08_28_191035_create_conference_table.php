<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConferenceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conference', function(Blueprint $table)
		{
			$table->smallInteger('confid', true)->unsigned();
			$table->string('name', 32);
			$table->string('urlname', 48)->unique('urlname');
			$table->boolean('levelid');
			$table->enum('divisions', ['0','1'])->default('0');
			$table->integer('stateid')->default(6)->index('stateid');
			$table->unique(['urlname','stateid']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conference');
	}

}
