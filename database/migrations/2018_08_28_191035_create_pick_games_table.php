<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePickGamesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pick_games', function(Blueprint $table)
		{
			$table->integer('gameid')->primary();
			$table->year('year')->index('sportid_2');
			$table->boolean('week');
			$table->date('cutoff');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pick_games');
	}

}
