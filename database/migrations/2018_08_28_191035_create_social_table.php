<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSocialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('social', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('stateid')->index('onesportperstate');
			$table->integer('levelid')->nullable()->index();
			$table->string('accesstoken', 56);
			$table->string('accesstokensecret', 56);
			$table->boolean('twitter_active')->default(1);
			$table->bigInteger('facebookid')->unsigned()->default(0);
			$table->string('pagename', 64)->default('');
			$table->text('facebook_access_token', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social');
	}

}
