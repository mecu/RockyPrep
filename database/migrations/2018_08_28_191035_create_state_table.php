<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('state', function(Blueprint $table)
		{
			$table->boolean('stateid')->primary();
			$table->string('name', 14);
			$table->string('urlname', 14);
			$table->string('abbr', 2);
			$table->string('timezone', 28);
			$table->boolean('football')->default(1);
			$table->boolean('boys-basketball')->default(1);
			$table->boolean('boys_basketball')->default(2);
			$table->boolean('baseball')->default(1);
			$table->boolean('girls-basketball')->default(1);
			$table->boolean('girls_basketball')->default(4);
			$table->boolean('ice-hockey')->default(0);
			$table->boolean('boys-soccer')->default(0);
			$table->boolean('girls-soccer')->default(0);
			$table->boolean('boys-lax')->default(0);
			$table->boolean('girls-lax')->default(0);
			$table->boolean('field-hockey')->default(0);
			$table->boolean('softball')->default(0);
			$table->boolean('girls-volleyball')->default(0);
			$table->boolean('boys-volleyball')->default(0);
			$table->decimal('lat', 10, 6);
			$table->decimal('lng', 10, 6);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('state');
	}

}
