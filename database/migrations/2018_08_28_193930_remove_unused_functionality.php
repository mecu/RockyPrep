<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnusedFunctionality extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('articles', function(Blueprint $table)
        {
            $table->dropForeign('articles_language_id_foreign');

            $table->dropColumn('article_category_id');
        });
        Schema::table('articles', function(Blueprint $table)
        {
            $table->dropColumn('language_id');
        });

        Schema::drop('groups');
        Schema::drop('permission');
        Schema::drop('photo_albums');
        Schema::drop('photos');
        Schema::drop('player');
        Schema::drop('player_type');
        Schema::drop('player_year');
        Schema::drop('pick');
        Schema::drop('pick_games');
        Schema::drop('profile_field');
        Schema::drop('profile_field_type');
        Schema::drop('video_albums');
        Schema::drop('videos');

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
