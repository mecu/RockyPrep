<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_info', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->smallInteger('teamid')->unsigned()->index();
			$table->integer('hudl')->unsigned()->nullable();
			$table->string('website', 62)->nullable();
			$table->string('facebook', 126)->nullable();
			$table->bigInteger('facebook_id')->unsigned()->nullable();
			$table->string('twitter', 15)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_info');
	}

}
