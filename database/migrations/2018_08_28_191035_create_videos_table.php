<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->index('videos_language_id_foreign');
			$table->integer('position')->nullable();
			$table->string('filename')->nullable();
			$table->string('name')->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('youtube')->nullable();
			$table->integer('video_album_id')->unsigned()->nullable()->index('videos_video_album_id_foreign');
			$table->boolean('album_cover')->nullable();
			$table->integer('user_id')->unsigned()->nullable()->index('videos_user_id_foreign');
			$table->integer('user_id_edited')->unsigned()->nullable()->index('videos_user_id_edited_foreign');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
	}

}
