<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rate', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('teamid')->unsigned()->index();
			$table->year('year')->index();
			$table->decimal('rpi', 5, 4);
			$table->decimal('erpi', 5, 4);
			$table->decimal('rpi_wp', 5, 4);
			$table->decimal('rpi_owp', 5, 4);
			$table->decimal('rpi_oowp', 5, 4);
			$table->decimal('srs', 4, 1);
			$table->decimal('srs_avgpd', 4, 1);
			$table->decimal('srs_oppavg', 4, 1);
			$table->boolean('srs_numgames');
			$table->timestamps();
			$table->dateTime('deleted_at')->default('0000-00-00 00:00:00');
			$table->unique(['teamid','year'], 'teamid_2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rate');
	}

}
