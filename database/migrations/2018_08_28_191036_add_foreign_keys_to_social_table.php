<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSocialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('social', function(Blueprint $table)
		{
			$table->foreign('stateid', 'social_ibfk_1')->references('stateid')->on('state')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('social', function(Blueprint $table)
		{
			$table->dropForeign('social_ibfk_1');
		});
	}

}
