<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('photo_album_id')->references('id')->on('photo_albums')->onUpdate('RESTRICT')->onDelete('SET NULL');
			$table->foreign('user_id_edited')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('SET NULL');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			$table->dropForeign('photos_language_id_foreign');
			$table->dropForeign('photos_photo_album_id_foreign');
			$table->dropForeign('photos_user_id_edited_foreign');
			$table->dropForeign('photos_user_id_foreign');
		});
	}

}
