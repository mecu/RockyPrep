<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStadiumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('stadium', function(Blueprint $table)
		{
			$table->foreign('stateid', 'stadium_ibfk_1')->references('stateid')->on('state')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('stadium', function(Blueprint $table)
		{
			$table->dropForeign('stadium_ibfk_1');
		});
	}

}
