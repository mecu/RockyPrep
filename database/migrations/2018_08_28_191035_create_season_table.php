<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeasonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('season', function(Blueprint $table)
		{
			$table->increments('id');
			$table->year('year')->index();
			$table->smallInteger('teamid')->unsigned();
			$table->smallInteger('levelid')->unsigned()->nullable()->index()->comment('Only used for CO WC');
			$table->smallInteger('levelid_state')->unsigned();
			$table->smallInteger('enrollment')->unsigned()->nullable();
			$table->smallInteger('confid')->unsigned()->nullable()->index();
			$table->integer('divid')->nullable();
			$table->boolean('district')->nullable();
			$table->smallInteger('coachid')->unsigned()->nullable()->index();
			$table->boolean('level_alter')->nullable();
			$table->decimal('rpi', 5, 4);
			$table->decimal('rpi_wp', 5, 4);
			$table->decimal('rpi_owp', 5, 4);
			$table->decimal('rpi_oowp', 5, 4);
			$table->decimal('crpi', 5, 4)->nullable();
			$table->decimal('crpi_wp', 5, 4)->nullable();
			$table->decimal('crpi_owp', 5, 4)->nullable();
			$table->decimal('crpi_oowp', 5, 4)->nullable();
			$table->decimal('srs', 4, 1);
			$table->decimal('srs_avgpd', 4, 1);
			$table->decimal('srs_oppavg', 4, 1);
			$table->boolean('srs_numgames');
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['year','teamid']);
			$table->index(['teamid','confid','levelid','year']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('season');
	}

}
