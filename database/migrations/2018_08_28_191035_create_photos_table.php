<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->index('photos_language_id_foreign');
			$table->integer('position')->nullable();
			$table->boolean('slider')->nullable();
			$table->string('filename');
			$table->string('name')->nullable();
			$table->text('description', 65535)->nullable();
			$table->integer('photo_album_id')->unsigned()->nullable()->index('photos_photo_album_id_foreign');
			$table->boolean('album_cover')->nullable();
			$table->integer('user_id')->unsigned()->nullable()->index('photos_user_id_foreign');
			$table->integer('user_id_edited')->unsigned()->nullable()->index('photos_user_id_edited_foreign');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}
