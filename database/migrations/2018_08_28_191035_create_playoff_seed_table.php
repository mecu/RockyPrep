<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayoffSeedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playoff_seed', function(Blueprint $table)
		{
			$table->year('year')->index();
			$table->smallInteger('levelid')->index();
			$table->integer('teamid')->index();
			$table->smallInteger('seed');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playoff_seed');
	}

}
