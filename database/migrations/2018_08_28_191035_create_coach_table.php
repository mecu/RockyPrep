<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoachTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coach', function(Blueprint $table)
		{
			$table->smallInteger('coachid', true)->unsigned();
			$table->string('name', 63)->unique('name');
			$table->string('urlname', 63)->nullable()->unique('coach_urlname_uindex');
			$table->integer('hwin')->unsigned()->default(0)->comment('historical');
			$table->integer('hloss')->unsigned()->default(0)->comment('historical');
			$table->enum('complete', array('0','1'))->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coach');
	}

}
