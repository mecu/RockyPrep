<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStadiumTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stadium', function(Blueprint $table)
		{
			$table->smallInteger('stadiumid', true)->unsigned();
			$table->string('name', 64)->unique('stadium_name_uindex');
			$table->boolean('stateid')->index();
			$table->text('location', 65535)->nullable();
			$table->text('alt')->nullable();
			$table->string('address', 125)->nullable();
			$table->integer('zip')->nullable();
			$table->float('lat', 10, 6)->nullable();
			$table->float('lng', 10, 6)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stadium');
	}

}
