<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('game', function(Blueprint $table)
		{
			$table->foreign('hometeam', 'game_ibfk_1')->references('teamid')->on('team')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('awayteam', 'game_ibfk_3')->references('teamid')->on('team')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('stadiumid', 'game_ibfk_4')->references('stadiumid')->on('stadium')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('game', function(Blueprint $table)
		{
			$table->dropForeign('game_ibfk_1');
			$table->dropForeign('game_ibfk_3');
			$table->dropForeign('game_ibfk_4');
		});
	}

}
