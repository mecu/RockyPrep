<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->index('articles_language_id_foreign');
			$table->integer('position')->nullable();
			$table->integer('user_id')->unsigned()->nullable()->index('articles_user_id_foreign');
			$table->integer('user_id_edited')->unsigned()->nullable()->index('articles_user_id_edited_foreign');
			$table->integer('article_category_id')->unsigned()->nullable()->index('articles_article_category_id_foreign');
			$table->string('title');
			$table->string('slug')->nullable();
			$table->text('introduction', 65535);
			$table->text('content', 65535);
			$table->string('source')->nullable();
			$table->string('picture')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}
