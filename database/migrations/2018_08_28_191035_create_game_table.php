<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('game', function(Blueprint $table)
		{
			$table->increments('gameid');
			$table->smallInteger('hometeam')->unsigned()->index('hometeam');
			$table->smallInteger('awayteam')->unsigned()->index('awayteam');
			$table->boolean('homescore');
			$table->boolean('awayscore');
			$table->smallInteger('stadiumid')->unsigned()->nullable()->index('stadiumid');
			$table->boolean('neutral')->default(0);
			$table->date('date')->default('0000-00-00')->index('date');
			$table->time('time')->default('00:00:00');
			$table->smallInteger('confid')->unsigned()->nullable();
			$table->boolean('srs_predict')->nullable();
			$table->integer('srs_teamid')->unsigned()->nullable();
			$table->smallInteger('winner')->unsigned()->nullable()->index('winner');
			$table->boolean('forfeit')->nullable();
			$table->boolean('tournament')->default(0);
			$table->boolean('playoff')->nullable();
			$table->integer('reportedby')->unsigned()->nullable();
			$table->string('contesturl', 22)->nullable()->unique('contesturl_2')->comment('MaxPreps Contest URL');
			$table->string('contestid', 36)->nullable()->unique('contestid');
			$table->boolean('human')->nullable();
			$table->boolean('want_to_delete')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->index(['hometeam','date'], 'hometeam_games');
			$table->index(['awayteam','date'], 'awayteam_games');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('game');
	}

}
