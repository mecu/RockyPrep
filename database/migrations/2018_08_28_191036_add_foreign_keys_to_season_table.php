<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeasonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('season', function(Blueprint $table)
		{
			$table->foreign('teamid', 'season_ibfk_1')->references('teamid')->on('team')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('coachid', 'season_ibfk_3')->references('coachid')->on('coach')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('season', function(Blueprint $table)
		{
			$table->dropForeign('season_ibfk_1');
			$table->dropForeign('season_ibfk_3');
		});
	}

}
