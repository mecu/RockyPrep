<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePacTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pac', function(Blueprint $table)
		{
			$table->integer('confid')->index('confid');
			$table->year('year')->default(0000)->index();
			$table->boolean('auto')->comment('Number of automatic qualifiers');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pac');
	}

}
