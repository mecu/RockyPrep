<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sport', function(Blueprint $table)
		{
			$table->boolean('sportid')->primary();
			$table->string('name', 16);
			$table->string('urlname', 19)->unique();
			$table->string('classname', 16);
			$table->string('twitter', 4);
			$table->string('maxpreps_url_form', 30);
			$table->string('maxpreps_url_schedule', 60);
			$table->string('maxpreps_mobile_sport_form', 60);
			$table->string('maxpreps_season', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sport');
	}

}
