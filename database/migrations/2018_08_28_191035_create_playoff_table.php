<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayoffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playoff', function(Blueprint $table)
		{
			$table->integer('gameid');
			$table->year('year')->index();
			$table->boolean('levelid')->index();
			$table->integer('gamenum');
			$table->enum('flip', array('0','1'))->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playoff');
	}

}
