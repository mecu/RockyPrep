<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePickTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pick', function(Blueprint $table)
		{
			$table->integer('userid');
			$table->integer('gameid')->unsigned();
			$table->integer('pick');
			$table->index(['userid','gameid'], 'userid');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pick');
	}

}
