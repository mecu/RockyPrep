<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlayerYearTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('player_year', function(Blueprint $table)
		{
			$table->foreign('playerid', 'player_year_ibfk_2')->references('playerid')->on('player')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('teamid', 'player_year_ibfk_3')->references('teamid')->on('team')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('player_year', function(Blueprint $table)
		{
			$table->dropForeign('player_year_ibfk_2');
			$table->dropForeign('player_year_ibfk_3');
		});
	}

}
