<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team', function(Blueprint $table)
		{
			$table->smallInteger('teamid', true)->unsigned();
			$table->string('name', 100)->index();
			$table->string('urlname', 103)->unique();
			$table->string('mascot', 48)->nullable();
			$table->string('city', 48)->nullable();
			$table->boolean('stateid')->index();
			$table->enum('jv', array('0','1'))->default('0');
			$table->string('color1', 6)->nullable();
			$table->string('color2', 6)->nullable();
			$table->string('address', 128)->nullable();
			$table->string('zip', 9)->nullable();
			$table->float('lat', 10, 0)->nullable();
			$table->float('lng', 10, 0)->nullable();
			$table->string('twitter', 15)->nullable();
			$table->string('maxpreps_name', 125)->nullable()->unique('maxpreps_name');
			$table->string('maxpreps_id', 36)->nullable()->unique('maxpreps_id');
			$table->bigInteger('facebook_id')->nullable();
			$table->string('facebook', 128)->nullable();
			$table->string('website', 128)->nullable();
			$table->integer('hudl')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team');
	}

}
