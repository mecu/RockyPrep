<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerYearTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_year', function(Blueprint $table)
		{
			$table->integer('playerid')->unsigned()->index('playerid');
			$table->smallInteger('teamid')->unsigned()->index('teamid');
			$table->year('year')->index();
			$table->smallInteger('jersey')->unsigned();
			$table->boolean('height');
			$table->smallInteger('weight')->unsigned();
			$table->boolean('captain')->nullable();
			$table->boolean('pos1');
			$table->boolean('pos2')->nullable();
			$table->boolean('pos3')->nullable();
			$table->boolean('pos4')->nullable();
			$table->boolean('pos5')->nullable();
			$table->text('notes', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_year');
	}

}
