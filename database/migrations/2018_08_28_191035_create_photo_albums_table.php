<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotoAlbumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photo_albums', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->index('photo_albums_language_id_foreign');
			$table->integer('position')->nullable();
			$table->string('name');
			$table->text('description', 65535)->nullable();
			$table->string('folder_id');
			$table->integer('user_id')->unsigned()->nullable()->index('photo_albums_user_id_foreign');
			$table->integer('user_id_edited')->unsigned()->nullable()->index('photo_albums_user_id_edited_foreign');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photo_albums');
	}

}
