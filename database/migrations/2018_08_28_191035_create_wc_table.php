<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWcTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wc', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->year('year')->index();
			$table->boolean('levelid')->index();
			$table->boolean('opplevel');
			$table->boolean('points');
			$table->boolean('jv')->default(0);
			$table->boolean('win');
			$table->boolean('oppwins');
			$table->index(['year','levelid','opplevel','win','oppwins']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wc');
	}

}
