<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_info', function(Blueprint $table)
		{
			$table->foreign('teamid', 'team_info_ibfk_1')->references('teamid')->on('team')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_info', function(Blueprint $table)
		{
			$table->dropForeign('team_info_ibfk_1');
		});
	}

}
