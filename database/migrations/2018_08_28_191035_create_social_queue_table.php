<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSocialQueueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('social_queue', function(Blueprint $table)
		{
			$table->increments('queueid');
			$table->boolean('sportid');
			$table->boolean('stateid');
			$table->enum('social', array('twitter','facebook'))->default('twitter');
			$table->text('text', 65535);
			$table->boolean('tries')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('social_queue');
	}

}
