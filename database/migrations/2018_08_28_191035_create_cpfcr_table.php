<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCpfcrTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cpfcr', function(Blueprint $table)
		{
			$table->boolean('levelid')->index('levelid_2');
			$table->year('year')->default(0000)->index('year');
			$table->boolean('week')->index('week');
			$table->boolean('pollid')->index('pollid');
			$table->integer('teamid')->unsigned();
			$table->boolean('rank');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cpfcr');
	}

}
