<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLevelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('level', function(Blueprint $table)
		{
			$table->smallInteger('levelid', true)->unsigned();
			$table->string('name', 32)->index();
			$table->integer('stateid')->index();
			$table->string('urlname', 32)->index();
			$table->string('image', 32);
			$table->integer('order')->nullable();
			$table->boolean('private')->default(0);
			$table->unique(['stateid','urlname'], 'level_stateid_urlname_uindex');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('level');
	}

}
