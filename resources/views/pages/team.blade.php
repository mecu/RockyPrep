@extends('layouts.master-sidebarright')

@section('title')
    {{ $title }} @parent
@stop

@section('sidebar')
    @if ( $state->stateid === 6)
        <a href="https://twitter.com/RockyPrep" class="twitter-follow-button"
           data-show-count="false" data-size="large">
            Follow @RockyPrep
        </a>
        <a class="twitter-timeline" data-width="220" data-height="700"
           href="https://twitter.com/RockyPrep?ref_src=twsrc%5Etfw">
            Tweets by RockyPrep
        </a>
    @else
        <a href="https://twitter.com/RockyPrep{!! $state->abbr !!}" class="twitter-follow-button"
           data-show-count="false" data-size="large">
            Follow @RockyPrep{!! $state->abbr !!}
        </a>
        <a class="twitter-timeline" data-width="220" data-height="700"
           href="https://twitter.com/RockyPrep{!! $state->abbr !!}?ref_src=twsrc%5Etfw">
            Tweets by RockyPrep{!! $state->abbr !!}
        </a>
    @endif
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@stop

@section('content')
    @include('partials.years', ['year' => $year, 'years' => $years, 'state' => $state, 'route' => 'team', 'params' => ['team' => $team->urlname]])

    <h2>{{ $yearDisplay }} <span itemprop="name">{{ $team->name }} {{ $team->mascot }}</span> Football Schedule</h2>

    @if ( $trust )
        <div class="card border border-primary">
            <div class="card-body border-primary">
                <h3 class="card-title">You are a trusted user for this team.</h3>
                <div class="card-text">
                    Team ID: {{ $team->teamid }}
                    <a href="#addGameModal" class="btn btn-success" data-toggle="modal">Add Game</a>
                    <a href="#editTeamInfoModal" class="btn btn-info" data-toggle="modal">Edit Team Information</a>
                    <a href="#editTeamModal" class="btn btn-warning" data-toggle="modal">Edit School Data</a>
                    <a href="#editSeasonModal" class="btn btn-primary" data-toggle="modal">Edit Season</a>
                </div>
            </div>
        </div>
    @endif

    @if ( isset($nogames) )
        <h3>There are no games yet scheduled for this team for this season.</h3>
    @else
        <div itemscope itemtype="http://data-vocabulary.org/Organization">
            @if ( $team->hasColors() )
                <div class="pull-right teamcolorbox">
                    <div class="pull-right teamcolor" style="background-color:#{!! $team->color2 !!}"></div>
                    <div class="pull-right teamcolor" style="background-color:#{!! $team->color1 !!}"></div>
                </div>
            @endif

            @if ( $team->getHelmet() )
                <div class="pull-right img-thumbnail helmet">
                    <img src="/{{ $team->getHelmet() }}" height="150"
                         alt="{{ $team->name }} High School Football Helmet"/>
                </div>
            @endif

            <div class="teaminfo-inset">
                <p>
                    @if ( $team->city )
                        from <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">{!! $team->getCity(true) !!}, {{ $team->state->name }}</span>
                    @endif
                    @if ( $season->levelid_state )
                        playing {!! link_to_route('standings', 'level ' . $season->levelState->name, ['state' => $state->urlname, 'level' => $season->levelState->urlname, 'year' => $year->year]) !!}
                    @endif
                    @if ( $season->conference )
                        in
                        the {!! link_to_route('conference', $season->conference->name . ' conference', ['state' => $state->urlname, 'conference' => $season->conference->urlname, 'year' => $year->year]) !!}
                    @endif

                    @if ( $team->lat && $team->lng)
                        <span class="geo">
                            <span class="latitude">
                              <span class="value-title" title="{{ $team->lat }}"></span>
                            </span>
                            <span class="longitude">
                              <span class="value-title" title="{{ $team->lng }}"></span>
                            </span>
                        </span>
                    @endif
                </p>
                @if ( $season->coach )
                    Head Coach: {!! link_to_route('coach', $season->coach->name , ['state' => $state->urlname, 'coach' => $season->coach->urlname, 'year' => $year]) !!}
                @endif
                <p>
                    {!! $season->team->getTwitter() !!}
                    {!! $season->team->getFacebook() !!}
                    {!! $season->team->getHudl() !!}
                    {!! $season->team->getWebsite() !!}
                </p>

                @if ( $year->year > 2010 )
                    <h3>{!! link_to_route('rpi', 'RPRPI' , ['state' => $state->urlname, 'year' => $year->year]) !!}: {!! sprintf('%0.4f', $season->rpi) !!}</h3>
                    <h3>{!! link_to_route('srs', 'SRS' , ['state' => $state->urlname, 'year' => $year->year]) !!}: {{ $season->srs }}</h3>
                @endif
            </div>
                
            <div class="clearfix"></div>
            {!! App\Helpers\Adblock::top() !!}
            <div class="clearfix"></div>

            <div class="table-responsive">
                <table id="teamSchedule" class="table table-hover table-sm table-striped">
                    <thead>
                    <tr class="active">
                        <th class="text-center align-middle">
                            Date
                        </th>
                        <th class="text-center align-middle" colspan="3">
                            Opponent<br>
                            <small><em>Location</em></small>
                        </th>
                        <th class="text-center align-middle">
                            Result
                        </th>
                        @if ( $coloradoFootball )
                            <th class="text-center">
                                Justification
                            </th>
                            <th class="text-center">
                                <abbr title="1st level tie-break points">1st</abbr>
                            </th>
                            <th class="text-center">
                                <abbr title="2nd level tie-break points">2nd</abbr>
                            </th>
                            <th class="text-center">
                                <abbr title="Total tie-break points">Total</abbr>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ( $schedule as $gameRow )
                        <tr class="@if($gameRow['game']->isDeleted()) table-info
                                    @elseif($gameRow['game']->status($team) === 'WIN') table-success
                                    @elseif($gameRow['game']->status($team) === 'LOSE') table-danger
                                    @elseif($gameRow['game']->status($team) === 'TIE') table-warning
                                    @endif
                                " itemscope itemtype="http://data-vocabulary.org/Event"
                            @if($trust && $gameRow['game']->human) style="border:1px solid red;border-collapse: collapse;" @endif
                        >
                            <td class="text-center align-middle">
                                <time itemprop="startDate"
                                      datetime="{{ strtotime($gameRow['game']->date->format('Y-m-d')) }}">
                                    {{ link_to_route('schedule', App\Helpers\Pretty::prettydate($gameRow['game']->date), [
                                    'state' => $state->urlname,
                                    'year'  => $gameRow['game']->date->format('Y'),
                                    'month' => $gameRow['game']->date->format('n'),
                                    'day'   => $gameRow['game']->date->format('j'),
                                    ]) }}
                                </time>
                            </td>
                            <td class="text-center align-middle">
                                @if ($gameRow['game']->isAway($team)) @
                                @else &nbsp;
                                @endif
                                @if ( $trust )
                                    <br><a href="#swapModal" class="btn btn-xs btn-primary" data-toggle="modal"
                                           data-id="{{ $gameRow['game']->gameid }}">Swap Home/Away</a>
                                @endif
                            </td>
                            <td itemprop="summary" class="text-left align-middle">
                                @if ($gameRow['game']->opponent($team)->state !== null)
                                {{ link_to_route('team', $gameRow['game']->opponent($team)->name, [
                                    $gameRow['game']->opponent($team)->state->urlname,
                                    $gameRow['game']->opponent($team)->urlname,
                                    $year->year,
                                ], ['itemprop' => 'url']) }}
                                @else
                                    {{ $gameRow['game']->opponent($team)->name }}
                                @endif
                                @if ($gameRow['game']->opponentSeason($team, $year)->isPlayUp()) <i
                                        class="fa fa-arrow-up"></i>
                                @elseif ($gameRow['game']->opponentSeason($team, $year)->isPlayDown()) <i
                                        class="fa fa-arrow-down"></i>
                                @endif
                                @if ($gameRow['game']->opponent($team)->state !== null && $team->state->stateid !== $gameRow['game']->opponent($team)->state->stateid) {{ $gameRow['game']->opponent($team)->state->abbr }} @endif

                                ({{ $gameRow['game']->opponentSeason($team, $year)->getWins() }}&ndash;{{ $gameRow['game']->opponentSeason($team, $year)->getLoss() }})

                                @if ( $year->year > 2010 )<amp
                                        class="badge badge-primary pull-right mr-4">{!! sprintf('%0.4f', $gameRow['game']->opponentSeason($team, $year)->rpi) !!}</amp>
                                    @endif
                                @if ($gameRow['game']->stadiumid) <br>
                                <em>{{ $gameRow['game']->stadium->name}}</em> @endif
                            </td>
                            <td class="text-center align-middle">
                                @if ($gameRow['game']->confid) <i class="fa fa-copyright"></i>
                                @else &nbsp;
                                @endif
                            </td>
                            @if ( $gameRow['game']->hasBoxscore() )
                                <td class="text-center align-middle"
                                    data-content="@include('partials.boxscore', ['game' => $gameRow['game']])"
                                    data-toggle="popover"
                                >
                            @else
                                <td class="text-center align-middle">
                            @endif
                                    @if ( $gameRow['game']->hasWinner())
                                        {{ $gameRow['game']->result($team) }} {{ $gameRow['game']->numberOT() }}
                                        @if ( $gameRow['game']->playoff ) <br><em>Playoff</em> @endif
                                        @if ( $gameRow['game']->forfeit ) <br><em>Forfeit</em> @endif
                                        @if ( $gameRow['game']->hasBoxscore() ) &beta; @endif
                                    @elseif ( $gameRow['game']->hasPredict() ) <span class="label label-info">&nbsp;{{ $gameRow['predict'] }}&nbsp;</span>
                                    @endif
                                    @if ( $trust ) <br>
                                        @if ( !$gameRow['game']->deleted_at && date('Y-m-d') >= $gameRow['game']->date->format('Y-m-d'))
                                            <a href="#reportModal" class="btn btn-success" data-toggle="modal"
                                               data-id="{{ $gameRow['game']->gameid }}"
                                               data-home-team-name="{{ $gameRow['game']->home->name }}"
                                               data-home-team-id="{{ $gameRow['game']->home->teamid }}"
                                               data-home-team-score="{{ $gameRow['game']->homescore }}"
                                               data-away-team-name="{{ $gameRow['game']->away->name }}"
                                               data-away-team-id="{{ $gameRow['game']->away->teamid }}"
                                               data-away-team-score="{{ $gameRow['game']->awayscore }}"
                                               data-winner="{{ $gameRow['game']->winner }}"
                                            >Report</a><br>
                                        @endif
                                        <a href="#editGameModal" class="btn btn-warning" data-toggle="modal"
                                           data-id="{{ $gameRow['game']->gameid }}"
                                           data-date="{{ $gameRow['game']->date->format('Y-m-d') }}"
                                           data-forfeit="{{ $gameRow['game']->forfeit }}"
                                           data-neutral="{{ $gameRow['game']->neutral }}"
                                           data-playoff="{{ $gameRow['game']->playoff }}"
                                           data-conference="{{ $gameRow['game']->confid }}"
                                           data-stadium="{{ $gameRow['game']->stadiumid }}"
                                        >Edit</a>
                                        @if ( $gameRow['game']->isDeleted() )
                                            <a href="#restoreModal" class="btn btn-info" data-toggle="modal"
                                               data-id="{{ $gameRow['game']->gameid }}">Restore</a>
                                        @else
                                            <a href="#deleteModal" class="btn btn-danger" data-toggle="modal"
                                               data-id="{{ $gameRow['game']->gameid }}">Delete</a>
                                        @endif
                                    @endif
                                </td>
                                @if ( $coloradoFootball )
                                    @if ($gameRow['game']->playoff)
                                        <td colspan="4" class="text-center align-middle">
                                            <em>Playoff</em>
                                        </td>
                                    @else
                                        <td class="text-center">
                                            {!! $gameRow['justify'] !!}
                                        </td>
                                        <td class="text-center">
                                            {{ $gameRow['tb1'] }}
                                        </td>
                                        <td class="text-center">
                                            {{ $gameRow['tb2'] }}
                                        </td>
                                        <td class="text-center">
                                            {{ $gameRow['tbt'] }}
                                        </td>
                                    @endif
                                @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr class="active">
                        <th colspan="4">&nbsp;</th>
                        <th class="text-center">
                            {{ $season->getWins() }}&ndash;{{ $season->getLoss() }}
                                ({{ $season->getConferenceWins() }}&ndash;{{ $season->getConferenceLoss() }})
                        </th>
                        @if ( $coloradoFootball )
                            <th class="text-center">
                                {{ isset($scheduleSummary['sumPoints']) ? $scheduleSummary['sumPoints'] : 0 }}
                            </th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th class="text-center">
                                {{ isset($scheduleSummary['sumTBP']) ? $scheduleSummary['sumTBP'] : 0 }}
                            </th>
                        @endif
                    </tr>
                    </tfoot>
                </table>
            </div>
            {!! App\Helpers\Adblock::bottom() !!}

            <div class="clearfix"></div>
            <h4>Legend:</h4>
            <dl class="row border border-info rounded">
                @if ( $state->stateid === 6 )
                    <dt class="col-2 text-right">Records</dt>
                    <dd class="col-10">Records for end of regular season.</dd>
                @endif
                <dt class="col-2 text-right"><i class="fa fa-copyright"></i></dt>
                <dd class="col-10">Conference Game</dd>
                <dt class="col-2 text-right">&beta;</dt>
                <dd class="col-10">Box Score Available (hover over to see)</dd>
                <dt class="col-2 text-right"><samp class="badge badge-primary">0.6789</samp></dt>
                <dd class="col-10">RPI</dd>
                <dt class="col-2 text-right"><span class="label label-info">&nbsp;-7&nbsp;</span></dt>
                <dd class="col-10">Favored to win by this number</dd>
                <dt class="col-2 text-right"><span class="label label-info">&nbsp;7&nbsp;</span></dt>
                <dd class="col-10">Opponent is favored to win by this number</dd>
                @if ( $state->stateid === 6 )
                    <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
                    <dd class="col-10">Playing up. See CHSAA for details.</dd>
                    <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
                    <dd class="col-10">Playing down. See CHSAA for details.</dd>
                @endif
            </dl>

            <script async>
                $(function () {
                    $('[data-toggle="popover"]').popover({
                        html:true,
                        placement:'left',
                        boundary:'window',
                        trigger:'hover',
                        container:'body',
                        gpuAcceleration: false,
                    })
                })
            </script>
            @endif
        </div>
    @if ( $trust )
        <script>
            $().ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'api/v1/game',
                    async: false
                });

                {{-- Generic setting the data-id for all game related buttons (except add, since we don't know an id yet) --}}
                $("#deleteModal, #restoreModal, #reportModal, #swapModal").on('show.bs.modal', function (event) {
                    $(this).data('id', $(event.relatedTarget).data('id'));
                });

                $("#reportModal").on('show.bs.modal', function (event) {
                    $('.awayTeam').text($(event.relatedTarget).data('awayTeamName'));
                    $('.homeTeam').text($(event.relatedTarget).data('homeTeamName'));
                    $('#awayWinner').val($(event.relatedTarget).data('awayTeamId'));
                    $('#homeWinner').val($(event.relatedTarget).data('homeTeamId'));
                    $('#awayteamscore').val($(event.relatedTarget).data('awayTeamScore'));
                    $('#hometeamscore').val($(event.relatedTarget).data('homeTeamScore'));
                    if ($(event.relatedTarget).data('winner') == $(event.relatedTarget).data('awayTeamId')) {
                        $('#awayWinner').prop('checked', true);
                    } else if ($(event.relatedTarget).data('winner') == $(event.relatedTarget).data('homeTeamId')) {
                        $('#homeWinner').prop('checked', true);
                    }
                    var button = $(event.relatedTarget);
                    $(this).find('#report-id').text(button.data('id'));
                });

                $("#editGameModal").on('show.bs.modal', function (event) {
                    var $editGameModal = $('#editGameModal');
                    $editGameModal.find('#gameid').val($(event.relatedTarget).data('id'));
                    $editGameModal.find('.date').val($(event.relatedTarget).data('date'));
                    $editGameModal.find('input[name=forfeit][value="0"]').prop('checked', $(event.relatedTarget).data('forfeit') == '');
                    $editGameModal.find('input[name=forfeit][value="1"]').prop('checked', $(event.relatedTarget).data('forfeit') !== '');
                    $editGameModal.find('input[name=neutral][value="0"]').prop('checked', $(event.relatedTarget).data('neutral') == '');
                    $editGameModal.find('input[name=neutral][value="1"]').prop('checked', $(event.relatedTarget).data('neutral') !== '');
                    $editGameModal.find('input[name=playoff][value="0"]').prop('checked', $(event.relatedTarget).data('playoff') == '');
                    $editGameModal.find('input[name=playoff][value="1"]').prop('checked', $(event.relatedTarget).data('playoff') !== '');
                    $editGameModal.find('input[name=conference][value="0"]').prop('checked', $(event.relatedTarget).data('conference') == 0);
                    $editGameModal.find('input[name=conference][value="1"]').prop('checked', $(event.relatedTarget).data('conference') != 0);
                    $editGameModal.find('.stadium').prop('value', $(event.relatedTarget).data('stadium'));
                    $editGameModal.find('.stadiumid').prop('value', $(event.relatedTarget).data('stadiumid'));
                });

                $("#editGameButton").on('click', function () {
                    $.ajax({
                        method: 'PUT',
                        data: $('#editGameForm').serializeArray(),
                        url: '{{ URL::route('apiPutGame') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });


                $("#deleteButton").on('click', function () {
                    $.ajax({
                        method: 'DELETE',
                        data: {id: $('#deleteModal').data('id')},
                        url: '{{ URL::route('apiDeleteGame') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                $("#restoreButton").on('click', function () {
                    $.ajax({
                        method: 'PUT',
                        data: {
                            id: $('#restoreModal').data('id'),
                            undelete: true
                        },
                        url: '{{ URL::route('apiPutGame') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                $("#swapButton").on('click', function () {
                    $.ajax({
                        method: 'PUT',
                        data: {
                            id: $('#swapModal').data('id'),
                            swap: true
                        },
                        url: '{{ URL::route('apiPutGame') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                $("#reportButton").on('click', function () {
                    var $homeTeamScore = $('#hometeamscore');
                    var $awayTeamScore = $('#awayteamscore');
                    var $reportModal = $('#reportModal');
                    var $winnerChecked = $reportModal.find('input[name=winner]:checked');

                    if ($homeTeamScore.val() < 0) {
                        alert('The home score cannot be less than zero.');
                        return false;
                    }
                    if ($awayTeamScore.val() < 0) {
                        alert('The away score cannot be less than zero.');
                        return false;
                    }
                    if ($winnerChecked.val() == undefined) {
                        alert('You must pick a winner!');
                        return false;
                    }

                    var $awayTeamScores = $reportModal.find('.boxscore.away');
                    var $homeTeamScores = $reportModal.find('.boxscore.home');

                    $.ajax({
                        method: 'PUT',
                        url: '{{ URL::route('apiPutGame') }}',
                        data: {
                            id: $reportModal.data('id'),
                            homescore: $homeTeamScore.val(),
                            awayscore: $awayTeamScore.val(),
                            winner: $winnerChecked.val(),
                            awayscores: $awayTeamScores.serialize(),
                            homescores: $homeTeamScores.serialize()
                        },
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                $("#editTeamInfoButton").on('click', function () {
                    $.ajax({
                        method: 'PUT',
                        data: $('#editTeamInfoForm').serializeArray(),
                        url: '{{ URL::route('adminAPITeamInfo') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                $("#editSeasonButton").on('click', function () {
                    $.ajax({
                        method: 'PUT',
                        data: $('#editSeasonForm').serializeArray(),
                        url: '{{ URL::route('apiPutSeason') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                $("#addGameButton").on('click', function () {
                    $.ajax({
                        method: 'POST',
                        data: $('#addGameForm').serializeArray(),
                        url: '{{ URL::route('apiPostGame') }}',
                        success: ajaxSuccess,
                        error: ajaxError
                    });
                });

                function ajaxSuccess() {
                    location.reload(true);
                }

                function ajaxError(err) {
                    var msg = $.parseJSON(err).msg;
                    alert(msg);
                    return false;
                }

                $("#teamSearch").autocomplete({
                    minLength: 3,
                    delay: 280,
                    source: function (request, response) {
                        var $addGameForm = $('#addGameForm');
                        $.getJSON("/api/v1/search", {
                            term: request.term,
                            stateid: $addGameForm.find('#searchAll').prop('checked') ? null : $addGameForm.data('stateid')
                        }, function (data) {
                            response(data);
                        });
                    },
                    select: function (event, ui) {
                        $("#teamSearch").val(ui.item.label);
                        $("#opponentid").val(ui.item.id);
                        return false;
                    }
                })
                        .data('ui-autocomplete')._renderItem = function (ul, item) {
                    return $("<li>")
                            .append("<a>" + item.label + "<br>" + item.loc + "</a>")
                            .appendTo(ul);
                };

                $("#stadiumSearch").autocomplete({
                    minLength: 3,
                    delay: 280,
                    source: function (request, response) {
                        var $editGameForm = $('#editGameForm');
                        $.getJSON('{{ URL::route('apiStadiumSearch') }}', {
                            term: request.term,
                            stateid: $editGameForm.find('#searchAll').prop('checked') ? null : $editGameForm.data('stateid')
                        }, function (data) {
                            response(data);
                        });
                    },
                    select: function (event, ui) {
                        $("#stadiumSearch").val(ui.item.label);
                        $("#stadiumid").val(ui.item.id);
                        return false;
                    }
                })
                        .data('ui-autocomplete')._renderItem = function (ul, item) {
                    return $("<li>")
                            .append("<a>" + item.label + "</a>")
                            .appendTo(ul);
                };
            });
        </script>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <div id="addGameModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addGameModalLabel">Add Game</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form" id="addGameForm" data-stateid="{{ $state->stateid }}">
                            {!! Form::input('number', 'teamid', $team->teamid, ['class'=>'teamid d-none', 'id'=>'teamid']) !!}
                            {!! Form::input('number', 'human', 1, ['class'=>'human d-none', 'id'=>'human']) !!}
                            <div class="modal-body">
                                <div class="form-group">
                                    {!! Form::label('Date') !!}
                                    {!! Form::input('date', 'date', date('Y-m-d'), ['class'=>'form-control date']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Away Game') !!}
                                    {!! Form::checkbox('away', 1, null, ['class'=>'form-control away']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Neutral Field') !!}
                                    Yes {!! Form::radio('neutral', 1, null) !!}
                                    No {!! Form::radio('neutral', 0, true) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Playoff') !!}
                                    Yes {!! Form::radio('playoff', 1, null) !!}
                                    No {!! Form::radio('playoff', 0, true) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Conference Game') !!}
                                    Yes {!! Form::radio('conference', 1, null) !!}
                                    No {!! Form::radio('conference', 0, true) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Search all States') !!}
                                    {!! Form::checkbox('searchAll', 'true', null, ['class'=>'form-control searchAll', 'id'=>'searchAll']) !!}
                                </div>
                                <div class="form-group ui-front">
                                    {!! Form::label('Opponent') !!}
                                    <input id="teamSearch" class="form-control input-lg" type="text"
                                           title="Team Finder"/>

                                    <p id="help-block" class="help-block">3 characters minimum. Results are limited to
                                        10.</p>
                                    {!! Form::input('number', 'opponentid', '', ['class'=>'opponentid d-none', 'id'=>'opponentid']) !!}
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                            Close
                        </button>
                        <button type="button" class="btn btn-info" id="addGameButton"><i class="fa fa-check"></i>
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Really Delete?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to soft delete this game?</p>

                        <div id="modal-id" class="d-none"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i> Close
                        </button>
                        <button type="button" class="btn btn-danger" id="deleteButton"><i class="fa fa-ban"></i>
                            Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="restoreModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="restoreModalLabel">Really Restore?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to restore this game?</p>

                        <div id="modal-id" class="d-none"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i> Close
                        </button>
                        <button type="button" class="btn btn-info" id="restoreButton"><i class="fa fa-check"></i>
                            Restore
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="swapModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="swapModalLabel">Really Swap?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to swap home and away teams?</p>

                        <div id="modal-id" class="d-none"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i> Close
                        </button>
                        <button type="button" class="btn btn-info" id="swapButton"><i class="fa fa-exchange"></i>
                            Swap
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="reportModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog mw-100 w-75" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="reportModalLabel">Report Game Results</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="report-id" class="d-none"></div>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Team</th>
                                <th>Q1</th>
                                <th>Q2</th>
                                <th>Q3</th>
                                <th>Q4</th>
                                <th colspan="2">Total</th>
                                <th>Winner</th>
                            </tr>
                            </thead>
                            <tr>
                                <td><span class="awayTeam"></span></td>
                                <td>{!! Form::input('number', 'awayscores[1]', null, ['class'=>'form-control number boxscore score away', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("away", "awayTotal")']) !!}</td>
                                <td>{!! Form::input('number', 'awayscores[2]', null, ['class'=>'form-control number boxscore score away', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("away", "awayTotal")']) !!}</td>
                                <td>{!! Form::input('number', 'awayscores[3]', null, ['class'=>'form-control number boxscore score away', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("away", "awayTotal")']) !!}</td>
                                <td>{!! Form::input('number', 'awayscores[4]', null, ['class'=>'form-control number boxscore score away', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("away", "awayTotal")']) !!}</td>
                                <td><span id="awayTotal"></span></td>
                                <td>{!! Form::input('number', 'awayteamscore', null, ['class'=>'form-control number', 'min'=>0, 'style'=>'width:60px', 'id'=>'awayteamscore', 'required'=>'required']) !!}</td>
                                <td><input type="radio" name="winner" id="awayWinner" required value="" title="Away Team Wins"></td>
                            </tr>
                            <tr>
                                <td><span class="homeTeam"></span></td>
                                <td>{!! Form::input('number', 'homescores[1]', null, ['class'=>'form-control number boxscore score home', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("home", "homeTotal")']) !!}</td>
                                <td>{!! Form::input('number', 'homescores[2]', null, ['class'=>'form-control number boxscore score home', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("home", "homeTotal")']) !!}</td>
                                <td>{!! Form::input('number', 'homescores[3]', null, ['class'=>'form-control number boxscore score home', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("home", "homeTotal")']) !!}</td>
                                <td>{!! Form::input('number', 'homescores[4]', null, ['class'=>'form-control number boxscore score home', 'min'=>0, 'style'=>'width:60px', 'onblur' => 'sum("home", "homeTotal")']) !!}</td>
                                <td><span id="homeTotal"></span></td>
                                <td>{!! Form::input('number', 'hometeamscore', null, ['class'=>'form-control number', 'min'=>0, 'style'=>'width:60px', 'id'=>'hometeamscore', 'required'=>'required']) !!}</td>
                                <td><input type="radio" name="winner" id="homeWinner" required value="" title="Home Team Wins"></td>
                            </tr>
                            <tr>
                                <td colspan="6"></td>
                                <td>Tie</td>
                                <td><input type="radio" name="winner" required value="0" title="A TIE!"></td>
                            </tr>
                        </table>
                        <script>
                            function sum(scores, total)
                            {
                                let arr = document.getElementsByClassName("number " + scores);
                                let tot=0;
                                for(let i=0;i<arr.length;i++){
                                    if(parseInt(arr[i].value))
                                        tot += parseInt(arr[i].value);
                                }
                                document.getElementById(total).textContent = tot;
                            }
                        </script>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i> Close
                        </button>
                        <button type="button" class="btn btn-success" id="reportButton"><i class="fa fa-check"></i>
                            Report
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="editGameModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editGameModalLabel">Edit Game Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form" id="editGameForm">
                            {!! Form::input('number', 'id', null, ['class'=>'gameid d-none', 'id'=>'gameid']) !!}
                            <div class="form-group">
                                {!! Form::label('Date') !!}
                                {!! Form::input('date', 'date', date('Y-m-d'), ['class'=>'form-control date']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Forfeit') !!}
                                Yes {!! Form::radio('forfeit', 1) !!}
                                No {!! Form::radio('forfeit', 0) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Neutral Field') !!}
                                Yes {!! Form::radio('neutral', 1) !!}
                                No {!! Form::radio('neutral', 0) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Playoff') !!}
                                Yes {!! Form::radio('playoff', 1) !!}
                                No {!! Form::radio('playoff', 0) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Conference Game') !!}
                                Yes {!! Form::radio('conference', 1) !!}
                                No {!! Form::radio('conference', 0) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Search all States for Stadium') !!}
                                {!! Form::checkbox('searchAll', 'true', null, ['class'=>'form-control searchAll', 'id'=>'searchAll']) !!}
                            </div>
                            <div class="form-group ui-front">
                                {!! Form::label('Stadium (only if not "just at the HS")') !!}
                                <input id="stadiumSearch" class="form-control input-lg" type="text"
                                       title="Stadium Finder"/>
                                <p id="help-block" class="help-block">3 characters minimum. Results are limited to
                                    10.</p>
                            </div>
                            {!! Form::input('number', 'stadiumid', '', ['class'=>'stadiumid d-none', 'id'=>'stadiumid']) !!}
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i> Close
                        </button>
                        <button type="button" class="btn btn-success" id="editGameButton"><i
                                    class="fa fa-check"></i> Edit
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id="editTeamInfoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editTeamInfoModalLabel">Edit Team Information</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form" id="editTeamInfoForm">
                        <div class="modal-body">
                            <div class="d-none">{!! Form::text('id', $team->teamid) !!}</div>
                            <div class="form-group">@
                                {!! Form::label('Twiter') !!}
                                {!! Form::text('twitter', $season->team->twitter ?? '', ['class'=>'form-control', 'placeholder'=>'twitter handle']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Hudl') !!}
                                {!! Form::text('hudl', $season->team->hudl ?? '', ['class'=>'form-control', 'placeholder'=>'hudl ID#']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Facebook: (omit http:// and https:// and www and facebook.com/') !!}
                                {!! Form::text('facebook', $season->team->facebook ?? '', ['class'=>'form-control', 'placeholder'=>'Facebook Page']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Facebook ID#') !!}
                                {!! Form::text('facebookid', $season->team->facebook_id ?? '', ['class'=>'form-control', 'placeholder'=>'Facebook ID #']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Website: (omit http(s)://') !!}
                                {!! Form::text('website', $season->team->website ?? '', ['class'=>'form-control', 'placeholder'=>'website']) !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                        class="fa fa-times"></i> Close
                            </button>
                            <button type="button" class="btn btn-primary" id="editTeamInfoButton"><i
                                        class="fa fa-check"></i> Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="editTeamModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editTeamModalLabel">Edit School Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form" id="editTeamForm">
                        <div class="modal-body">
                            <div class="d-none">{!! Form::text('id', $team->teamid) !!}</div>
                            <div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', $team->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', $team->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', $team->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', $team->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', $team->name, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                        class="fa fa-times"></i> Close
                            </button>
                            <button type="button" class="btn btn-primary" id="editTeamInfoButton"><i
                                        class="fa fa-check"></i> Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="editSeasonModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editSeasonModalLabel">Edit Season (Conference, Level, etc.)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form" id="editSeasonForm">
                        <div class="modal-body">
                            <div class="d-none">
                                {!! Form::text('id', $team->teamid) !!}
                                {!! Form::text('year', $season->year) !!}
                            </div>
                            {{--<div class="form-group">--}}
                            {{--{!! Form::label('Conference / League') !!} Not working--}}
                            {{--{!! Form::text('conference', is_object($season->getConference()) ? $season->getConference()->getConfid() : '', ['class'=>'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--{!! Form::label('Division') !!} Not working--}}
                            {{--{!! Form::text('division', is_object($season->getConference()) ? $season->getConference()->getConfid() : '', ['class'=>'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--{!! Form::label('District') !!} Not working--}}
                            {{--{!! Form::text('district', is_object($season->getConference()) ? $season->getConference()->getConfid() : '', ['class'=>'form-control']) !!}--}}
                            {{--</div>--}}
                            <div class="form-group">
                                {!! Form::label('Level (Colorado)') !!}
                                {!! Form::text('level', $season->levelid ?: '', ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Level (Global)') !!}
                                {!! Form::text('levelState', $season->levelid_state ?: '', ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Play UP') !!}
                                {!! Form::checkbox('playup', 1, $season->isPlayup(), ['class'=>'form-control playup']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Play down') !!}
                                {!! Form::checkbox('playdown', -1, $season->isPlaydown(), ['class'=>'form-control playdown']) !!}
                            </div>
                            {{--<div class="form-group">--}}
                            {{--{!! Form::label('Enrollment') !!}--}}
                            {{--{!! Form::text('enrollment', $season->getEnrollment(), ['class'=>'form-control']) !!}--}}
                            {{--</div>--}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                        class="fa fa-times"></i> Close
                            </button>
                            <button type="button" class="btn btn-primary" id="editSeasonButton"><i
                                        class="fa fa-check"></i> Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@stop
