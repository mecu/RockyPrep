<script>
$("#editform").delegate('.away', 'change', function ()
{
    var sum = 0;
    $("#editform").find(".away").each(function ()
    {
        var value = Number($(this).val());
        if (!isNaN(value)) sum += value;
    });

    $("#awaytotal").val(sum);
    $("#awaynotice").removeClass("ui-icon-info");
});
$("#editform").delegate('.home', 'change', function ()
{
    var sum = 0;
    $("#editform").find(".home").each(function ()
    {
        var value = Number($(this).val());
        if (!isNaN(value)) sum += value;
    });

    $("#hometotal").val(sum);
    $("#homenotice").removeClass("ui-icon-info");
});
$("#editform").delegate('#hometotal', 'change', function ()
{
    if ($("hometotal") > $("#awaytotal")) $("winner").val($("#hometeam").val());
    else if ($("hometotal") < $("#awaytotal")) $("winner").val($("#awayteam").val());
    else if ($("hometotal") == $("#awaytotal")) $("winner").val(0);
});
$("#editform").delegate('#awaytotal', 'change', function ()
{
    if ($("hometotal") > $("#awaytotal")) $("winner").val($("#hometeam").val());
    else if ($("hometotal") < $("#awaytotal")) $("winner").val($("#awayteam").val());
    else $("winner").val(0);
});
</script>
<form name="report_game" method="post" id="editform" action="report.php">
<input name="sportid" type="hidden" value="{{ $game->sportid }}" id="sportid" />
<input name="gameid" type="hidden" value="{{ $game->gameid }}" id="gameid" />
<input name="winner" type="hidden" value="0" id="winner" />
<input name="hometeam" type="hidden" value="{{ $game->hometeam }}" id="hometeam" />
<input name="awayteam" type="hidden" value="{{ $game->awayteam }}" id="awayteam" />

<table class="boxscore">
<tr><td colspan="10">Enter the box scores.<br />Put zero in a quarter if no points scored then.<br />If no OT played, then leave OT boxes blank.<br />More than 4 OT? We can't handle that yet.</td></tr>
<tr><th>Team</th><th>Q1</th><th>Q2</th><th>Q3</th><th>Q4</th><th>OT1</th><th>OT2</th><th>OT3</th><th>OT4</th><th>Total</th></tr>

<tr><td>{{ $game->away->name }}</td>
<td><input class="away" name="A1" tabindex="1" type="text" size="3"></td>
<td><input class="away" name="A2" tabindex="2" type="text" size="3"></td>
<td><input class="away" name="A3" tabindex="3" type="text" size="3"></td>
<td><input class="away" name="A4" tabindex="4" type="text" size="3"></td>
<td><input class="away" name="A5" type="text" size="3"></td>
<td><input class="away" name="A6" type="text" size="3"></td>
<td><input class="away" name="A7" type="text" size="3"></td>
<td><input class="away" name="A8" type="text" size="3"></td>
<td><span id="awaynotice" class="ui-icon ui-icon-info"></span><input id="awaytotal" name="awayscore" type="text" size="3"></td>

<tr><td>{{ $game->home->name }}</td>
<td><input class="home" name="H1" tabindex="6" type="text" size="3"></td>
<td><input class="home" name="H2" tabindex="7" type="text" size="3"></td>
<td><input class="home" name="H3" tabindex="8" type="text" size="3"></td>
<td><input class="home" name="H4" tabindex="9" type="text" size="3"></td>
<td><input class="home" name="H5" type="text" size="3"></td>
<td><input class="home" name="H6" type="text" size="3"></td>
<td><input class="home" name="H7" type="text" size="3"></td>
<td><input class="home" name="H8" type="text" size="3"></td>
<td><span id="homenotice" class="ui-icon ui-icon-info"></span><input id="hometotal" name="homescore" type="text" size="3"></td>

</table>
</form>
