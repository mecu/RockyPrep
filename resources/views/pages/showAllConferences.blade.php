@extends('layouts.master')

@section('title')
    {{ $title }} @parent
@stop

@section('sidebar')
<div class="pull-right" style="padding-top:100px">
{!! App\Helpers\Adblock::side() !!}
</div>
@stop

@section('content')

<h3>Select conference:</h3>

<ul id="teamList" class="list-unstyled">
    @foreach ( $conferences as $key => $value )
        <li>
            <a href="/{{ $state->urlname }}/conference/{{ $value->urlname }}/{{ $year->year }}">{{ $value->name }}</a>
        </li>
    @endforeach
</ul>
@stop
