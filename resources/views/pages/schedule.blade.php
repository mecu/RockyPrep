@extends('layouts.master-nosidebar')

@section('title')
    {{ $title }} @parent
@stop

@section('content')
    {!! App\Helpers\Adblock::top() !!}

    @include('partials.years', ['year' => $year, 'years' => $years, 'state' => $state, 'route' => 'schedule', 'params' => ['month' => $month, 'day' => $day]])

    @if ($nodates)
        <div class="alert alert-danger" style="font-size:2em"><strong>There are no games for this season and state.</strong></div>
        <div class="alert alert-info" style="font-size:2em"><strong>Please select another season above.</strong></div>
    @else
        <div id="datepicker"></div>

        <div class="clearfix"></div>

        <h3 class="text-center">Schedule &amp; Scores for {{ $date }}</h3>

        @if ($nogames)
            <div class="alert alert-danger" style="font-size:2em"><strong>There are no games scheduled for this date.</strong></div>
            <div class="alert alert-info" style="font-size:2em"><strong>Please select another date above.</strong></div>
        @else
            <table id="schedule" class="table table-hover table-condensed table-striped">
                <thead>
                <tr>
                    <th class="text-center">Class</th>
                    <th class="text-center">Away</th>
                    <th class="text-center">&nbsp;</th>
                    <th class="text-center no-sort">&nbsp;</th>
                    <th class="text-center">&nbsp;</th>
                    <th class="text-center">Home</th>
                    <th class="text-center">Location</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($games as $game)
                    <tr>
                        <td {{ $game->classificationSort($year) }} class="text-center">
                            @if ($game->awaySeason($year)->levelid )
                                {{ $game->awaySeason($year)->level->name }}
                            @endif
                            @if ($game->awaySeason($year)->levelid && $game->homeSeason($year)->levelid &&
                                $game->awaySeason($year)->levelid !== $game->homeSeason($year)->levelid )
                                / {{ $game->homeSeason($year)->level->name }}
                            @endif
                        </td>
                        <td class="text-center{!! $game->highlightWinner('away') !!}">
                            {{ link_to_route('team', $game->away->name, ['state' => $state->urlname, 'team' => $game->away->urlname, 'year' => $year->year]) }}
                            @if ($game->awaySeason($year)->isPlayUp()) <i class="fa fa-arrow-up"></i>
                            @elseif ($game->awaySeason($year)->isPlayDown()) <i class="fa fa-arrow-down"></i>
                            @endif
                            @if ($game->away->state !== null && $state->stateid !== $game->away->state->stateid) {{ $game->away->state->abbr }} @endif
                        </td>
                        <td class="text-center">
                            @if ($game->winner )
                                <span style="font-family:'Alfa Slab One'">{{ $game->awayscore }}</span>
                            @elseif ( $game->hasPredict() && $game->away->teamid === $game->srs_teamid )
                                <span class="label label-info">{{ $game->srs_predict }}</span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($game->neutral)
                                vs.
                            @else
                                @
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($game->winner )
                                <span style="font-family:'Alfa Slab One'">{{ $game->homescore }}</span>
                            @elseif ( $game->hasPredict() && $game->home->teamid === $game->srs_teamid )
                                <span class="label label-info">{{ $game->srs_predict }}</span>
                            @endif
                        </td>
                        <td class="text-center{!! $game->highlightWinner('home') !!}">
                            {{ link_to_route('team', $game->home->name, ['state' => $state->urlname, 'team' => $game->home->urlname, 'year' => $year->year]) }}
                            @if ($game->homeSeason($year)->isPlayUp()) <i class="fa fa-arrow-up"></i>
                            @elseif ($game->homeSeason($year)->isPlayDown()) <i class="fa fa-arrow-down"></i>
                            @endif
                            @if ($game->home->state !== null && $state->stateid !== $game->home->state->stateid) {{ $game->home->state->abbr }} @endif
                        </td>
                        <td class="text-center">
                            @if ( $game->stadium )
                                @if ($game->stadium->location)
                                    <a href="{{ $game->stadium->location }}" title="{{ $game->stadium->alt }}">
                                        {{ $game->stadium->name }}
                                    </a>
                                @else
                                    {{ $game->stadium->name }}
                                @endif
                            @else
                                &nbsp;
                            @endif
                            @if ( $game->playoff )
                                <i class="fa fa-certificate" title="Playoff"></i>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th></th>
                    <th colspan="6"></th>
                </tr>
                </tfoot>
            </table>
        @endif

        <div class="clearfix"></div>
        <h4>Legend:</h4>
        <dl class="row border border-info rounded">
            @if ( $state->stateid === 6 )
                <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
                <dd class="col-10">Playing up. See CHSAA for details.</dd>
                <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
                <dd class="col-10">Playing down. See CHSAA for details.</dd>
            @endif
            <dt class="col-2 text-right"><i class="fa fa-certificate"></i></dt>
            <dd class="col-10">Playoff Game</dd>
            <dt class="col-2 text-right"><span class="label label-info">##</span></dt>
            <dd class="col-10">SRS Prediction (The team is favored by that many points.)</dd>
        </dl>

        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>

        <script async>
            var datelist = ["{!! $validDates->implode('", "') !!}"];

            function dateToYMD(date) {
                var d = date.getUTCDate();
                var m = date.getUTCMonth() + 1;
                var y = date.getUTCFullYear();
                return "/" + y + "/" + m + "/" + d + "/";
            }

            $().ready(function () {
                $("#datepicker").datepicker({
                    numberOfMonths: 5,
                    dateFormat: "yy-mm-dd",
                    defaultDate: "{{ $today }}",
                    minDate: "{{ $minDay }}",
                    maxDate: "{{ $maxDay }}",
                    beforeShowDay: function (d) {
                        // normalize the date for searching in array
                        var ymd = d.getUTCFullYear() + "-" + ("00" + (d.getUTCMonth() + 1)).slice(-2) + "-" + ("00" + d.getUTCDate()).slice(-2);
                        if ($.inArray(ymd, datelist) >= 0) {
                            return [true, ""];
                        }
                        return [false, ""];
                    },
                    onSelect: function (day) {
                        window.location = "/{{ $state->urlname }}/schedule" + dateToYMD(new Date(day));
                        return true;
                        },
                    })
                    .css({"font-size": 12, "line-height": 1.2})
                    .disableSelection();

                $("#schedule").dataTable({
                    pagingType: "full_numbers",
                    order: [[0, "desc"]],
                    columnDefs: [{targets: "no-sort", orderable: false}]
                });
            });
        </script>

        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.0.0/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Alfa+Slab+One">
    @endif
    {!! App\Helpers\Adblock::bottom() !!}
@stop
