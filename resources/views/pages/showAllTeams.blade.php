@extends('layouts.master')

@section('title')
    {!! $year->year !!} {!! $state->name !!} Teams :: @parent
@stop

@section('sidebar')
    <div class="pull-right" style="padding-top:100px">
        {!! App\Helpers\Adblock::side() !!}
    </div>
@stop

@section('content')
    @include('partials.years', ['year' => $year, 'years' => $years, 'state' => $state, 'route' => 'allteams', 'params' => []])

    <h3>Select team to show:</h3>

    @spaceless
    <ul id="teamList" class="list-unstyled">
        @foreach ( $state->teams as $team )
            <li>
                <a href="{!! route('team', ['state' => $state->urlname, 'team' => $team->urlname, 'year' => $year->year]) !!}">
                    <button class="btn btn-primary btn-block mt-1">
                        {!! $team->name !!}
                    </button>
                </a>
            </li>
        @endforeach
    </ul>
    @endspaceless

    <script src="/js/jquery.listnav.min-2.3.js"></script>
    <script src="/js/teamList.js"></script>

    <link rel="stylesheet" href="/css/listnav.min.2.2.css">
@stop
