@extends('layouts.master')

@section('title')
    {{ $title }} @parent
@stop

@section('content')
{!! App\Helpers\Adblock::top() !!}

<h3>Coach {{ $coach->name }} {{ $sport->name }} Record:</h3>

<div class="table-responsive pull-left">
<table class="table table-hover table-condensed table-striped">
<thead>
<tr>
    <th><div class="text-center">Year</div></th>
    <th><div class="text-center">Team</div></th>
    <th><div class="text-center">Record</div></th>
    <th><div class="text-center">Conference<br />Record</div></th>
</tr>
</thead>
<tbody>
    @foreach ( $team as $year => $team )
<tr>
    <td><div class="text-center">{{ $year }}</div></td>
    <td><div class="text-center"><a href="{{ $team['url'] }}">{{ $team['name'] }}</a></div></td>
    <td><div class="text-center">{{ $team['wins'] }}&ndash;{{ $team['loss'] }}</div></td>
    <td><div class="text-center">{{ $team['cwins'] }}&ndash;{{ $team['closs'] }}</div></td>
</tr>
    @endforeach
</tbody>
<tfoot>
<tr>
    <td colspan="2">Total</td>
    <td><div class="text-center">{{ $total['wins'] }}&ndash;{{ $total['loss'] }} ({{ $total['avg'] }})</div></td>
    <td><div class="text-center">{{ $total['cwins'] }}&ndash;{{ $total['closs'] }} ({{ $total['cavg'] }})</div></td>
</tr>
</tfoot>
</table>
</div>

@if ( $state->id === 6 )
<div class="clearfix"></div>
<h4>Legend:</h4>
<dl class="row border border-info rounded">
        <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
        <dd class="col-10">Playing up. See CHSAA for details.</dd>
        <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
        <dd class="col-10">Playing down. See CHSAA for details.</dd>
</dl>
@endif

{!! App\Helpers\Adblock::bottom() !!}
@stop
