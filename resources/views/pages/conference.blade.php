@extends('layouts.master')

@section('title')
    {{ $title }} @parent
@stop

@section('sidebar')
<div class="pull-right" style="padding-top:100px">
{!! App\Helpers\Adblock::side() !!}
</div>
@stop

@section('content')
{!! App\Helpers\Adblock::top() !!}

@include('partials.years', ['year' => $year, 'years' => $years, 'state' => $state, 'route' => 'conference', 'params' => ['conference' => $conference]])

<h3>{{ $year->year }} {{ $state->name }} {{ $conference->name }} Conference Rankings:</h3>
@if (isset($nodata))
    <h2>No data yet ...</h2>
    <h3>Likely teams have not yet been loaded into conferences yet...</h3>
@else
    <div class="table-responsive">
    <table class="table table-hover table-condensed table-striped">
    <thead>
    <tr>
        <th><div class="text-center">Rank</div></th>
        <th><div class="text-center">Team</div></th>
        <th><div class="text-center">Record</div></th>
        <th><div class="text-center">Conference<br />Record</div></th>
    </tr>
    </thead>
    <tbody>
        @foreach ( $ranking as $key => $team )
    <tr>
        <td><div class="text-center">{{ $key }}</div></td>
        <td><div class="text-center"><a href="{{ $team['url'] }}">{{ $team['name'] }}</a></div></td>
        <td><div class="text-center">{{ $team['wins'] }}&ndash;{{ $team['loss'] }}</div></td>
        <td><div class="text-center">{{ $team['cwins'] }}&ndash;{{ $team['closs'] }}</div></td>
    </tr>
        @endforeach
    </tbody>
    </table>
    </div>
    
    @if ( $state->id === 6 )
    <div class="clearfix"></div>
    <h4>Legend:</h4>
    <dl class="row border border-info rounded">
        <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
        <dd class="col-10">Playing up. See CHSAA for details.</dd>
        <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
        <dd class="col-10">Playing down. See CHSAA for details.</dd>
    </dl>
    @endif
@endif

{!! App\Helpers\Adblock::bottom() !!}
@stop
