@extends('layouts.master-nosidebar')

@section('title')
    All States ::  @parent
@stop

@section('sidebar')
@stop

@section('content')
    <div>{!! App\Helpers\Adblock::top() !!}</div>
    <h3>All States:</h3>
    <div>
        @foreach ( $states as $stateid => $state )
            <a href="{{ URL::route('allteams', ['state'=>$state->urlname, 'year'=>$year->year ] ) }}"><button class="btn btn-primary btn-lg">{{ $state->name}}</button></a>
        @endforeach
    </div>
@stop
