@extends('layouts.master')

@section('sidebar')
<h2>{{ $state->name }} Football Content Available:</h2>
<ul class="pull-left">
    @if ( $state->id === 6 )
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/map/{{ $year->year }}">Map</a></li>
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/coach">Coaches</a></li>
    @endif
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/srs/{{ $year->year }}">SRS</a></li>
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/rpi/{{ $year->year }}">RPI</a></li>
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/standings">Standings</a></li>
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/schedule/{{ $year->year }}/{{ date("m/d") }}">Schedule</a></li>
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/team/{{ $year->year }}">Teams</a></li>
    <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/conference">Conference</a></li>
    @if ( $standings_exist )
    <li>Standings:<ul>
        @foreach ( $standings as $levelid => $level )
            <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/standings/{{ $level->urlname }}/{{ $year->year }}">{{ $level->name }}</a></li>
        @endforeach
        </ul>
    @endif
</ul>
@stop

@section('content')
<div>{!! App\Helpers\Adblock::top() !!}</div>
@foreach ( $articles as $key => $content )
<div class="article">
    <h2><a href="/pages/posts/{{ $content->get("name") }}">{{ $content->get("title") }}</a></h2>
    <h3>{{ $content->get("subtitle") }}</h3>
    <p class="datetime">{{ date("F j, Y", $content->get("created")) }}</p>
    <p>{{ $content->get("body") }}</p>
</div>
<hr>
@endforeach

@stop
