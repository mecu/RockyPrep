<script src="/js/teamSearch.js"></script>
<script src="/js/stadiumSearch.js"></script>
<form class="form-horizontal" role="form" id="editgame">
{{ Form::hidden('sportid', $sport->sportid) }}
{{ Form::hidden('stateid', $team->stateid) }}
{{ Form::hidden('hometeam', $team->teamid) }}
{{ Form::hidden('gameid', $game->gameid) }}
{{ Form::hidden('year', $year->year) }}
{{ Form::hidden('awayteam', $opponent->teamid) }}

<div class="form-group">
    <label>Date</label>
    <input type="date" class="form-control" value="{{ $game->date }}" name="date" required="required" min="{{ $year->start }}" max="{{ $year->end }}">
</div>
<div class="form-group">
    <label>Time</label>
    <input type="time" class="form-control" value="{{ $game->time }}" name="time" required="required">
</div>

<div class="form-group ui-widget">
    <label for="opponent">Opponent</label>
    <input type="text" class="form-control" name="opponent" title="Opponent Finder" id="teamSearch" required="required" value="{{ $opponent->name }}">
    <p id="help-block" class="help-block">3 characters minimum. Results are limited to 10.</p>
    <label><input type="checkbox" name="allstates" id="allstates" value="1"> Search All States</label>
    <button class="btn-success hidden" id="{{ $team->stateid }}"></button>
    <input class="hidden" type="text" name="awayteam" id="awayteam" required="required" value="{{ $opponent->teamid }}">
</div>

<div class="form-group ui-widget">
    <label for="stadium">Stadium</label>
    <input type="text" class="form-control" name="stadium" title="Stadium Finder" id="stadiumSearch" required="required" 
    @if ( $game->stadiumid ) value="{{ $game->stadium->name }}" @endif 
    >
    <p id="help-block" class="help-block">3 characters minimum. Results are limited to 10.<br />If at the school, leave blank.</p>
    <label><input type="checkbox" name="stadiumallstates" id="stadiumallstates" value="1"> Search All States</label>
    <button class="btn-success hidden" id="{{ $team->stateid }}"></button>
    <input class="hidden" type="text" name="stadiumid" id="stadiumid" required="required"
    @if ( $game->stadiumid ) value="{{ $game->stadiumid }}" @endif
    >
</div>

</div>
<div class="row">
    <div class="col-xs-6">
        <div class="form-group">
            <label>
                <input type="checkbox" name="swap" value="1"
                @if ( $game->isAway($team) ) checked="checked" @endif
                > Away Game
            </label>
        </div>
        
        <div class="form-group">
            <label>
                <input type="checkbox" name="conference" value="1"
                @if ( $game->confid ) checked="checked" @endif
                > Conference Game
            </label>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label>
                <input type="checkbox" name="neutral" value="1"
                @if ( $game->neutral ) checked="checked" @endif
                > Neutral Game
            </label>
        </div>
        
        <div class="form-group">
            <label>
                <input type="checkbox" name="playoff" value="1"
                @if ( $game->playoff ) checked="checked" @endif
                > Playoff Game
            </label>
        </div>
    </div>
</div>
</form>
