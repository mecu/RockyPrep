@extends('layouts.master')

@section('title')
    {{ $title }} @parent
@stop

@section('sidebar')
    <div class="pull-left" style="padding-top:100px">
        {!! App\Helpers\Adblock::side() !!}
    </div>
@stop

@section('content')
    {!! App\Helpers\Adblock::top() !!}

    @include('partials.years', ['year' => $year, 'years' => $years, 'state' => $state, 'route' => 'standings', 'params' => ['level' => $level->urlname]])

    <nav class="navbar navbar-expand-md">
        <a class="navbar-brand" href="#">Levels</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#levels-navs">
            <i class="fa fa-navicon"></i>
        </button>
        <div class="navbar-collapse collapse" id="levels-navs">
            <div class="navbar-nav">
                @foreach ( $menuLevels as $menuLevel )
                    @if ( $menuLevel->levelid === $level->levelid )
                        <a class="nav-item nav-link active"
                    @else
                        <a class="nav-item nav-link"
                   @endif
                           href="{!! route('standings', ['state' => $state->urlname, 'level' => $menuLevel->urlname, 'year' => $year->year]) !!}">
                            {!! $menuLevel->name !!}
                        </a>
                @endforeach
            </div>
        </div>
    </nav>

    @if ( $state->stateid == 6)
        <h4><span class="badge badge-info">This is based on the RPRPI, not the CHSAA RPI, though the results are similar.</span></h4>
    @endif

    <h3 class="text-center">{{ $year->year }} {{ $state->name }} Football {{ $level->name }} Standings</h3>
    <h4><span class="badge badge-pill badge-dark pull-right">Generated: {{ $time }}</span></h4>
    <div class="table-responsive pull-left">
        <table class="table table-hover table-condensed table-striped">
            <thead>
            <tr>
                <th class="text-center" colspan="{!! $coloradoFootball ? 5 : 7 !!}">Combined Standings</th>
            </tr>
            <tr>
                <th class="text-center">Rank</th>
                <th class="text-center">Team</th>
                <th class="text-center">Record</th>
                @if ( $coloradoFootball )
                    <th class="text-center">Points</th>
                    <th class="text-center">Tiebreak Points</th>
                @else
                    <th class="text-center">RPRPI</th>
                    <th class="text-center">WP</th>
                    <th class="text-center">OWP</th>
                    <th class="text-center">OOWP</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach ( $ranking as $key => $team )
                <tr>
                    <td class="text-center">{{ $key }}</td>
                    <td class="text-center">
                            <a href="{{ URL::action('TeamController@team', [$state->urlname, $team['team']->urlname, $year->year]) }}">{{ $team['name'] }}</a>
                            @if ($team['playUp']) <i class="fa fa-arrow-up"></i>
                            @elseif ($team['playDown']) <i class="fa fa-arrow-down"></i>
                            @endif
                    </td>
                    <td class="text-center">{{ $team['wins'] }}&ndash;{{ $team['loss'] }}</td>
                    @if ( $coloradoFootball )
                        <td class="text-center">{{ $team['points'] }}</td>
                        <td class="text-center">{{ $team['tiebreak'] }}</td>
                    @else
                        <td class="text-center">{!! sprintf('%0.4f', $team['rpi']) !!}</td>
                        <td class="text-center">{!! sprintf('%0.4f', $team['season']->rpi_wp) !!}</td>
                        <td class="text-center">{!! sprintf('%0.4f', $team['season']->rpi_owp) !!}</td>
                        <td class="text-center">{!! sprintf('%0.4f', $team['season']->rpi_oowp) !!}</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @if ( $state->id === 6 )
        <div class="clearfix"></div>
        <h4>Legend:</h4>
        <dl class="row border border-info rounded">
            <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
            <dd class="col-10">Playing up. See CHSAA for details.</dd>
            <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
            <dd class="col-10">Playing down. See CHSAA for details.</dd>
        </dl>
    @endif

    {!! App\Helpers\Adblock::bottom() !!}
@stop
