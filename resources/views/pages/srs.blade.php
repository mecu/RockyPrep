@extends('layouts.master-nosidebar')

@section('title')
    {{ $year->year}} {{$state->name}} Football Simple Ranking System (SRS) :: @parent
@stop

@section('content')
{!! App\Helpers\Adblock::top() !!}

<h2 class="text-center">SRS for {{ $year->year }} {{ $state->name }} Football</h2>

@include('partials.years', ['year' => $year, 'years' => \App\Helpers\Navigation::menuYearsSchedule($state), 'state' => $state, 'route' => 'srs', 'params' => []])

<h3><a href="{{ route('srsPage') }}"><i class="fa fa-question-circle"></i> What is the SRS?</a></h3>

<div class="table-responsive">
<table id="srs" class="table table-hover table-condensed">
<thead>
<tr>
    <th class="text-center">Team</th>
    <th class="text-center">Class</th>
    <th class="text-center">SRS</th>
    <th class="text-center">Average<br>Point Differential</th>
    <th class="text-center">Opponent's Average<br>Point Differential</th>
    <th class="text-center">Number<br>Games</th>
</tr>
</thead>
<tbody>
    @foreach ( $ranking as $teamSeason )
<tr>
    <td class="text-center">{!! link_to_route('team', $teamSeason->team->name , [$state->urlname, $teamSeason->team->urlname, $year->year]) !!}</td>
    <td class="text-center">{{ $teamSeason->level ? $teamSeason->level->name : '&nbsp;' }}</td>
    <td class="text-center">{{ $teamSeason->srs }}</td>
    <td class="text-center">{{ $teamSeason->srs_avgpd }}</td>
    <td class="text-center">{{ $teamSeason->srs_oppavg }}</td>
    <td class="text-center">{{ $teamSeason->srs_numgames }}</td>
</tr>
    @endforeach
</tbody>
<tfoot>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</tfoot>
</table>
</div>

<script>
    $().ready(function () {
        $("#srs").dataTable({
            bJQueryUI: true,
            bAutoWidth: true,
            sPaginationType: "full_numbers",
            iDisplayLength: 10,
            aaSorting: [[ 2, "desc" ]]
        }).columnFilter({
            aoColumns: [null, {
                type: "select",
                values: ["{!! implode('","', $levels->toArray())  !!}"]
            }, null, null, null, null]
        });
    });
</script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="/js/jquery.dataTables.columnFilter.js"></script>
<script async src="//cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css" property="stylesheet">

{!! App\Helpers\Adblock::bottom() !!}

@if ( $state->stateid === 6 )
    <div class="clearfix"></div>
    <h4>Legend:</h4>
    <dl class="row border border-info rounded">
        <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
        <dd class="col-10">Playing up. See CHSAA for details.</dd>
        <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
        <dd class="col-10">Playing down. See CHSAA for details.</dd>
    </dl>
@endif

@stop
