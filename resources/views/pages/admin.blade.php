@extends('layouts.master')

@section('sidebar')
    <h2>Admin functions</h2>
    <ul>
        @foreach ( $option as $link )
            <li><a href="/{{ $link['link'] }}">{{ $link['text'] }}</a></li>
        @endforeach
    </ul>
@stop

@section('content')
    <div class="col-sm-12">
        @if ($wildcard)
            {{ Form::open(['class' => 'form-horizontal', 'url' => $state->urlname . '/admin/wildcard']) }}
            {{ Form::label('levelid', 'Class') }}
            {{ Form::select('levelid', [$levels->levelid => $levels->name]) }}
            {{ Form::label('levelid2', 'Opponent Class') }}
            {{ Form::select('levelid2', [$levels->levelid => $levels->name]) }}
            {{ Form::text('year') }}

            <div class="col-sm-2"></div>
            <div class="col-sm-5">Win</div>
            <div class="col-sm-5">Lose</div>

            @for ($i = 10; $i > 0; --$i)
                <div class="form-group">
                    <div class="control-label col-sm-2">{{ $i }} wins:</div>
                    <div class="col-sm-5">{{ Form::text('V') }}</div>
                    <div class="col-sm-5">{{ Form::text('JV')}}</div>
                </div>
            @endfor
            {{ Form::token() }}
            {{ Form::close() }}
        @endif

        @if ($classify)
            {{ Form::open(['class' => 'form-horizontal', 'url' => $state->urlname . '/admin/class']) }}
            {{ Form::label('levelid', 'Class') }}
            {{ Form::select('levelid', [$levels->levelid => $levels->name]) }}
            {{ Form::text('year') }}
            @foreach ($seasons as $season)
                <div class="form-group">
                    <div class="control-label col-sm-2">{{ $season['name'] }}:</div>
                    <div class="col-sm-10">{{ Form::select($season['teamid'], $levels, $season['levelid']) }}</div>
                </div>
            @endforeach
            {{ Form::token() }}
            {{ Form::submit('Submit') }}
            {{ Form::close() }}
        @endif
    </div>
@stop
