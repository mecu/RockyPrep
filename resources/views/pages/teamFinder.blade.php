@extends('layouts.master')

@section('title')
    Team Finder :: @parent
@stop

@section('content')
<script src="/js/teamSearch.js"></script>
<h2 class="text-center">Team Search</h2>

<div class="ui-widget">
    <label for="teamSearch" class="sr-only">Search: </label>
    <input id="teamSearch" class="form-control input-lg" type="text" title="Team Finder" />
    <p id="help-block" class="help-block">3 characters minimum. Results are limited to 10.</p>
    <div id="teamSearch-buttonlinks"></div>
</div>

<h4>Optionally search a specific state:</h4>
<div class="unselectable" unselectable="yes">
    @foreach ($states as $state)
        <button class="btn btn-primary btn-lg stateselect" id="{!! $state->stateid !!}">{!! $state->name !!}</button>
    @endforeach
</div>
@stop
