@extends('layouts.master')

@section('sidebar')
    <div class="list-group">
        @foreach ( $states as $state )
            <a href="{!! URL::route('allteams', ['state'=>$state->urlname, 'year'=>\App\Helpers\Helpers::currentYear() ] ) !!}"
                class="list-group-item list-group-item-primary list-group-item-action">
                {!! $state->name !!}
            </a>
        @endforeach
        @php
            // Do this so there isn't a conflict with the nav bar
            unset($state)
        @endphp
    </div>
@stop

@section('content')
    <div>{!! App\Helpers\Adblock::top() !!}</div>
    @if(count($articles) > 0)
        <div class="article">
            <h2>News</h2>
            @foreach ($articles as $post)
                <div class="row">
                    <div class="col">
                        <h4>
                            <strong><a href="{!! URL::to('news/'.$post->id.'') !!}">{!! $post->title !!}</a></strong>
                        </h4>
                        <span><i class="fa fa-calendar"></i> {!! $post->created_at->format('F j, Y') !!}</span>
                        <p>{!! $post->introduction !!}</p>
                    </div>
                </div>
                @if (!$loop->last)<hr>@endif
            @endforeach
        </div>
    @endif
@endsection
