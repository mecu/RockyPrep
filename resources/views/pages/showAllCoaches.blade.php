@extends('layouts.master')

@section('title')
    {{ $title }} @parent
@stop

@section('sidebar')
<div class="pull-right" style="padding-top:100px">
{!! App\Helpers\Adblock::side() !!}
</div>
@stop

@section('content')
<h3>Select Coach:</h3>

<ul id="teamList" class="list-unstyled">
    @foreach ( $coaches as $name => $coach )
        <li><a href="/{{ $sport->urlname }}/{{ $state->urlname }}/coach/{{ $coach['urlname'] }}">{{ $name }} ({{ $coach['teams'] }})</a></li>
    @endforeach
</ul>

<script src="/js/jquery.listnav.min-2.3.js"></script>
<script src="/js/teamList.js"></script>

<link rel="stylesheet" href="/css/listnav.min.2.2.css">
@stop
