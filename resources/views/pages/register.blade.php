@extends('layouts.master-nosidebar')

@section('content')
<div class="col-xs-3"></div>
<div class="col-xs-6">
<div class="card">
  <div class="card-heading">
    <h3 class="card-title">Why should I register for an account?</h3>
  </div>
  <div class="card-body">
    Primarily, registering for an account is required and the first step to be allowed to edit. However, registering also allows you to set some preferences to allow you to get to your desired content more quickly. Registering is free.
  </div>
</div>

<form role="form" id="register" action="/register" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email" required>
  </div>
  
  <div class="form-group">
    <label for="name">Name / Alias</label>
    <input type="text" class="form-control" id="name" placeholder="Visible Name" name="name" required>
  </div>
  
  <div class="form-group">
    <label for="firstname">First Name</label>
    <input type="text" class="form-control" id="firstname" placeholder="First Name" name="firstname" required>
  </div>
  
  <div class="form-group">
    <label for="lastname">Last Name</label>
    <input type="text" class="form-control" id="lastname" placeholder="Last Name" name="lastname" required>
  </div>
  
  <div class="form-group">
    <label for="exampleInputPassword2">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
  </div>
  
  <div class="form-group">
    <label for="exampleInputPassword2">Repeat Password</label>
    <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Repeat Password" name="confirmPassword" required>
  </div>
  
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" placeholder="Title" name="title">
    <span class="help-block">Example: Coach, Athletic Director, Parent, Fan, Student-Athlete, etc.</span>
  </div>
  
  <div class="form-group">
    <label for="location">Location</label>
    <input type="text" class="form-control" id="location" placeholder="Location" name="location">
  </div>
  
  <div class="form-group">
    <label for="timezone">Time Zone</label>
    <select class="form-control" name="timezone">
        <option value="America/New_York">Eastern</option>
        <option value="America/Chicago">Central</option>
        <option value="America/Denver">Mountain</option>
        <option value="America/Phoenix">Mountain (No DST)</option>
        <option value="America/Los_Angeles">Pacific</option>
        <option value="America/Anchorage">Alaska</option>
        <option value="America/Adak">Hawaii</option>
        <option value="America/Honolulu">Hawaii (No DST)</option>
    </select>
  </div>
  
  <div class="form-group">
    <label for="state">Favorite State</label>
    <select class="form-control" name="user_favorite_state" id="state">
            <option value="">Select</option>
        @foreach ($states as $key => $state)
            <option value="{{$state->stateid}}">{{$state->name}}</option>
        @endforeach
    </select>
  </div>
  
  <div class="form-group">
    <label for="sport">Favorite Sport</label>
    <select class="form-control" name="user_favorite_sport" id="sport">
            <option value="">Select</option>
        @foreach ($sports as $key => $sport)
            <option value="{{$sport->sportid}}">{{$sport->name}}</option>
        @endforeach
    </select>
  </div>
  
  <div class="form-group">
    <label for="team">Favorite Team</label>
    <input type="text" class="form-control" id="team" placeholder="3 chars minimum" name="user_favorite_team" disabled>
    </select>
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>
</div>

<div class="col-xs-3"></div>
@stop
