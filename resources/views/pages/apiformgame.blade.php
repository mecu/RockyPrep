<script src="/js/teamSearch.js"></script>
<script src="/js/stadiumSearch.js"></script>
<form class="form-horizontal" role="form" id="addgame">
{{ Form::hidden('sportid', $sport->sportid) }}
{{ Form::hidden('stateid', $team->stateid) }}
{{ Form::hidden('hometeam', $team->teamid) }}
{{ Form::hidden('year', $year->year) }}


<div class="form-group">
    <label>Date</label>
    <input type="date" class="form-control" value="{{ date("Y-m-d") }}" name="date" required="required" min="{{ $year->start }}" max="{{ $year->end }}">
</div>
<div class="form-group">
    <label>Time</label>
    <input type="time" class="form-control" value="19:00" name="time" required="required">
</div>

<div class="form-group ui-widget">
    <label for="opponent">Opponent</label>
    <input type="text" class="form-control" name="opponent" title="Opponent Finder" id="teamSearch" required="required">
    <p id="help-block" class="help-block">3 characters minimum. Results are limited to 10.</p>
    <label><input type="checkbox" name="allstates" id="allstates" value="1"> Search All States</label>
    <button class="btn-success hidden" id="{{ $team->stateid }}"></button>
    <input class="hidden" type="text" name="awayteam" id="awayteam" required="required">
</div>

<div class="form-group ui-widget">
    <label for="stadium">Stadium</label>
    <input type="text" class="form-control" name="stadium" title="Stadium Finder" id="stadiumSearch" required="required">
    <p id="help-block" class="help-block">3 characters minimum. Results are limited to 10. If at the school, leave blank.</p>
    <label><input type="checkbox" name="stadiumallstates" id="stadiumallstates" value="1"> Search All States</label>
    <button class="btn-success hidden" id="{{ $team->stateid }}"></button>
    <input class="hidden" type="text" name="stadiumid" id="stadiumid" required="required">
</div>

</div>
<div class="row">
    <div class="col-xs-6">
        <div class="form-group">
            <label>
                <input type="checkbox" name="away" value="1"> Away Game
            </label>
        </div>
        
        <div class="form-group">
            <label>
                <input type="checkbox" name="conference" value="1"> Conference Game
            </label>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label>
                <input type="checkbox" name="neutral" value="1"> Neutral Game
            </label>
        </div>
        
        <div class="form-group">
            <label>
                <input type="checkbox" name="playoff" value="1"> Playoff Game
            </label>
        </div>
    </div>
</div>
</form>
