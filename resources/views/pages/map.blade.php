@extends('layouts.master-nosidebar')

@section('title')
    {{ $year->year }} {{ $state->name }} Football Map :: @parent
@stop

@section('content')
    @include('partials.years', ['year' => $year, 'years' => $years, 'state' => $state, 'route' => 'map', 'params' => []])

    @if($noTeams)
        <div>
            <h1>Sorry</h1>
            <p>I don't have any teams to display for you. It requires inputting the latitude and longitude of each team,
                which takes a lot of work. If you'd like to help, please {{ link_to_route('contact-us', 'contact us') }}
                .</p>
            <p>You can see what the map looks like on
                the {{ link_to_route('map', 'Colorado Map', ['state' => 'colorado', 'year' => $year->year]) }}.</p>
        </div>
    @else
        <script>
            var teams = [
                @foreach($teams as $team){!! json_encode([$team->name, $team->lat, $team->lng, $team->levelid_state]) !!}@if ($teams->last() !== $team),@endif @endforeach
            ];
            var stateCenterLat = {{ $state->lat }};
            var stateCenterLng = {{ $state->lng }};
            var imageColor = [];
            var markers = [];
            @foreach($teams->pluck('levelState')->unique() as $key => $levelState)
                    markers[{{ $levelState->levelid }}] = [];
            imageColor[{{ $levelState->levelid }}] = '{{ $levelState->image }}';
            @endforeach
        </script>
        {!! App\Helpers\Adblock::top() !!}

        <h1>{{ $year->year }} {{ $state->name }} Football Map</h1>

        <div id="map_legend">
            <ul class="list-inline list-unstyled">
                @foreach ( $teams->pluck('levelState')->unique() as $key => $levelState )
                    <li>
                        <button type="button" class="btn btn-default" id="{{ $levelState->leelid }}"
                                onclick="Markers('{{ $levelState->levelid }}')">
                            {{ $levelState->name }} {!! HTML::image("/img/marker/marker_".$levelState->image.".png") !!}
                        </button>
                    </li>
                @endforeach
            </ul>
        </div>

        <div id="map-canvas" style="height:900px;margin-top:10px"></div>

        <script async src="//maps.googleapis.com/maps/api/js?key=AIzaSyAQi1C3DEkLgQbeKTnurJNvdwBhtCFgnI8"></script>
        <script async src="/js/map.js"></script>
    @endif
@stop
