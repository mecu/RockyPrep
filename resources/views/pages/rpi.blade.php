@extends('layouts.master-nosidebar')

@section('title')
    {{ $year->year}} {{$state->name}} Football Ratings Percentile Index (RPI) :: @parent
@stop

@section('content')
    {!! App\Helpers\Adblock::top() !!}

    <h2 class="text-center">RPI for {!! $year->year !!} {!! $state->name !!} Football</h2>

    @include('partials.years', ['year' => $year, 'years' => \App\Helpers\Navigation::menuYearsSchedule($state), 'state' => $state, 'route' => 'rpi', 'params' => []])

    <h3><a href="{{ route('rpiPage') }}"><i class="fa fa-question-circle"></i> What is the RPI?</a></h3>
    <h4>This is not the CHSAA RPI. This is the RPRPI RPI.</h4>
    <h4>Only teams that have played games in this season will show in this list.</h4>

    <table id="rpi" class="table table-hover table-condensed table-striped">
        <thead>
        <tr>
            <th class="text-center">Team</th>
            <th class="text-center">Class</th>
            <th id="standard-RPI" class="text-center">RPI</th>
            <th title="Winning Percentage" class="text-center">WP</th>
            <th title="Opponent's Winning Percentage" id="standard-RPI" class="text-center">OWP</th>
            <th title="Opponent's Opponent's Winning Percentage" id="standard-RPI" class="text-center">OOWP</th>
        </tr>
        </thead>
        <tbody>
        @foreach ( $ranking as $key => $season )
            <tr>
                <td class="text-center">
                    <a href="{{ URL::action('TeamController@team', [$state->urlname, $season->team->urlname, $year->year]) }}">
                        {{ $season->team->name }}
                    </a>
                    <a class="excel" title="Excel File showing RPI calculations for {{ $season->team->name }}"
                       href="{{ URL::action('TeamController@rpi', [$state->urlname, $season->team->urlname, $year->year]) }}">
                        <i class="fa fa-file-excel-o"></i>
                    </a>
                    {{--
                    @if ( $season->team->stateid == 6 )
                    <a class="excel" title="Excel File showing RPRPI calculations for {{ $season->team->urlname }}" href="{{ URL::action('TeamController@rprpi', [$state->urlname, $season->team->urlname, $year->year]) }}"><i class="fa fa-file-excel-o"></i></a>
                    @endif
                    --}}
                </td>
                <td class="text-center">{{ $season->level ? $season->level->name : '&nbsp;' }}</td>
                <td class="text-center">{{ $season->rpi }}</td>
                <td class="text-center">{{ $season->rpi_wp }}</td>
                <td class="text-center">{{ $season->rpi_owp }}</td>
                <td class="text-center">{{ $season->rpi_oowp }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        </tfoot>
    </table>

    <script>
        $().ready(function () {
            $("#rpi").dataTable({
                bJQueryUI: true,
                bAutoWidth: true,
                sPaginationType: "full_numbers",
                iDisplayLength: 10,
                aaSorting: [[2, "desc"]]
            }).columnFilter({
                aoColumns: [null, {
                    type: "select",
                    values: ["{!! implode('","', $levels->toArray())  !!}"]
                }, null, null, null, null]
            });
            $("#standard-RPI").qtip({content: "<img src='/img/standard-rpi.gif' height='38' width='223' style='padding:0;' />"});
            $('.excel').qtip();
        });
    </script>

    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="/js/jquery.dataTables.columnFilter.js"></script>
    <script async src="//cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css" property="stylesheet">

    {!! App\Helpers\Adblock::bottom() !!}

    @if ( $state->stateid === 6 )
        <div class="clearfix"></div>
        <h4>Legend:</h4>
        <dl class="row border border-info rounded">
            <dt class="col-2 text-right"><i class="fa fa-arrow-up"></i></dt>
            <dd class="col-10">Playing up. See CHSAA for details.</dd>
            <dt class="col-2 text-right"><i class="fa fa-arrow-down"></i></dt>
            <dd class="col-10">Playing down. See CHSAA for details.</dd>
        </dl>
    @endif

@stop
