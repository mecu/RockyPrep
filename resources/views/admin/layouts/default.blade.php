@extends('layouts.sidenav')

@section('title')
    Administration :: @parent
@endsection

@section('styles')
    @parent

    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <script src="{{ asset('js/admin.js') }}"></script>

    {{-- Not yet a part of Elixir workflow --}}
    <script src="{{asset('assets/admin/js/bootstrap-dataTables-paging.js')}}"></script>
    <script src="{{asset('assets/admin/js/datatables.fnReloadAjax.js')}}"></script>
    <script src="{{asset('assets/admin/js/modal.js')}}"></script>
@endsection

@section('sidebar')
    @include('admin.partials.nav')
@endsection
