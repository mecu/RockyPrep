@extends('admin.layouts.modal') {{-- Content --}} @section('content')

{{-- Delete Post Form --}}
<form id="deleteForm" class="form-horizontal" method="post"
	action="@if (isset($news)){{ URL::to('admin/news/' . $news->id . '/delete') }}@endif"
	autocomplete="off">

	<input type="hidden" name="_token" value="{!! csrf_token() !!}" /> <input
		type="hidden" name="id" value="{{ $news->id }}" />
	<!-- <input type="hidden" name="_method" value="DELETE" /> -->
	<!-- ./ csrf token -->

	<!-- Form Actions -->
	<div class="form-group">
		<div class="controls">
			<p>Are you sure you want to delete?</p>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="fa fa-ban-circle"></span> Cancel</element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="fa fa-trash"></span> Delete
			</button>
		</div>
	</div>
	<!-- ./ form actions -->
</form>
@stop
