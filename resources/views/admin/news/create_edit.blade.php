@extends('admin.layouts.modal') {{-- Content --}} @section('content')
<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab"> General</a></li>
</ul>
<!-- ./ tabs -->
{{-- Edit Blog Form --}}
<form class="form-horizontal" enctype="multipart/form-data"
	method="post"
	action="@if(isset($news)){{ URL::to('admin/news/'.$news->id.'/edit') }}
	        @else{{ URL::to('admin/news/create') }}@endif"
	autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
	<!-- ./ csrf token -->
	<!-- Tabs Content -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<div class="tab-pane active" id="tab-general">
				<div
					class="form-group {!! $errors->has('language_id') ? 'has-error' : '' !!}">
					<div class="col-md-12">
						<label class="control-label" for="language_id"> Lanugage</label> <select
							style="width: 100%" name="language_id" id="language_id"
							class="form-control"> @foreach($languages as $item)
							<option value="{{$item->id}}"
								@if(!empty($language))
                                        @if($item->id==$language)
								selected="selected" @endif @endif >{{$item->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div
					class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
					<div class="col-md-12">
						<label class="control-label" for="title"> Title</label> <input
							class="form-control" type="text" name="title" id="title"
							value="{!! Input::old('title', isset($news) ? $news->title : null) !!}" />
						{{$errors->first('title', '<label class="control-label">:message</label>')}}
					</div>
				</div>
				<div
					class="form-group {!! $errors->has('introduction') ? 'has-error' : '' !!}">
					<div class="col-md-12">
						<label class="control-label" for="introduction">Introduction</label>
						<textarea class="form-control full-width wysihtml5"
							name="introduction" value="introduction" rows="10">{!! Input::old('introduction', isset($news) ? $news->introduction : null) !!}</textarea>
						{{ $errors->first('introduction', '<label class="control-label">:message</label>')
						}}
					</div>
				</div>
				<div
					class="form-group {!! $errors->has('content') ? 'has-error' : '' !!}">
					<div class="col-md-12">
						<label class="control-label" for="content">Content</label>
						<textarea class="form-control full-width wysihtml5" name="content"
							value="content" rows="10">{!! Input::old('content', isset($news) ? $news->content : null) !!}</textarea>
						{{ $errors->first('content', '<label class="control-label">:message</label>')
						}}
					</div>
				</div>
				<div
					class="form-group {!! $errors->has('source') ? 'error' : '' !!}">
					<div class="col-md-12">
						<label class="control-label" for="title">Source</label> <input
							class="form-control" type="text" name="source" id="source"
							value="{!! Input::old('source', isset($news) ? $news->source : null) !!}" />
						{{ $errors->first('source', '<label class="control-label">:message</label>')
						}}
					</div>
				</div>
				</div>
				<!-- ./ general tab -->
			</div>
			<!-- ./ tabs content -->

			<!-- Form Actions -->

			<div class="form-group">
				<div class="col-md-12">
					<button type="reset" class="btn btn-sm btn-warning close_popup">
						<span class="fa fa-ban-circle"></span> Cancel</button>
					<button type="reset" class="btn btn-sm btn-default">
						<span class="fa fa-remove-circle"></span> Reset</button>
					<button type="submit" class="btn btn-sm btn-success">
						<span class="fa fa-ok-circle"></span>
						@if	(isset($news))
							Edit
						@else
							Create
						@endif
					</button>
				</div>
			</div>
			<!-- ./ form actions -->
</form>
@stop
