@extends('admin.layouts.default')

@section('title') RPI Reach Dashboard :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Colorado RPI Reach</h3>
    </div>

    <ul>
        @foreach($reach as $season)
            <li>{{ $season->team->name }} ({{ $season->team->state->abbr }}): {{$season->levelid}}</li>
        @endforeach
    </ul>
@endsection
