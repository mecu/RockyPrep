@extends('admin.layouts.default')

@section('title') Level Entry for {{ $state->name }} :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Level Entry for {{ $state->name }}</h3>
        <h4>Year: {{ \App\Helpers\Helpers::currentYear() }}</h4>
    </div>

    @if(Session::has('updated'))
        <p class="alert alert-success">Updated {{ Session::get('updated') }} teams.</p>
    @endif

    {{ Form::open(['class' => 'form']) }}
        <table class="table">
            <thead>
            <tr>
                <th style="text-align:center">Team</th>
                <th style="text-align:center">Level (CO)</th>
                <th style="text-align:center">Level State</th>
                <th style="text-align:center">Play Down/Equal/Up</th>
            </tr>
            </thead>
            <tbody>
                @foreach($teams as $team)
                    <tr>
                        {{--*/ $season = $team->seasons()->where('year', Helpers::currentYear())->first(); /*--}}
                        <td style="text-align:right">{{ $team->name }}</td>
                        <td>{{ Form::select('l_' . $team->teamid, $levels, !empty($season->levelid) ? $season->levelid : '') }}</td>
                        <td>{{ Form::select('ls_' . $team->teamid, $levels, !empty($season->levelid_state) ? $season->levelid_state : '') }}</td>
                        <td>
                            <i class="fa fa-arrow-down"></i>{{ Form::radio('a_' . $team->teamid, -1, !empty($season->level_alter) && $season->level_alter < 0) }}
                            {{ Form::radio('a_' . $team->teamid, 0, empty($season->level_alter)) }}
                            {{ Form::radio('a_' . $team->teamid, 1, !empty($season->level_alter) && $season->level_alter > 0) }}<i class="fa fa-arrow-up"></i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ Form::submit('Submit', ['class' => 'btn btn-lg btn-primary']) }}
        {{ Form::close() }}

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
@endsection
