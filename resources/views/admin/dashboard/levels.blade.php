@extends('admin.layouts.default')

@section('title') Level Entry State Selection Dashboard :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Level Entry State Selection Dashboard</h3>
    </div>

    @include('admin.partials.statesNav', ['routeName' => 'levelsState'])
@endsection
