@extends('admin.layouts.default')

@section('title') Create Stadium :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Create Stadium</h3>
    </div>

    @if(Session::has('created'))
        <p class="alert alert-success">Stadium created! Stadium ID: {{ Session::get('created') }}</p>
    @endif
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    {{ Form::open(['class' => 'form-horizontal']) }}
    {{ Form::model($team) }}
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('name', 'Name &reg;', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::text('name', null, ['class' => 'form-control', 'required' => true]) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('location', 'Google Maps URL', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('location', null, ['class' => 'form-control']) }}
            <p class="help-block">Example: https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1669+Eagle+Drive,+Loveland,+CO+80537&sll=38.959008,-104.733632&sspn=0.015734,0.026157&ie=UTF8&hq=&hnear=1669+Eagle+Dr,+Loveland,+Larimer,+Colorado+80537&ll=40.388683,-105.099442&spn=0.007706,0.013078&t=h&z=17</p></div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('address', 'Street Address', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('address', null, ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('alt', 'Alternate Name (at a HS or City, ST)', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('alt', null, ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('stateid', 'State &reg;', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::select('stateid', $states, '', ['class' => 'form-control', 'required' => true]) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('zip', 'Zip Code', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('zip', null, ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('lat', 'Latitude', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('lat', null, ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('lng', 'Longitude', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('lng', null, ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {{ Form::submit('Create', ['class' => 'btn btn-lg btn-primary']) }}
        </div>
    </div>
    {{ Form::close() }}

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
@endsection
