@extends('admin.layouts.default')

@section('title') Game Reporting for {{ $state->name }} :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Game Reporting for {{ $state->name }}</h3>
    </div>

    @foreach($games as $game)
        <div class="card-group">
            <div class="card" id="parentGame{{ $game->gameid }}">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ $game->date }}: {{ $game->away->name }} @ {{ $game->home->name }} <button data-toggle="collapse" data-target="#game{{ $game->gameid }}">Report</button>
                    </h4>
                <div id="game{{ $game->gameid }}" class="collapse">
                    {{ Form::open(['class' => 'form report', 'data-game' => $game->gameid]) }}
                        <div class="card-body">
                                <table>
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">Team</th>
                                        <th style="text-align:center">Q1</th>
                                        <th style="text-align:center">Q2</th>
                                        <th style="text-align:center">Q3</th>
                                        <th style="text-align:center">Q4</th>
                                        <th style="text-align:center">Total</th>
                                        <th style="text-align:center" colspan="2">Winner</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td style="text-align:right">{{ $game->away->name }}</td>
                                        <td><input type="number" min="0" step=1" title="Away quarter 1 score" name="game[scores][1][away]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Away quarter 2 score" name="game[scores][2][away]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Away quarter 3 score" name="game[scores][3][away]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Away quarter 4 score" name="game[scores][4][away]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Away final score" name="awayscore" style="width: 4em;" required></td>
                                        <td style="text-align:center"><div class="radio"><label><input type="radio" name="winner" value="{{ $game->awayteam }}" required></label></div></td>
                                        <td style="text-align:center" rowspan="2"><div class="radio"><label><input type="radio" name="winner" value="0" required>Tie</label></div></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right">{{ $game->home->name }}</td>
                                        <td><input type="number" min="0" step=1" title="Home quarter 1 score" name="game[scores][1][home]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Home quarter 2 score" name="game[scores][2][home]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Home quarter 3 score" name="game[scores][3][home]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Home quarter 4 score" name="game[scores][4][home]" style="width: 4em;"></td>
                                        <td><input type="number" min="0" step=1" title="Home final score" name="homescore" style="width: 4em;" required></td>
                                        <td style="text-align:center"><div class="radio"><label><input type="radio" name="winner" value="{{ $game->hometeam }}" required></label></div></td>
                                    </tr>
                                    <tr>
                                        <td colspan="99">
                                            <div class="checkbox"><label><input type="checkbox" name="neutral" value="{{ $game->neutral }}"> Neutral</label></div>
                                            <div class="checkbox"><label><input type="checkbox" name="forfeit" value="{{ $game->forfeit }}"> Forfeit</label></div>
                                            <div class="checkbox"><label><input type="checkbox" name="playoff" value="{{ $game->playoff }}"> Playoff</label></div>
                                            <div class="checkbox"><label><input type="checkbox" name="conference" value="{{ $game->conference }}"> Conference</label></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
            </div>
        </div>
    @endforeach

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script>
        $().ready(function() {
            $('form.report').on('submit', function (e) {
                e.stopImmediatePropagation();

                var $form = $(this);
                var gameid = $form.data('game');
                console.log($form);
                console.log($form.serialize());

                $.ajax({
                    type: "POST",
                    url: '/admin/api/report/game/' + gameid,
                    data: $form.serialize()
                }).done(function (data) {
                    $form.replaceWith('<div class="alert alert-success" role="alert" style="color:#000">Success!</div>');
                }).fail(function (data) {
                    $form.append('<div class="alert alert-warning" role="alert" style="color:#000">FAIL! ' + data.responseText + '</div>');
                    console.log(data);
                });

                return false;
            });
        });
    </script>
@endsection
