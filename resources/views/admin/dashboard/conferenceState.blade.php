@extends('admin.layouts.default')

@section('title') Conference Entry for {{ $state->name }} :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Conference Entry for {{ $state->name }}</h3>
        <h4>Year: {{ \App\Helpers\Helpers::currentYear() }}</h4>
    </div>

    @if(Session::has('updated'))
        <p class="alert alert-success">Updated {{ Session::get('updated') }} teams.</p>
    @endif

    {{ Form::open(['class' => 'form']) }}
        <table class="table">
            <thead>
            <tr>
                <th style="text-align:center">Team</th>
                <th style="text-align:center">Conference</th>
            </tr>
            </thead>
            <tbody>
                @foreach($teams as $team)
                    <tr>
                        {{--*/ $season = $team->seasons()->where('year', Helpers::currentYear())->first(); /*--}}
                        <td style="text-align:right">{{ $team->name }}</td>
                        <td>{{ Form::select('conference_' . $team->teamid, $conferences, !empty($season->confid) ? $season->confid : '') }}</td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ Form::submit('Submit', ['class' => 'btn btn-lg btn-primary']) }}
    {{ Form::close() }}

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
@endsection
