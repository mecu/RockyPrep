@extends('admin.layouts.default')

@section('title') Create Team Automatically:: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Create Team Automatically</h3>
    </div>

    @if(Session::has('created'))
        <p class="alert alert-success">Team created! Team ID: {{ Session::get('created') }}</p>
    @endif
    @if(Session::has('error'))
        <p class="alert alert-danger">ERROR: {!! Session::get('error') !!}</p>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::open(['class' => 'form-horizontal']) }}
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('url', 'URL', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::text('url', old('url'), ['class' => 'form-control', 'required' => true]) }}</div>
    </div>
    {{ Form::submit('Create') }}
    {{ Form::close() }}
@endsection
