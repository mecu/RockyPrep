@extends('admin.layouts.default')

@section('title') Conference Entry State Selection Dashboard :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Conference Entry State Selection Dashboard</h3>
    </div>

    @include('admin.partials.statesNav', ['routeName' => 'conferencesState'])
@endsection
