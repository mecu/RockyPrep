@extends('admin.layouts.default')

@section('title') Create Team :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Create Team</h3>
    </div>

    @if(Session::has('created'))
        <p class="alert alert-success">Team created! Team ID: {{ Session::get('created') }}</p>
    @endif
    @if(Session::has('error'))
        <p class="alert alert-danger">ERROR: {!! Session::get('error') !!}</p>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::open(['class' => 'form-horizontal']) }}
    {{ Form::model($team) }}
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('name', 'Name &reg;', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::text('name', old('name'), ['class' => 'form-control', 'required' => true]) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('urlname', 'Slug &reg;', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::text('urlname', old('urlname'), ['class' => 'form-control', 'required' => true]) }}
            <p class="help-block">Do not use: /:(), replace spaces with _, / with -</p>
            <p class="help-block">Should have _st at the end, except for Colorado</p>
            <p class="help-block">example: peak_to_peak and lacrosse-washtucna-kahlotus_wa</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('mascot', 'Mascot', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('mascot', old('mascot'), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('city', 'City', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('city', old('city'), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('stateid', 'State &reg;', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::select('stateid', $states, old('stateid'), ['class' => 'form-control', 'required' => true]) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('jv', 'JV?', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::checkbox('jv', '1', old('jv', false), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('color1', 'Primary Color', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('color1', old('color1'), ['class' => 'form-control']) }}
            <p class="help-block">hex code, 6 char long, do not include #</p></div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('color2', 'Secondary Color', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('color2', old('color2'), ['class' => 'form-control']) }}
            <p class="help-block">hex code, 6 char long, do not include #</p></div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('address', 'Street Address', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('address', old('address'), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('zip', 'Zip Code', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('zip', old('zip'), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('lat', 'Latitude', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('lat', old('lat'), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('lng', 'Longitude', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('lng', old('lng'), ['class' => 'form-control']) }}</div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('maxpreps_name', 'MaxPreps Name', ['class' => 'control-label']) }}</div>
        <div class="col-xs-10">{{ Form::text('maxpreps_name', old('maxpreps_name'), ['class' => 'form-control']) }}
            <p class="help-block">example: arapahoe-warriors-(centennial,co)</p></div>
    </div>
    <div class="form-group">
        <div class="col-xs-2 text-right">{{ Form::label('maxpreps_id', 'MaxPreps ID &reg;', ['class' => 'control-label', 'required' => true]) }}</div>
        <div class="col-xs-10">{{ Form::text('maxpreps_id', old('maxpreps_id'), ['class' => 'form-control', 'required' => true]) }}
            <p class="help-block">example: c3d0b30b-6473-4b23-a241-055ab523ede8</p></div>
    </div>
    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {{ Form::submit('Create', ['class' => 'btn btn-lg btn-primary']) }}
        </div>
    </div>
    {{ Form::close() }}

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script>
        function slugify(text) {
            return text.toString().toLowerCase()
                    .replace(/\s+/g, '_')           // Replace spaces with _
                    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                    .replace(/\_\_+/g, '_')         // Replace multiple _ with single _
                    .replace(/^-+/, '')             // Trim - from start of text
                    .replace(/^_+/, '')             // Trim _ from start of text
                    .replace(/^-+/, '')             // Trim - from start of text
                    .replace(/_+$/, '');            // Trim _ from end of text
        }
        $(function () {
            $('input#name').on('change', function () {
                $('input#urlname').val(slugify($('input#name').val()));
            });
        });
    </script>
@endsection
