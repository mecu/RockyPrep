@extends('admin.layouts.default')

@section('title') Games the Scraper Wanted to Delete :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Games the Scraper Wanted to Delete</h3>
    </div>

    <ul>
        @foreach($games as $game)
            <li>{{ $game->date->format('Y-m-d') }}: {{ link_to_route('team', $game->away->name, [
                                    $game->away->state->urlname,
                                    $game->away->urlname,
                                    $game->year()->year,
                                ]) }} @ {{ link_to_route('team', $game->home->name, [
                                    $game->home->state->urlname,
                                    $game->home->urlname,
                                    $game->year()->year,
                                ]) }}</li>
        @endforeach
    </ul>
@endsection
