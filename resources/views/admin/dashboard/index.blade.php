@extends('admin.layouts.default')

@section('title') {!! $title !!} :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>
            {!! $title !!}
        </h3>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">
                    <i class="fa fa-fw fa-file fa-3x"></i>
                    Scraper Reports
                </h3>
                <h4 class="card-subtitle mb-2 text-muted">Last Run Date: TBD</h4>
                <a href="{{URL::to('admin/reports')}}"
                   class="btn btn-primary pull-right">
                    See Report List <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">
                        <i class="fa fa-fw fa-calendar fa-3x"></i>
                        Game Reporting
                    </h3>
                    <h4 class="card-subtitle mb-2 text-muted">Overdue Games: TBD</h4>
                    <a href="{{URL::to('admin/reporting')}}"
                       class="btn btn-primary pull-right">
                        See Game List <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">
                        <i class="fa fa-fw fa-eraser fa-3x"></i>
                        Games Scraper Wanted To Delete
                    </h3>
                    <h4 class="card-subtitle mb-2 text-muted">Games: TBD</h4>
                    <a href="{{URL::to('admin/wantToDelete')}}"
                       class="btn btn-primary pull-right">
                        See Game List <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
