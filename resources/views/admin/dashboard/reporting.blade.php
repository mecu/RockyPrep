@extends('admin.layouts.default')

@section('title') Reporting State Selection Dashboard :: @parent @stop

@section('main')
    <div class="page-header">
        <h3>Reporting State Selection Dashboard</h3>
    </div>

    @include('admin.partials.statesNav', ['routeName' => 'reportingState'])
@endsection
