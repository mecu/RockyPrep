@extends('admin.layouts.default')

@section('title') {!! $title !!} :: @parent @stop

@section('main')

    <div class="page-header">
        <h3>{!! $title !!}</h3>
    </div>

    <table id="data-table" class="table table-striped table-condensed table-hover" style="width:auto">
        <thead>
        <tr>
            <td>Name</td>
            <td>Updated @ (Mountain)</td>
            <td>Size (Kb)</td>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $file)
            <tr>
                <td><a href="./reports/{{ $file['filename'] }}">{{ $file['name'] }}</a></td>
                <td>{{ \Carbon\Carbon::createFromTimestamp($file['attr']['mtime'], 'UTC')->setTimezone('America/Denver')->format('l, M j @ g:i a') }}</td>
                <td>{{ round($file['attr']['size']/1000, 0) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
