@extends('admin.layouts.default')

@section('title') Users :: @parent
@stop

@section('main')
    <div class="page-header">
            <div class="pull-right">
                <a href="{!! URL::to('admin/users/create') !!}" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus-circle"></i> New
                </a>
            </div>
        <h3>Users</h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Confirmed</th>
            <th>Created</th>
            <th>Deleted</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->confirmed ? 'Yes' : 'No' }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->deleted_at ? 'Yes' : 'No' }}</td>
                <td><a href="{{ route('edit.user', ['id' => $user->id]) }}"><i class="fa fa-pencil"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
