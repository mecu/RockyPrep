@extends('admin.layouts.default')
@section('content')
    <form class="form-horizontal" method="post"
          action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
        <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Name</label>
                        <div class="col-md-10">
                            <input class="form-control" tabindex="1"
                                   placeholder="name" type="text"
                                   name="name" id="name"
                                   value="{!! Input::old('name', isset($user) ? $user->name : null) !!}">
                        </div>
                    </div>
                </div>
                @if(!isset($user))
                    <div class="col-md-12">
                        <div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
                            <label class="col-md-2 control-label" for="username">Username</label>
                            <div class="col-md-10">
                                <input class="form-control" type="username" tabindex="4"
                                       placeholder="username" name="username"
                                       id="username"
                                       value="{!! Input::old('username', isset($user) ? $user->username : null) !!}"/>
                                {{ $errors->first('username', '<label class="control-label"
                                                                    for="username">:message</label>')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                            <label class="col-md-2 control-label" for="email">email</label>
                            <div class="col-md-10">
                                <input class="form-control" type="email" tabindex="4"
                                       placeholder="email" name="email"
                                       id="email"
                                       value="{!! Input::old('email', isset($user) ? $user->email : null) !!}"/>
                                {{ $errors->first('email', '<label class="control-label"
                                    for="email">:message</label>')}}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
                        <label class="col-md-2 control-label" for="password">Password</label>
                        <div class="col-md-10">
                            <input class="form-control" tabindex="5"
                                   placeholder="password"
                                   type="password" name="password" id="password" value=""/>
                            {{$errors->first('password', '<label class="control-label"
                                for="password">:message</label>')}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {!! $errors->has('password_confirmation') ? 'has-error' : '' !!}">
                        <label class="col-md-2 control-label" for="password_confirmation">Password Confirmation</label>
                        <div class="col-md-10">
                            <input class="form-control" type="password" tabindex="6"
                                   placeholder="pasword confirmation"
                                   name="password_confirmation" id="password_confirmation" value=""/>
                            {{$errors->first('password_confirmation', '<label
                                class="control-label" for="password_confirmation">:message</label>')}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="confirm">Confirm</label>
                        <div class="col-md-10">
                            <select class="form-control" name="confirmed" id="confirmed">
                                <option value="1" {!! ((isset($user) && $user->confirmed == 1)? ' selected="selected"' : '') !!}>Yes</option>
                                <option value="0" {!! ((isset($user) && $user->confirmed == 0) ?' selected="selected"' : '') !!}>No</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="reset" class="btn btn-sm btn-warning close_popup">
                    <span class="fa fa-ban-circle"></span> Cancel
                </button>
                <button type="reset" class="btn btn-sm btn-default">
                    <span class="fa fa-remove-circle"></span> Reset
                </button>
                <button type="submit" class="btn btn-sm btn-success">
                    <span class="fa fa-ok-circle"></span>
                    @if	(isset($user))
                        Edit
                    @else
                        Create
                    @endif
                </button>
            </div>
        </div>
    </form>
@stop