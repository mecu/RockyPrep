<div class="metismenu">
<ul class="nav nav-pills nav-stacked">
    @foreach ($states as $state)
    <li>{{ link_to_route($routeName, $state->name, ['state' => $state->urlname]) }}</li>
    @endforeach
</ul>
</div>
