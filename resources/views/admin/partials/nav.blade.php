<div class="metismenu">
    <ul class="nav nav-pills nav-stacked">
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/dashboard')}}">
                <i class="fa fa-dashboard fa-fw"></i>
                <span class="hidden-sm text"> Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/reports')}}">
                <i class="fa fa-file fa-fw"></i>
                <span class="hidden-sm text"> Scraper Reports</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/reporting')}}">
                <i class="fa fa-calendar fa-fw"></i>
                <span class="hidden-sm text"> Game Reporting</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/reach')}}">
                <i class="fa fa-book fa-fw"></i>
                <span class="hidden-sm text"> RPI Reach</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/levels')}}">
                <i class="fa fa-list-ol fa-fw"></i>
                <span class="hidden-sm text"> Level Entry</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/conferences')}}">
                <i class="fa fa-link fa-fw"></i>
                <span class="hidden-sm text"> Conference Entry</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/create/team')}}">
                <i class="fa fa-plus fa-fw"></i>
                <span class="hidden-sm text"> Create Team</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/create/auto-team')}}">
                <i class="fa fa-plus-square fa-fw"></i>
                <span class="hidden-sm text"> Auto Create Team</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/create/stadium')}}">
                <i class="fa fa-map-marker fa-fw"></i>
                <span class="hidden-sm text"> Create Stadium</span>
            </a>
        </li>
        <li class="nav-item">
            Site Utilities:
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/users')}}">
                <i class="fa fa-user"></i>
                <span class="hidden-sm text"> Users</span>
            </a>
        </li>
    </ul>
</div>
