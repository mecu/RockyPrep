@extends('layouts.master-nosidebar')
@section('title') Contact :: @parent @stop
@section('content')
    <div class="page-header">
        <h2>Contact Page</h2>
    </div>
    <p>You can call 303-578-CPF0 (<strong>303-578-2730</strong>) (leave a message). You can text to this number as well.</p>
    <p>Reporting game scores? Call or text the number above, e-mail <a href="mailto:scores@rockyprep.com">scores@rockyprep.com</a> or contact via Twitter or Facebook.</p>
    <p>See an error? Call or text the number above, e-mail <a href="mailto:errors@rockyprep.com">errors@rockyprep.com</a> or contact via Twitter or Facebook. In order to help solve the problem faster, please be sure sure to include the <strong>state</strong> and <strong>full team name</strong>.</p>

    <p>Last modified: July 27, 2017</p>
@endsection
