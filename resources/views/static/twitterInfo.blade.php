@extends('layouts.master-nosidebar')
@section('title') Twitter :: @parent @stop
@section('content')
    <div id="header" class="col-xs-12">
        <div class="jumbotron">
            <h1>All Twitter Accounts</h1>
        </div>
    </div>

    <p>This is the list of all RockyPrep Twitter accounts. Twitter is primarily used to tweet out (recent) game results.</p>
    <ul>
        <li>Alabama <a href="https://twitter.com/RockyPrepAL">Football</a></li>
        <li>Alaska <a href="https://twitter.com/RockyPrepAK">Football</a></li>
        <li>Arizona <a href="https://twitter.com/RockyPrepAZ">Football</a></li>
        <li>Arkansas <a href="https://twitter.com/RockyPrepAR">Football</a></li>
        <li>California <a href="http://twitter.com/RockyPrepCA">Football</a></li>
        <li>Colorado <a href="http://twitter.com/RockyPrep">Football</a> (Also the main account)</li>
                {{--<li><a href="http://twitter.com/RockyPrepCOBBB">Boys Basketball</a></li>--}}
                {{--<li><a href="http://twitter.com/RockyPrepCOGBB">Girls Basketball</a></li>--}}
                {{--<li><a href="http://twitter.com/RockyPrepCOBB">Baseball</a></li>--}}
                {{--<li><a href="http://twitter.com/RockyPrepCOSB">Softball</a></li>--}}
                {{--<li><a href="http://twitter.com/RockyPrepCOGVB">Girls Volleyball</a></li>--}}
        <li>Connecticut <a href="https://twitter.com/RockyPrepCT">Football</a></li>
        <li>Delaware <a href="https://twitter.com/RockyPrepDE">Football</a></li>
        <li>Florida <a href="http://twitter.com/RockyPrepFL">Football</a></li>
        <li>Georgia <a href="http://twitter.com/RockyPrepGA">Football</a></li>
        <li>Hawaii <a href="http://twitter.com/RockyPrepHI">Football</a></li>
        <li>Idaho <a href="http://twitter.com/RockyPrepID">Football</a></li>
        <li>Illinois <a href="http://twitter.com/RockyPrepIL">Football</a></li>
        <li>Indiana <a href="http://twitter.com/RockyPrepIN">Football</a></li>
        <li>Iowa <a href="http://twitter.com/RockyPrepIA">Football</a></li>
        <li>Kansas <a href="http://twitter.com/RockyPrepKS">Football</a></li>
        <li>Kentucky <a href="http://twitter.com/RockyPrepKY">Football</a></li>
        <li>Louisiana <a href="http://twitter.com/RockyPrepLA">Football</a></li>
        <li>Maine <a href="http://twitter.com/RockyPrepME">Football</a></li>
        <li>Maryland <a href="http://twitter.com/RockyPrepMD">Football</a></li>
        <li>Massachusetts <a href="http://twitter.com/RockyPrepMA">Football</a></li>
        <li>Michigan <a href="http://twitter.com/RockyPrepMI">Football</a></li>
        <li>Minnesota <a href="http://twitter.com/RockyPrepMN">Football</a></li>
        <li>Mississippi <a href="http://twitter.com/RockyPrepMS">Football</a></li>
        <li>Missouri <a href="http://twitter.com/RockyPrepMO">Football</a></li>
        <li>Montana <a href="http://twitter.com/RockyPrepMT">Football</a></li>
        <li>Nebraska <a href="http://twitter.com/RockyPrepNE">Football</a></li>
        <li>Nevada <a href="http://twitter.com/RockyPrepNV">Football</a></li>
        <li>New Hampshire <a href="http://twitter.com/RockyPrepNH">Football</a></li>
        <li>New Jersey <a href="http://twitter.com/RockyPrepNJ">Football</a></li>
        <li>New Mexico <a href="http://twitter.com/RockyPrepNM">Football</a></li>
        <li>New York <a href="http://twitter.com/RockyPrepNY">Football</a></li>
        <li>North Carolina <a href="http://twitter.com/RockyPrepNC">Football</a></li>
        <li>North Dakota <a href="http://twitter.com/RockyPrepND">Football</a></li>
        <li>Ohio <a href="http://twitter.com/RockyPrepOH">Football</a></li>
        <li>Oklahoma <a href="http://twitter.com/RockyPrepOK">Football</a></li>
        <li>Oregon <a href="http://twitter.com/RockyPrepOR">Football</a></li>
        <li>Pennsylvania <a href="http://twitter.com/RockyPrepPA">Football</a></li>
        <li>Rhode Island <a href="http://twitter.com/RockyPrepRI">Football</a></li>
        <li>South Carolina <a href="http://twitter.com/RockyPrepSC">Football</a></li>
        <li>South Dakota <a href="http://twitter.com/RockyPrepSD">Football</a></li>
        <li>Tennessee <a href="http://twitter.com/RockyPrepTN">Football</a></li>
        <li>Texas <a href="http://twitter.com/RockyPrepTX">Football</a></li>
        <li>Utah <a href="http://twitter.com/RockyPrepUT">Football</a></li>
        <li>Vermont <a href="http://twitter.com/RockyPrepVT">Football</a></li>
        <li>Virginia <a href="http://twitter.com/RockyPrepVA">Football</a></li>
        <li>Washington <a href="http://twitter.com/RockyPrepWA">Football</a></li>
        <li>Washington DC <a href="http://twitter.com/RockyPrepDC">Football</a></li>
        <li>West Virginia <a href="http://twitter.com/RockyPrepWV">Football</a></li>
        <li>Wisconsin <a href="http://twitter.com/RockyPrepWI">Football</a></li>
        <li>Wyoming <a href="http://twitter.com/RockyPrepWY">Football</a></li>
    </ul>
    <div>
        <p>Last modified: May 20, 2016</p>
    </div>
@endsection
