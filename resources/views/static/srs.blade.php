@extends('layouts.master-nosidebar')
@section('title') SRS :: @parent @stop

@section('content')
    <div class="jumbotron">
	    <h1>SRS is the Simple Rating System</h1>
	</div>
	
    <p>Under each sport and classification, there's a SRS: Simple Ranking System. What is the SRS? <a href="http://www.pro-football-reference.com/blog/?p=37">This blog post is a good explanation</a> on what it is (you really should read at least the first part of that post before reading more, or it just won't make sense).</p>
    <p>How is the <a href="http://rockyprep.com/football/colorado/srs/">SRS</a> implemented here?</p>
    <ul>
        <li><strong>SRS</strong> is the raw SRS with no margin limit.</li>
        <li><strong>Avg PD</strong> is the average point differential.</li>
        <li><strong>Opp Avg</strong> is the Opponents average point differential. </li>
        <li><strong>Num Games</strong> is, well, the number of games that team has played.</li>
    </ul>
    <p>Only the current, up-to-the-minute SRS is available to view. You can see last-season (or any season, but seasons without complete data such as before 2010 are meaningless). Here is the <a href="http://rockyprep.com/football/colorado/srs/5A/2011/">2011 5A Football final SRS</a>.</p>
    <p>Last modified: March 28, 2014</p>
@endsection
