@extends('layouts.master-nosidebar')
@section('title') RPI :: @parent @stop

@section('content') 
    <div class="jumbotron">
	    <h1>RPI is the Ratings Percentage Index</h1>
	</div>
	
    <p>The Ratings Percentage Index (RPI) was created in 1981 as a simple way to measure a team’s relative strength, including its schedule strength, and has been used for years by the NCAA to rank basketball and baseball teams in order to aid in the selection and seeding process for their postseason tournaments.</p>
    <p>See also: <a href="http://www.collegerpi.com/rpifaq.html">http://www.collegerpi.com/rpifaq.html</a></p>
    <p>For Colorado, the RPI accounts for a team’s win‐loss record (winning percentage) and its strength of schedule, and does so without consideration to margin of victory or defeat. The RPI considers the win‐loss records of not just its opponents, but also that of the opponents of a team’s opponent. Clearly, the true relative strength of two opponents with identical records can be vastly different (based on the teams that each of them have played), and the RPI formula accounts for that with this additional factor, which helps to produce a result that is a little closer to what we would expect for an appropriate ranking of teams.</p>
    <p>You can see the proposal here: <a href="https://docs.google.com/open?id=0B4eKsnD6QixfbnZzd1YzVTYwcEE">https://docs.google.com/open?id=0B4eKsnD6QixfbnZzd1YzVTYwcEE</a></p>
    <p>There are several versions on the site. These just combine the WP, OWP and OOWP values in different quantities:</p>
    <p>
        <strong>RPI</strong>: (1/4 + 1/2 + 1/4) (this is the standard)<br>
        <strong>=RPI</strong>: The values are equal (1/3 + 1/3 + 1/3) (an alternate approach)
    </p>
    <p>For the Colorado proposal, an alternate system that incorporates the classifications of teams primarily based on school size (but may include other factors) is also used to account for the fact that a 5A team beating a 1A team isn't that significant as a 5A beating another 5A (or 4A or 3A or 2A). This is called the RPRPI (or CoRPI) and produces results that should be considered better than the standard RPI calculation that has no classification factoring.</p>
    <h3>UPDATE:</h3>
    <p>CHSAA will use the RPI calculation for ALL TEAM sports to determine playoff teams starting with the 2016-2017 season.</p>
    <p>This site does not factor in home and away in the calculations, since they are often erroneous and difficult to determine in high school with district facilities.</p>        
    <p>Last modified: September 5, 2018</p>
@endsection
