@extends('layouts.master-nosidebar')
@section('title') About :: @parent @stop
@section('content')
    <div class="page-header">
        <h2>About RockyPrep.com</h2>
    </div>
    
    <p>The goal of this website is to calculate <a href="{{ route('rpiPage') }}">Ratings Percentage Index (RPI)</a> for High School Football.</p>
    <p>Secondary, to better provide information about High School Football, specifically schedule, results and standings.</p>
    <p>The focus is not primary score reporting (getting the scores quicker than competitors).</p>
    <p>The initial purpose of the site was to take the results of high school football in Colorado and apply the points, as determined by CHSAA, to show the current standings of the playoffs. The most useful part was to see potential opponents, whereas if your team is ranked #20, you are scheduled to play #13, but then you can also see who the #10-#16 teams are as well to see who could move up and down. For 5A, these point rankings determine the seeding. For other levels, a committee determines wild-card teams, so this site can only display the automatic qualifiers and show the remaining teams who would likely be chosen by the committee. While wild card points are not calculated or considered for the 2A and lower levels by CHSAA or any committee, I calculate and present them here for informational purposes.</p>
    <p>All other features on the site are to increase the function and purpose stated above. You can help by letting us know of any mistakes.</p>
    <p>The website will always be free.</p>
    <p>You can get <a href="{{ route('twitter-info') }}">score reports via Twitter</a>.</p>
    <p>This website is run by myself, Michael Hoppes, a former assistant football coach at Mountain Range High School in Westminster, Colorado and 1 amazing assistant who wishes to remain anonymous, and not by a company seeking to make any profit. The sponsorship I seek is to offset the cost of running the website (domain name, web hosting, etc.). I am always open to others who wish to help cover over 20,000 high school teams (so far) in 12 sports (so far). Contact me if you are interested in helping.</p>
    <p>All errors are purely accidental (Sorry!) and will be corrected once noticed. Please <a href="{{ route('contact-us') }}">let me know</a> if you discover an error. Thanks!</p>
    <p>We are an independent group intersted in high school sports in the USA. We seek to provide an Anyone Can Edit<sup>**</sup> (like Wikipedia) high school sports team scores, standings and results page. Calculating the Ratings Percentile Index (RPI) for all sports as well.
        <br><sup>**</sup> Editor rights can be earned or given, but taken away for abusive use.
    </p>

    <h3>Find the Peak?</h3>
    <p>The slogan of the site means to find peak performers within each sport through ratings systems. The <a href="{{ route('rpiPage') }}">RPI (Ratings Percentile Index)</a> is the primary system, but the <a href="{{ route('srsPage') }}">SRS (Simple Rating System)</a> also has its place.</p>

    <h3>Let's go!</h3>
    <p>Find your team by <a href="{{ route('showall') }}">selecting the state and sport</a>. Let us know of any problems.</p>

    <p>Last modified: July 27, 2017</p>
@endsection
