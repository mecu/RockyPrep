@spaceless
<nav class="navbar fixed-top navbar-expand-md">
    <a class="navbar-brand" href="/">RockyPrep</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#header-menu">
        <i class="fa fa-navicon"></i>
    </button>
    <div class="navbar-collapse collapse" id="header-menu">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{!! route('showall') !!}">All States</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{!! route('teamfinder') !!}">Team Finder</a>
            </li>
            @if (isset($state))
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">{!! $state->name !!}</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item"
                       href="{!! route('schedule', ['state' => $state->urlname, 'year' => \App\Helpers\Helpers::currentYear(), 'month' => date('n'), 'day' => date('d')]) !!}">
                        Schedule
                    </a>
                    <a class="dropdown-item"
                       href="{!! route('standings', ['state' => $state->urlname, 'level' => '5A', 'year' => \App\Helpers\Helpers::currentYear()]) !!}">
                        Standings
                    </a>
                    <a class="dropdown-item" href="{!! route('allteams', ['state' => $state->urlname, 'year' => \App\Helpers\Helpers::currentYear()]) !!}">
                        Teams
                    </a>
                    <a class="dropdown-item" href="{!! route('srs', ['state' => $state->urlname, 'year' => \App\Helpers\Helpers::currentYear()]) !!}">
                        SRS
                    </a>
                    <a class="dropdown-item" href="{!! route('rpi', ['state' => $state->urlname, 'year' => \App\Helpers\Helpers::currentYear()]) !!}">
                        RPI
                    </a>
                    <a class="dropdown-item" href="{!! route('map', ['state' => $state->urlname, 'year' => \App\Helpers\Helpers::currentYear()]) !!}">
                        Map
                    </a>
                    {{--
                    <a class="dropdown-item" href="{!! route('conference', ['state' => $state->urlname, 'year' => \App\Helpers\Helpers::currentYear()]) !!}">
                        Conference
                    </a>
                    --}}
                    {{--
                    <a class="dropdown-item" href="{!! route('coach', ['state' => $state->urlname]) !!}">
                        Coaches
                    </a>
                    --}}
                </div>
            </li>
            @endif
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">About Us</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{!! route('about-us') !!}">About Us</a>
                    <a class="dropdown-item" href="{!! route('contact-us') !!}">Contact Us</a>
                    <a class="dropdown-divider"></a>
                    <a class="dropdown-item" href="{!! route('srsPage') !!}">What is the SRS?</a>
                    <a class="dropdown-item" href="{!! route('rpiPage') !!}">What is the RPI?</a>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav navbar-right">
            <li class="nav-item">
                <a class="nav-link" href="{!! route('twitter-info') !!}">Twitter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//www.facebook.com/RockyPrep">Facebook</a>
            </li>
            @if (Auth::check())
            <li>
                <a class="nav-link" href="{!! url('/logout') !!}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout </a>
                <form id="logout-form" action="{!! url('/logout') !!}"
                      method="POST" style="display: none;">{!! csrf_field() !!}
                </form>
            </li>
            @endif
        </ul>
    </div>
</nav>
@endspaceless
