@spaceless
<nav class="navbar navbar-expand-md">
    <a class="navbar-brand" href="#">Years</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#year-navs">
        <i class="fa fa-navicon"></i>
    </button>
    <div class="navbar-collapse collapse" id="year-navs">
        <div class="navbar-nav">
            @foreach ( $years as $key )
                @if ( $key == $year->year )
                    <a class="nav-item nav-link active"
                @else
                    <a class="nav-item nav-link"
                       @endif
                       href="{!! route($route, ['state' => $state->urlname, 'year' => $key]+$params) !!}">
                        {!! $key !!}
                    </a>
                    @endforeach
        </div>
    </div>
</nav>
@endspaceless
