<table class=&quot;table table-sm table-striped&quot; style=&quot;max-width:none&quot;>
    <thead>
    <tr>
        <td>&nbsp;</td>
        @foreach ($game->scores as $score)
            @if ($score->quarter < 5)
                <th>{{ $score->quarter }}</th>
            @elseif ($score->quarter === 5)
                <th>OT</th>
            @else
                <th>{{ $score->quarter - 4 }}OT</th>
            @endif
        @endforeach
        <th>T</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $game->away->name }}</td>
        @foreach ($game->scores as $score)
            <td>{{ $score->away }}</td>
        @endforeach
        <td>{{ $game->awayscore }}</td>
    </tr>
    <tr>
        <td>{{ $game->home->name }}</td>
        @foreach ($game->scores as $score)
            <td>{{ $score->home }}</td>
        @endforeach
        <td>{{ $game->homescore }}</td>
    </tr>
    </tbody>
</table>
