@extends('layouts.master')
@section('content')
    <div class="row">
            @yield('top')
    </div>
    <div class="row">
            @yield('main')
    </div>
@endsection
