@spaceless
<!doctype html>
<html lang="en">
@include('partials.head')
<body>
@include('partials.menu')
<div class="container" id="wrap">
    <div class="row">
        <div id="content" class="col">
            @yield('content')
        </div>
    </div>
</div>
@include('partials.footer')
</body>
</html>
@endspaceless
