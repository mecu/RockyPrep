@spaceless
<!doctype html>
<html lang="en">
@include('partials.head')
<body>
@include('partials.menu')
<div class="container" id="wrap">
    <div class="row">
        <div id="sidebar" class="col-sm-3 col-md-2">
            @yield('sidebar')
        </div>
        <div id="content" class="col">
            @yield('content')
        </div>
    </div>
</div>
@include('partials.footer')
</body>
</html>
@endspaceless
