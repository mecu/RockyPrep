@extends('layouts.master')

@section('title') Login :: @parent @stop

@section('content')
<div class="row">
    <div class="page-header">
        <h2>Login</h2>
    </div>
</div>

<form role="form" method="POST" action="{{ URL::route('login') }}">
    {!! csrf_field() !!}
    <div class="row form-group">
        @include('errors.list')
        <label class="col-3 control-label">E-Mail Address</label>
        <div class="col">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                   title="email username">
        </div>
    </div>
    <div class="row form-group">
        <label class="col-3 control-label">Password</label>
        <div class="col">
            <input type="password" class="form-control" name="password" title="password">
        </div>
    </div>

    <div class="row form-group">
        <div class="col col-offset-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" checked> Remember Me
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-6 col-offset-4">
            <button type="submit" class="btn btn-primary" style="margin-right: 15px;">Login</button>
            <a href="{{ URL::to('password/reset') }}">Forgot Your Password?</a>
        </div>
    </div>
</form>
@endsection
