@extends('layouts.master')

@section('title') Register :: @parent @stop

@section('content')
    <div class="row">
        <div class="page-header">
            <h2>Register</h2>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            @include('errors.list')

            <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/auth/register') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Username</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="username"
                               value="{{ old('username') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Email</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script async src="/js/bootstrapValidator.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrapValidator.min.css" property="stylesheet">
    <script async src="/js/register.js"></script>
@endsection
