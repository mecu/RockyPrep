@extends('layouts.master') {{-- Web site Title --}}
@section('title') Change Password ::
@parent @stop {{-- Content --}} @section('content')
    <div class="page-header">
        <h1>Change Password</h1>
    </div>
    <form method="POST" action="{{ URL::to('auth/changepassword') }}"
          accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
        <fieldset>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" placeholder="password" type="password" name="password" id="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Password Confirmation</label>
                <input class="form-control" placeholder="password confirmation" type="password" name="password_confirmation" id="password_confirmation">
            </div>
            @if ($errors->has()) @foreach ($errors->all() as $error)
                <div class="alert alert-danger">{{ $error }}</div>
            @endforeach @endif
            <div class="form-actions form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </fieldset>
    </form>
@stop
