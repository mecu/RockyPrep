<?php 
namespace App;

ini_set('memory_limit', '512M');

$path = '/home/rp/db-backup';

$date = date('Y-m-d');

$filename = "$date-split.gz";

exec("mysqldump --defaults-extra-file=/home/rp/db-backup/config.cnf rp_rp > $path/$date.sql");
exec("gzip -c $path/$date.sql | split -a 1 -d -b 20m - $path/$date-split.gz");

$count = 0;

while (true) {
    if (file_exists("$path/$filename$count")) {
        ++$count;
    } else {
        break;
    }
}
$total = $count;
--$count;
$emails = 0;
while (true) {
    if (file_exists("$path/$filename$count")) {
        $fileNumber = $count + 1;
        $message = "File $fileNumber of $total.";
        if (email_file($path, $filename . $count, $message)) {
            unlink("$path/$filename$count");
            echo "Email success $fileNumber of $total." . PHP_EOL;
        } else {
            throw new \RuntimeException("Email failed $fileNumber of $total.");
        }

        --$count;
        ++$emails;
        if ($emails > 10) {
            throw new \RuntimeException("I've sent 10 emails, something is probably broken.");
        }
    } else {
        break;
    }
}

unlink("$path/$date.sql");

function email_file($path, $filename)
{
    $file = "$path/$filename";
    $subject = 'RockyPrep Backup Database File';
    $mailTo = 'Michael Hoppes <hoppes@gmail.com>';

    $file_size = filesize($file);
    $handle = fopen($file, 'r');
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));

    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // main header (multipart mandatory)
    $headers = 'From: Michael Hoppes <michael@rockyprep.com>' . PHP_EOL;
    $headers .= 'MIME-Version: 1.0' . PHP_EOL;
    $headers .= "Content-Type: multipart/mixed; boundary=\"$separator\"" . PHP_EOL;
    $headers .= 'Content-Transfer-Encoding: 7bit' . PHP_EOL;
    $headers .= 'This is a MIME encoded message.' . PHP_EOL;

    // message
    $message = "--$separator" . PHP_EOL;
    $message .= 'Content-Type: text/plain; charset="iso-8859-1"' . PHP_EOL;
    $message .= 'Content-Transfer-Encoding: 8bit' . PHP_EOL;
    $message .= $message . PHP_EOL;

    // attachment
    $message .= "--$separator" . PHP_EOL;
    $message .= "Content-Type: application/octet-stream; name=\"$filename\"" . PHP_EOL;
    $message .= 'Content-Transfer-Encoding: base64' . PHP_EOL;
    $message .= 'Content-Disposition: attachment' . PHP_EOL;
    $message .= $content . PHP_EOL;
    $message .= "--$separator--";

    //SEND Mail
    return mail($mailTo, $subject, $message, $headers);
}
