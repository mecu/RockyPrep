<?php
namespace Deployer;

require 'recipe/laravel.php';

// Configuration
inventory('hosts.yml');

set('repository', 'ssh://git@gitlab.com/mecu/RockyPrep.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);
set('default_stage', 'production');


// Hosts
host('rockyprep.com')
    ->stage('production')
    ->set('deploy_path', '/home/rp');


// Tasks

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
task('pwd', function () {
    $result = run('pwd');
    writeln("Current dir: $result");
});
